# LHCb Analysis Productions API server

See https://lbap.app.cern.ch/docs.

## Development

```bash
mamba env create --file environment.yaml
conda activate lbapi
pip install -e .[testing]
pre-commit install
```

Tests can be ran with pytest against the production instance of Analysis productions provided you have access to the necessary config file:

```bash
LBAP_DOTENV=/path/to/secrets.dotenv pytest
```
