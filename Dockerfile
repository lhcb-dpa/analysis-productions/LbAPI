FROM gitlab-registry.cern.ch/linuxsupport/cs9-base:latest

RUN yum install -y epel-release && \
    echo $'[eos9-stable]\nname=EOS binaries from CERN Koji [stable]\nbaseurl=http://linuxsoft.cern.ch/internal/repos/eos9-stable/x86_64/os\nenabled=1\ngpgcheck=False\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2\npriority=9' > /etc/yum.repos.d/eos9-stable.repo && \
    dnf install -y eos-client bzip2 && \
    yum clean all && \
    rm -rf /var/cache/yum

ENV MAMBA_ROOT_PREFIX=/opt/conda \
    PATH=/opt/conda/bin:$PATH \
    EOS_MGM_URL=root://eoslhcb.cern.ch

COPY environment.yaml /source/environment.yaml

RUN mkdir -p /opt/conda && \
    cd /opt/conda && \
    curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba && \
    eval "$(micromamba shell hook -s posix)" && \
    micromamba config append channels conda-forge && \
    micromamba create --yes --file "/source/environment.yaml" --name "lbapi" && \
    micromamba clean --all --force-pkgs-dirs --yes

COPY . /source

RUN eval "$(micromamba shell hook -s posix)" && \
    micromamba activate lbapi && \
    pip install /source

ENV PATH=/opt/conda/envs/lbapi/bin:$PATH

EXPOSE 8000
USER 1234
WORKDIR /tmp
CMD ["uvicorn", "LbAPI:app", "--host", "0.0.0.0", "--proxy-headers"]
