###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import re
import time
from collections import defaultdict
from enum import Enum
from threading import Lock
from typing import Optional

from authlib.integrations.starlette_client import OAuth
from authlib.jose import JoseError
from cachetools import TTLCache, cached
from fastapi import Depends, HTTPException, Request, status
from fastapi.security import OpenIdConnect
from pydantic import BaseModel, ValidationError
from sqlalchemy import func, select, text, update
from sqlalchemy.ext.asyncio import AsyncSession

from LbAPI.settings import (
    settings,
)

from .database import SessionLocal, extra_func, get_db
from .database.models import GitLabToken, UserToken
from .responses import (
    credentials_exception,
)
from .utils import Token, is_cern_ip, parse_jwt

logger = logging.getLogger(__name__)

oidc_scheme = OpenIdConnect(openIdConnectUrl=str(settings.cern_sso_well_known))

oauth = OAuth()
oauth.register(
    name="cern",
    server_metadata_url=str(settings.cern_sso_well_known),
    client_id=str(settings.cern_sso_client_id),
)

USER_TOKEN_INACTIVITY_TIMEOUT = text("INTERVAL 12 hour")


class Roles(str, Enum):
    DEFAULT = "default-role"
    LIASON = "liason"
    ADMIN = "admin"


class AuthTypes(str, Enum):
    CERN_SSO = "CERN_SSO"
    USER_TOKEN = "USER_TOKEN"
    GITLAB_TOKEN = "GITLAB_TOKEN"


class User(BaseModel):
    username: str
    roles: list[Roles] = []
    auth_type: AuthTypes
    email: Optional[str] = None
    name: Optional[str] = None
    token_id: Optional[int] = None


async def get_untrusted_user(
    request: Request,
    authorization: str = Depends(oidc_scheme),
    db: AsyncSession = Depends(get_db),
) -> User:
    """Get the current user regardless of authentication method.

    This method is similar to ``get_current_user`` except it also accepts long
    lived access tokens. This method should only be used for low risk endpoints.
    """
    logger.debug(
        "Authentication request for get_untrusted_user from %s", request.client.host
    )
    if match := re.fullmatch(r"Bearer (.+)", authorization):
        token = match.group(1)
    else:
        raise credentials_exception
    if token.startswith("lbapi:"):
        token = Token(token)
        if token.purpose == "gitlab":
            if not is_cern_ip(request.client.host):
                logger.warning(
                    "GitLab token used from non-CERN IP %s", request.client.host
                )
                raise HTTPException(
                    status_code=status.HTTP_403_FORBIDDEN,
                    detail="GitLab access tokens are only valid from CERN IPs",
                )
            query = select(GitLabToken.id, GitLabToken.owner)
            query = query.where(GitLabToken.apd_token == token.hashed_token)
            query = query.filter(
                GitLabToken.expires.is_(None)
                | (GitLabToken.expires > extra_func.utcnow())
            )
            query = query.filter(~GitLabToken.revoked)
            async with SessionLocal() as session:
                result = (await session.execute(query)).first()
                if result is None:
                    raise credentials_exception
                user = User(
                    username=result.owner,
                    auth_type=AuthTypes.GITLAB_TOKEN,
                    token_id=result.id,
                )
        elif token.purpose == "user":
            query = select(UserToken.id, UserToken.owner)
            query = query.where(UserToken.token == token.hashed_token)
            query = query.filter(
                func.date_add(UserToken.last_used, USER_TOKEN_INACTIVITY_TIMEOUT)
                > extra_func.utcnow()
            )
            query = query.filter(UserToken.expires > extra_func.utcnow())
            query = query.filter(~UserToken.revoked)
            async with SessionLocal() as session:
                result = (await session.execute(query)).first()
                if result is None:
                    raise credentials_exception
                user = User(
                    username=result.owner,
                    auth_type=AuthTypes.USER_TOKEN,
                    token_id=result.id,
                )

            query = update(UserToken)
            query = query.filter(UserToken.id == result.id)
            query = query.values(last_used=extra_func.utcnow())
            async with db.begin():
                result = await db.execute(query)
                if result.rowcount == 0:
                    raise NotImplementedError("Didn't find any rows to update!")
        else:
            logger.warning("Unknown token purpose %r", token.purpose)
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Unrecognised token purpose: {token.purpose!r}",
            )

        logger.debug("Used %s token to authorize user %r", token.purpose, user.username)
        return user
    else:
        return await get_current_user(request, authorization)


async def get_current_user(
    request: Request,
    authorization: str = Depends(oidc_scheme),
) -> User:
    """Get the current user, as authenticated using the CERN SSO."""
    logger.debug(
        "Authentication request for get_current_user from %s", request.client.host
    )
    try:
        token = await parse_jwt(authorization, settings.cern_sso_client_id, oauth.cern)
    except JoseError as e:
        logger.exception(
            "Failed to validate credentials %s: %r", time.time(), authorization
        )
        raise credentials_exception from e

    try:
        user = User(
            username=str(token["cern_upn"]),
            roles=token["cern_roles"],
            email=token["email"],
            name=token["name"],
            auth_type=AuthTypes.CERN_SSO,
        )
    except ValidationError as e:
        logger.exception("Failed to make user from credentials")
        raise credentials_exception from e
    logger.debug(
        "Used CERN SSO to authorize user %r with token %r", user.username, token
    )
    return user


@cached(cache=TTLCache(1, 10 * 60), lock=Lock())
def dirac_username_mapping():
    """Extrace a mapping of CERN usernames to DIRAC nicknames

    The result is cached for up to 10 minutes.
    """
    from DIRAC.ConfigurationSystem.Client.CSAPI import CSAPI
    from dirac_prod.utils import dw

    users = dw(CSAPI().describeUsers())
    mapping = defaultdict(list)
    for nickname, attributes in users.items():
        if "PrimaryCERNAccount" not in attributes:
            logger.warning("PrimaryCERNAccount not found for %s", nickname)
            continue
        if attributes.get("CERNAccountType") not in ["Primary", "Secondary", "Service"]:
            continue
        mapping[attributes["PrimaryCERNAccount"]].append(nickname)

    for cern_account, nicknames in list(mapping.items()):
        if len(nicknames) == 1:
            mapping[cern_account] = nicknames.pop()
        else:
            del mapping[cern_account]
            logger.warning(
                "Multiple accounts found for %s: %s", cern_account, nicknames
            )

    return dict(mapping)


class RunAsDIRACUser:
    def __init__(self, cern_user, group=None, check_proxy=True):
        from DIRAC.ConfigurationSystem.Client.Helpers import Registry
        from DIRAC.FrameworkSystem.Client.ProxyManagerClient import gProxyManager
        from dirac_prod.utils import dw

        self._cern_user = cern_user
        nickname = dirac_username_mapping().get(self._cern_user)
        if not nickname:
            raise ValueError(
                f"Failed to find a corresponding DIRAC user for {self._cern_user}.\n"
                + "Please make sure you are registered in DIRAC, see the twiki for details:\n"
                "    https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/Certificate"
            )
        self._nickname = nickname
        self._dn = dw(Registry.getDNForUsername(self._nickname))[0]
        # TODO: We can do something clever here with groups in DIRAC v7r1 to improve turnaround time
        self._group = group or dw(Registry.findDefaultGroupForUser(self._nickname))
        if check_proxy:
            # There should be no need for this but userHasProxy has proxy
            # doesn't understand groupless proxies and for the next year
            # we have a mess of both. This makes pandas sad and can be
            # removed in February 2022. (TODO)
            for group in [self._group, ""]:
                has_valid_proxy = gProxyManager.userHasProxy(
                    self._dn, group, validSeconds=6 * 24 * 60 * 60
                )
                if dw(has_valid_proxy):
                    break
            else:
                raise ValueError(
                    f"Failed to find a valid proxy with group {self._dn} for {self._group}.\n"
                    + "Try running lhcb-proxy-init on lxplus"
                )

    def __enter__(self):
        from DIRAC.Core.DISET.ThreadConfig import ThreadConfig

        logger.debug(
            "Setting up credentials for CERN=%s as DIRAC=%s DN=%s group=%s",
            self._cern_user,
            self._nickname,
            self._dn,
            self._group,
        )
        self._tc = ThreadConfig()
        if self._tc.getDN():
            raise NotImplementedError(self._tc.dump())
        self._tc.setDN(self._dn)
        self._tc.setGroup(self._group)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._tc.reset()


async def get_apc(user=Depends(get_untrusted_user)):
    from LHCbDIRAC.ProductionManagementSystem.Client.AnalysisProductionsClient import (
        AnalysisProductionsClient,
    )

    str(RunAsDIRACUser)

    yield AnalysisProductionsClient()
