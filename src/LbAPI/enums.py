###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from enum import StrEnum


class WebhookType(StrEnum):
    MC_REQUEST = "MC_REQUEST"
    ANA_PROD = "ANA_PROD"


class PipelineStatus(StrEnum):
    PENDING = "PENDING"
    PROCESSED = "PROCESSED"
    ERROR = "ERROR"


class CIRunStatus(StrEnum):
    # Waiting for jobs to be generated
    PENDING = "PENDING"
    # Jobs have been generated and are being ran
    PROCESSED = "PROCESSED"
    # All jobs have been ran and dry run has been performed
    READY_TO_SUBMIT = "READY_TO_SUBMIT"
    # Something went wrong
    ERROR = "ERROR"


class JobStatus(StrEnum):
    PENDING = "PENDING"
    SUBMITTING = "SUBMITTING"
    SUBMITTED = "SUBMITTED"
    STARTED = "STARTED"
    RUNNING = "RUNNING"
    VALIDATING = "VALIDATING"
    SUCCESS = "SUCCESS"
    FAILED = "FAILED"
    CANCELLED = "CANCELLED"


ALLOWED_JOB_TRANSITIONS = {
    JobStatus.PENDING: [JobStatus.SUBMITTING, JobStatus.CANCELLED],
    JobStatus.SUBMITTING: [JobStatus.SUBMITTED, JobStatus.CANCELLED],
    JobStatus.SUBMITTED: [JobStatus.STARTED, JobStatus.CANCELLED],
    JobStatus.STARTED: [JobStatus.RUNNING, JobStatus.CANCELLED],
    JobStatus.RUNNING: [
        JobStatus.RUNNING,
        JobStatus.VALIDATING,
        JobStatus.SUCCESS,
        JobStatus.FAILED,
        JobStatus.CANCELLED,
    ],
    JobStatus.VALIDATING: [JobStatus.SUCCESS, JobStatus.FAILED, JobStatus.CANCELLED],
    JobStatus.SUCCESS: [],
    JobStatus.FAILED: [],
    JobStatus.CANCELLED: [],
}

assert set(JobStatus) == set(ALLOWED_JOB_TRANSITIONS)
