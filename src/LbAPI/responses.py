###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from fastapi import HTTPException, status

credentials_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"},
)


dirac_bad_user_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Failed to find a corresponding DIRAC user. Are you registered in DIRAC?",
)


dirac_bad_proxy_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Failed to find a valid DIRAC proxy. Running lhcb-proxy-init will usually fix this problem.",
)

webhook_unauthorized_request = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Unauthorized webhook payload",
    # headers={"X-Gitlab-Token": ""},
)

webhook_unsupported_request = HTTPException(
    status_code=status.HTTP_400_BAD_REQUEST,
    detail="Unsupported gitlab webhook event",
)

webhook_incorrect_project = HTTPException(
    status_code=status.HTTP_400_BAD_REQUEST,
    detail="Incorrect gitlab project",
)
