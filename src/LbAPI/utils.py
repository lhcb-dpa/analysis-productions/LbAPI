###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import asyncio
import base64
import hashlib
import inspect
import ipaddress
import json
import logging
import os
import re
import secrets
import subprocess
from datetime import datetime
from functools import wraps
from tempfile import NamedTemporaryFile
from typing import Optional

import cachetools
from authlib.integrations.starlette_client import StarletteOAuth2App
from authlib.jose import JsonWebKey, JsonWebToken
from authlib.oidc.core import IDToken
from pydantic import SecretStr

from LbAPI.settings import (
    gitlab,
    settings,
)

logger = logging.getLogger(__name__)


def set_content_range(response, unit, offset, results, n_total):
    header = f"{unit} "
    if results:
        header += f"{offset}-{offset + len(results) - 1}"
    else:
        header += "*"
    header += f"/{n_total}"
    response.headers["Content-Range"] = header
    response.headers["Access-Control-Expose-Headers"] = "Content-Range"


class Token:
    @classmethod
    def generate(cls, purpose: str) -> tuple[str, Token]:
        raw_token = f"lbapi:{purpose}:{secrets.token_urlsafe(32)}"
        token = cls(raw_token)
        return raw_token, token

    def __init__(self, raw_token: str):
        if not raw_token.startswith("lbapi:"):
            raise NotImplementedError("LbAPI tokens should start with 'lbapi:'")
        _, self.purpose, b64data = raw_token.split(":", 2)
        self.hashed_token = hashlib.pbkdf2_hmac(
            settings.token_algorithm,
            base64.urlsafe_b64decode(b64data + "=" * (len(b64data) % 4)),
            settings.token_salt,
            settings.token_rounds,
        )


async def get_krb5_creds(
    username: str, password: str, realm: str
) -> NamedTemporaryFile:
    if isinstance(username, SecretStr):
        username = username.get_secret_value()
    if isinstance(password, SecretStr):
        password = password.get_secret_value()

    if not hasattr(get_krb5_creds, "cache"):
        get_krb5_creds.cache = cachetools.TTLCache(maxsize=10, ttl=60 * 60)
    try:
        krb5_cache = get_krb5_creds.cache[(username, password, realm)]
        logger.debug("Using cached krb5 credentials from %s", krb5_cache.name)
        return krb5_cache
    except KeyError:
        pass
    krb5_cache = NamedTemporaryFile()
    returncode, _, _ = await run(
        *["kinit", f"{username}@{realm}"],
        extra_env={"KRB5CCNAME": f"FILE:{krb5_cache.name}"},
        input=password,
    )
    if returncode != 0:
        raise RuntimeError("Failed to make krb5 credentials")
    logger.debug("Wrote krb5 credentials to %s", krb5_cache.name)
    get_krb5_creds.cache[(username, password, realm)] = krb5_cache
    return krb5_cache


async def run(
    *command: str,
    extra_env: Optional[dict[str, str]] = None,
    cwd: Optional[os.PathLike] = None,
    input: Optional[str] = None,
    timeout=60,
    log_cmd: bool = True,
    log_stdout: bool = False,
    write_logfile: Optional[os.PathLike] = None,
) -> tuple[int, str, str]:
    proc = await asyncio.create_subprocess_exec(
        *command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=os.environ | (extra_env or {}),
        stdin=subprocess.PIPE if input else subprocess.DEVNULL,
        cwd=str(cwd) if cwd else None,
        limit=1024**2,
    )
    log = logger.getChild(f"{proc.pid}")

    if write_logfile:
        formatter = logging.Formatter(
            "[%(asctime)s] %(name)s - %(levelname)s - %(message)s"
        )
        handler = logging.FileHandler(write_logfile)
        handler.setFormatter(formatter)
        log.addHandler(handler)

    log.info("Running command %r", command if log_cmd else ["REDACTED"])
    awaitables = [
        _handle_output(log.getChild("stdout") if log_stdout else None, proc.stdout),
        _handle_output(log.getChild("stderr"), proc.stderr),
        asyncio.wait_for(proc.wait(), timeout=timeout),
    ]
    if input:
        proc.stdin.write(input.encode("utf-8"))
        proc.stdin.write_eof()
    stdout, stderr, proc_wait_ = await asyncio.gather(
        *awaitables, return_exceptions=True
    )
    if isinstance(proc_wait_, Exception):
        raise proc_wait_
    log.info("Command exited with %r", proc.returncode)
    return (proc.returncode, stdout, stderr)


async def _handle_output(logger: logging.Logger, stream: asyncio.StreamWriter):
    """Utility function used by run to handle stdout and stderr."""
    result = []
    while line := await stream.readline():
        line = line.decode("utf-8", errors="backslashreplace")
        result += [line]
        if logger:
            logger.debug(line.rstrip())
    return "".join(result)


async def eos_cp(local_path: str, path: str, krb5_cache=None):
    extra_env = {}
    if krb5_cache:
        extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache}"

    cmd = ["eos", "--batch", "cp", local_path, path]
    returncode, stdout, stderr = await run(*cmd, extra_env=extra_env)
    if "no such file or directory" in stderr:
        raise FileNotFoundError(f"No such file or directory: {path}")
    if returncode != 0:
        raise RuntimeError(
            f"Failed to copy {local_path} to {path}. See output: {stdout}. See error: {stderr}"
        )
    return


async def generate_eos_token(
    directory: str, allow_write: bool, owner: str, expires: datetime, *, tree=True
):
    extra_env = {}
    if settings.eos_token_user and settings.eos_token_password:
        krb5_cache = await get_krb5_creds(
            settings.eos_token_user, settings.eos_token_password, "CERN.CH"
        )
        extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache.name}"

    cmd = ["eos", "-r", "0", "0", "token"]
    cmd += ["--owner", owner, "--group", "z5"]
    cmd += ["--path", directory]
    if tree:
        cmd += ["--tree"]
    cmd += ["--permission", "rwx" if allow_write else "rx"]
    cmd += ["--expires", f"{expires.timestamp():.0f}"]
    returncode, stdout, _ = await run(*cmd, extra_env=extra_env)
    if returncode != 0:
        raise RuntimeError("Failed to request EOS token")
    eos_token = stdout.strip()

    # TODO: Why does this need credentials?
    returncode, stdout, _ = await run(
        *["eos", "token", "--token", eos_token],
        extra_env=extra_env,
        log_cmd=False,
    )
    if returncode != 0:
        raise RuntimeError("Failed to decode EOS token")
    token_metadata = json.loads(stdout)["token"]

    return eos_token, token_metadata


async def eos_ls(path: str) -> list[str]:
    extra_env = {}
    if settings.eos_token_user and settings.eos_token_password:
        krb5_cache = await get_krb5_creds(
            settings.eos_token_user, settings.eos_token_password, "CERN.CH"
        )
        extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache.name}"

    cmd = ["eos", "--batch", "ls", path]
    returncode, stdout, stderr = await run(*cmd, extra_env=extra_env)
    if "No such file or directory" in stderr:
        raise FileNotFoundError(f"No such file or directory: {path}")
    if returncode != 0:
        raise RuntimeError(f"Failed to list {path}")
    return [line for line in stdout.splitlines() if line]


async def safe_eos_ls(path: str) -> list[str]:
    """
    Safely list the contents of a directory
    Returns an empty list if the directory does not exist, instead of raising an exception
    """

    try:
        return await eos_ls(path)
    except (FileNotFoundError, RuntimeError):
        return []


async def eos_exists(path: str) -> bool:
    extra_env = {}
    if settings.eos_token_user and settings.eos_token_password:
        krb5_cache = await get_krb5_creds(
            settings.eos_token_user, settings.eos_token_password, "CERN.CH"
        )
        extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache.name}"

    cmd = ["eos", "--batch", "info", path]
    returncode, stdout, stderr = await run(*cmd, extra_env=extra_env)
    return returncode == 0 and "No such file or directory" not in stderr


async def eos_get_files_to_remove(path: str, days: int, size: int) -> list[str]:
    extra_env = {}
    if settings.eos_token_user and settings.eos_token_password:
        krb5_cache = await get_krb5_creds(
            settings.eos_token_user, settings.eos_token_password, "CERN.CH"
        )
        extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache.name}"
    cmd = ["eos", "--batch", "find", "-f", "-ctime", f"+{days}", "--size", path]
    returncode, stdout, stderr = await run(*cmd, extra_env=extra_env)
    if "No such file or directory" in stderr:
        raise FileNotFoundError(f"No such file or directory: {path}")
    if returncode != 0:
        raise RuntimeError(f"Failed to list {path}")
    file_paths = re.findall(r"(/eos/lhcb/.+?)\s+size=(\d+)", stdout)
    return [out_path for out_path, out_size in file_paths if int(out_size) >= size]


async def eos_rm(path: str):
    extra_env = {}
    if settings.eos_token_user and settings.eos_token_password:
        krb5_cache = await get_krb5_creds(
            settings.eos_token_user, settings.eos_token_password, "CERN.CH"
        )
        extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache.name}"
    # TODO: Absolute hack right now, look into EOS ACLs
    cmd = ["eos", "--batch", "-r", "0", "0", "rm", path]
    returncode, stdout, stderr = await run(*cmd, extra_env=extra_env)
    if "no such file or directory" in stderr:
        raise FileNotFoundError(f"No such file or directory: {path}")
    if returncode != 0:
        raise RuntimeError(
            f"Failed to remove {path}. See output: {stdout}. See error: {stderr}"
        )
    return


async def parse_jwt(authorization: str, audience: str, oauth2_app: StarletteOAuth2App):
    if match := re.fullmatch(r"Bearer (.+)", authorization):
        token = match.group(1)
    else:
        raise ValueError("Invalid authorization header")

    metadata = await oauth2_app.load_server_metadata()
    alg_values = metadata.get("id_token_signing_alg_values_supported") or ["RS256"]
    jwt = JsonWebToken(alg_values)
    jwk_set = await oauth2_app.fetch_jwk_set()
    token = jwt.decode(
        token,
        key=JsonWebKey.import_key_set(jwk_set),
        claims_cls=IDToken,
        claims_options={
            "iss": {"values": [metadata["issuer"]]},
            "aud": {"values": [audience]},
        },
    )
    token.validate()
    return token


def is_cern_ip(ip: str) -> bool:
    return any(
        ipaddress.ip_address(ip) in network for network in settings.cern_networks
    )


def get_package_version(pkg_name):
    result = subprocess.run(["pip", "show", pkg_name], stdout=subprocess.PIPE)
    out = str(result.stdout, "utf-8")
    match = re.search(r"Version: (.+)\nSummary", out)
    assert match, out
    return match.groups()[0]


def eos_artifact_path(
    eos_artifact_path: str,
    pipeline_id: int,
    ci_run_name: str,
    job_id: int,
    job_name: str,
) -> tuple[str, str]:
    fragments = [
        eos_artifact_path,
        pipeline_id,
        ci_run_name,
        job_id,
        f"{_make_eos_safe_name(job_name)}/",
    ]
    return "eoslhcb.cern.ch", "/".join(map(str, fragments))


def _make_eos_safe_name(name: str) -> str:
    """Make a name safe for EOS.

    EOS only allows A-Z a-z 0-9 / SPACE .-_~#:^ in filenames.
    Replace all other characters with #.
    """
    return re.sub(r"[^A-Za-z0-9 \.\-_~#:^]", "_", name)


class ExceptionForGitlab(Exception):
    """An exception that can be raised and then placed in to a Gitlab comment."""

    def __init__(self, message):
        super().__init__(message)
        self.recovery_cmd = None

    def post_issue_comment(self, project_id, issue_iid):
        project = gitlab.api.projects.get(project_id)
        resource = project.issues.get(issue_iid)
        return self._post_comment(resource)

    def post_mr_comment(self, project_id, mr_iid):
        project = gitlab.api.projects.get(project_id)
        resource = project.mergerequests.get(mr_iid)
        return self._post_comment(resource)

    def _post_comment(self, resource):
        body = str(self)
        if self.recovery_cmd:
            body += (
                "\n\n<details><summary>Info for experts</summary>\n"
                f"To recover, run:\n\n```\n{self.recovery_cmd}\n```\n"
                "</details>\n"
            )
        resource.discussions.create({"body": body})


def retryable_on_error(func):
    """Decorator for functions that should be retried on error.

    If the function raises an ExceptionForGitlab, the recovery command will be
    included in the exception message.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ExceptionForGitlab as e:
            # Add in recovery command
            e.recovery_cmd = (
                f"from {func.__module__} import {func.__name__}\n"
                f"{func.__name__}(*{args}, **{kwargs})\n"
            )

            # Try and post a MR comment
            try:
                sig = inspect.signature(func)
                arg_names = list(sig.parameters.keys())
                arg_dict = {**dict(zip(arg_names, args)), **kwargs}
                if all(arg in arg_dict for arg in ["project_id", "mr_id"]):
                    e.post_mr_comment(arg_dict["project_id"], arg_dict["mr_id"])
            except Exception as ex:
                logger.exception(f"Failed to post exception info to gitlab: {ex}")

            raise

    return wrapper
