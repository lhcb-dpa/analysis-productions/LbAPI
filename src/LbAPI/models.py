###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Any, Optional

from pydantic import BaseModel


class ProductionSummaryStats(BaseModel):
    full_input_size: int
    full_est_output_size: int
    test_input_size: int
    test_output_size: int
    test_total_processed: int
    mem_footprint: int


ProductionSummaryProgress = dict[str, int]


class JobInfo(BaseModel):
    id: int
    name: str
    keep_output: bool

    application_name: str
    application_version: str

    wg: str
    data_type: Optional[str]
    simulation: Optional[bool]
    input_type: Optional[str]
    conddb_tag: Optional[str]
    dddb_tag: Optional[str]
    priority: str
    automatically_configure: bool
    autoconf_options: Optional[str]
    dynamic_options_path: Optional[str]
    input: dict[str, Any]

    class TestInfo(BaseModel):
        attempt: int
        status: str

        class TestStatistics(BaseModel):
            events_processed: Optional[int]
            events_requested: Optional[int]
            run_time: Optional[float]
            cpu_norm: Optional[float]
            mem_footprint: Optional[int]

        statistics: TestStatistics

        class TestInputFile(BaseModel):
            path: str
            dataset_size: int
            size: int

        input_files: list[TestInputFile]

        class TestOutputFile(BaseModel):
            path: str
            dataset_size: int
            size: int

        output_files: list[TestOutputFile]

        class TestLogSummary(BaseModel):
            warning: Optional[int]
            error: Optional[int]
            fatal: Optional[int]

        n_log: TestLogSummary

    test: TestInfo


class PipelineProductionInfo(BaseModel):
    id: int
    pipeline_id: int
    name: str
    repo_url: str
    commit: str
    commit_message: str
    triggering_user: str
    pipeline_url: str
    comment: str


class FileURL(BaseModel):
    url: str
    name: str
    validity: int
