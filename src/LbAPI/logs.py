###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configure logging to write a JSON file for each process.

To use this an index pattern should be created in opensearch with::

    PUT _index_template/lbap-logs
    {
        "index_patterns": [
            "lbap-logs-*"
        ],
        "template": {
            "mappings": {
                "properties": {
                    "@timestamp": {"type": "date_nanos"},
                    "level": {"type": "keyword"},
                    "message": {"type": "text"},
                    "logger_name": {"type": "keyword"},
                    "process_name": {"type": "keyword"},
                    "process_id": {"type": "long"},
                    "thread_name": {"type": "keyword"},
                    "thread_id": {"type": "long"},
                    "message_raw": {"type": "keyword"},
                    "message_args": {"type": "text"},
                    "exc_info": {"type": "text"},
                    "hostname": {"type": "keyword"}
                }
            }
        }
    }

fluentbit can then be configured with::

    [SERVICE]
        flush             10
        log_level         debug
        parsers_file      /path/to/parsers.conf

    [INPUT]
        name              tail
        path              /path/to/lbap.log.*
        parser            myparser
        read_from_head    True
        refresh_interval  60
        skip_long_lines   On
        db                /path/to/logs.db

    [FILTER]
        name        expect
        match       *
        key_exists  level
        key_exists  message
        action      warn

    [OUTPUT]
        name  es
        match *
        host  xxxxxxxxxxx
        path /es
        http_user xxxxxxxxxxx
        http_passwd xxxxxxxxxxx
        port  443
        tls   on
        tls.verify on
        tls.ca_file /cvmfs/lhcb.cern.ch/etc/grid-security/certificates/CERN-Root-2.pem
        Time_Key_Nanos on
        Logstash_Format on
        Logstash_Prefix lbap-logs
        Logstash_DateFormat %Y.%m

where ``parsers.conf`` is::

    [PARSER]
        Name        myparser
        Format      json
        Time_Key    timestamp
        Time_Format %Y-%m-%d %H:%M:%S,%L
"""
import json
import logging.handlers
import os
import socket
import time

LOG_PREFIX = os.environ.get("LOG_PREFIX", "lbap.log")
LOGGER_PATTERNS = [
    "Lb*",
    "uvicorn*",
    "dirac*",
    "AnaProd*",
    "fastapi",
    "gitlab_runner_api",
]
logger = logging.getLogger(__name__)


class JsonFormatter(logging.Formatter):
    """Formatter that outputs JSON strings after parsing the LogRecord"""

    fmt_dict = {
        "level": "levelname",
        "message": "message",
        "logger_name": "name",
        "process_name": "processName",
        "process_id": "process",
        "thread_name": "threadName",
        "thread_id": "thread",
        "timestamp": "asctime",
        "message_raw": "message_raw",
        "message_args": "message_args",
    }

    def formatMessage(self, record) -> dict:
        return {
            fmt_key: record.__dict__[fmt_val]
            for fmt_key, fmt_val in self.fmt_dict.items()
        }

    def formatTime(self, record, datefmt=None):
        """:py:meth:`logging.Formatter.formatTime` with microsecond precision by default"""
        ct = self.converter(record.created)
        if datefmt:
            s = time.strftime(datefmt, ct)
        else:
            t = time.strftime("%Y-%m-%d %H:%M:%S", ct)
            s = "%s,%06d" % (t, (record.created - int(record.created)) * 1e6)
        return s

    def format(self, record) -> str:
        record.message = record.getMessage()
        record.asctime = self.formatTime(record, self.datefmt)
        record.message_raw = record.msg
        try:
            record.message_args = json.dumps(record.args)
        except Exception:
            record.message_args = "ERROR!"
        message_dict = self.formatMessage(record)
        message_dict["hostname"] = socket.gethostname()

        if record.exc_info:
            # Cache the traceback text to avoid converting it multiple times
            # (it's constant anyway)
            if not record.exc_text:
                record.exc_text = self.formatException(record.exc_info)

        if record.exc_text:
            message_dict["exc_info"] = record.exc_text

        if record.stack_info:
            message_dict["stack_info"] = self.formatStack(record.stack_info)

        return json.dumps(message_dict, default=str)
