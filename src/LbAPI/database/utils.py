###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "Column",
    "DateNowColumn",
    "EnumColumn",
    "NullColumn",
    "TokenColumn",
)

from functools import partial

from sqlalchemy import BINARY, DateTime, Enum
from sqlalchemy import Column as RawColumn

from . import extra_func

Column = partial(RawColumn, nullable=False)
NullColumn = partial(RawColumn, nullable=True)
TokenColumn = partial(Column, BINARY(32), unique=True, index=True)
DateNowColumn = partial(
    Column, DateTime(timezone=True), server_default=extra_func.utcnow()
)


def EnumColumn(enum_type, **kwargs):
    return Column(Enum(enum_type, native_enum=False, length=16), **kwargs)
