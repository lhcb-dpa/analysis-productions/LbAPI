###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = [
    "create_engine",
    "dispose_engine",
    "get_db",
    "extra_func",
    "webhooks",
    "retryable_query",
]
import asyncio
import functools
import logging
import random

import pymysql
import sqlalchemy
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import NullPool

from LbAPI.settings import (
    settings,
)

from . import extra_func, webhooks
from .models import Base

logger = logging.getLogger(__name__)


async def create_engine(*, poolclass=None):
    global engine, _SessionLocal
    engine = create_async_engine(
        str(settings.db_url).replace("mysql", "mysql+aiomysql", 1),
        echo=False,
        future=True,
        # MySQL closes connections if there is no activity so recycle them once an hour
        # https://docs.sqlalchemy.org/en/14/core/pooling.html#pool-setting-recycle
        pool_recycle=3600,
        poolclass=NullPool if settings.db_use_null_pool else poolclass,
    )

    # Define the listener function
    def set_group_concat_max_len(dbapi_connection, connection_record):
        cursor = dbapi_connection.cursor()
        cursor.execute("SET SESSION group_concat_max_len = 1048576")
        cursor.close()

    sqlalchemy.event.listen(engine.sync_engine, "connect", set_group_concat_max_len)

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    _SessionLocal = sessionmaker(
        autocommit=False, autoflush=False, bind=engine, class_=AsyncSession
    )


class _SessionProxy:
    def __getattribute__(self, name):
        return getattr(_SessionLocal, name)

    def __call__(self, *args, **kwargs):
        return _SessionLocal(*args, **kwargs)


SessionLocal = _SessionProxy()


async def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        await db.close()


async def dispose_engine():
    await engine.dispose()


def retryable_query(*, n_retries=5):
    def decorator(query_func):
        @functools.wraps(query_func)
        async def retryable_query_inner(*args, **kwargs):
            for retry in range(n_retries):
                try:
                    return await query_func(*args, **kwargs)
                except sqlalchemy.exc.OperationalError as e:
                    if not isinstance(e.orig, pymysql.err.OperationalError):
                        raise
                    if e.orig.args[0] != pymysql.constants.ER.LOCK_DEADLOCK:
                        raise
                    logger.exception(
                        "Found deadlock while inserting jobs, retry %s of %s",
                        retry,
                        n_retries - 1,
                    )
                    await asyncio.sleep(random.random() * 0.5 * n_retries)

            raise RuntimeError(f"Failed to run {query_func} after {n_retries} retries")

        return retryable_query_inner

    return decorator
