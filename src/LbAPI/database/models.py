###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "GitLabToken",
    "RegisteredRepository",
    "UserToken",
)

from sqlalchemy import JSON, Boolean, Computed, DateTime, Integer, String, Unicode
from sqlalchemy.ext.declarative import declarative_base

from .utils import Column, DateNowColumn, NullColumn, TokenColumn

Base = declarative_base()


class RegisteredRepository(Base):
    __tablename__ = "registered_repositories"

    id = Column(Integer, primary_key=True)
    project_id = Column(Integer, index=True)
    project_path = Column(String(64))
    user_login = Column(String(64), index=True)
    subject_filter = Column(String(64))
    only_protected = Column(Boolean)
    max_duration_seconds = Column(Integer)
    eos_specs = Column(JSON)
    expires = NullColumn(DateTime(timezone=True), default=None)
    revoked = Column(Boolean, default=False)


class GitLabToken(Base):
    __tablename__ = "gitlab_tokens"

    id = Column(Integer, primary_key=True)
    registered_repo_id = Column(Integer, index=True)
    created = DateNowColumn()
    successful = Column(Boolean, default=False)
    gitlab_token = Column(JSON)
    eos_token = Column(JSON)
    apd_token = TokenColumn()
    expires = Column(DateTime(timezone=True))
    revoked = Column(Boolean, default=False)
    owner = Column(
        String(64), Computed("`gitlab_token` ->> '$.user_login'"), index=True
    )
    project_id = Column(
        Integer,
        Computed("CAST((`gitlab_token` ->> '$.project_id') AS DECIMAL)"),
        index=True,
    )


class UserToken(Base):
    __tablename__ = "user_tokens"

    id = Column(Integer, primary_key=True)
    created = DateNowColumn()
    last_used = DateNowColumn(index=True)
    token = TokenColumn()
    owner = Column(String(64))
    description = Column(Unicode(1024))
    expires = Column(DateTime(timezone=True), default=None)
    revoked = Column(Boolean, default=False)
