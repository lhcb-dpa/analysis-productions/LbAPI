###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "CacheEntries",
    "ProjectWebhook",
    "Pipeline",
    "CIRun",
    "Job",
)

from sqlalchemy import JSON, Boolean, ForeignKey, Integer, String, UnicodeText
from sqlalchemy.orm import relationship
from sqlalchemy.schema import UniqueConstraint

from LbAPI.database.models import Base
from LbAPI.enums import CIRunStatus, JobStatus, PipelineStatus, WebhookType

from .utils import Column, DateNowColumn, EnumColumn, NullColumn, TokenColumn


class CacheEntries(Base):
    __tablename__ = "cache_entries"

    id = Column(Integer, primary_key=True)
    key = Column(String(128), unique=True)
    value = Column(JSON)
    created = DateNowColumn()


class ProjectWebhook(Base):
    __tablename__ = "v2_project_webhooks"

    project_id = Column(Integer, primary_key=True)
    name = Column(String(64))
    type = EnumColumn(WebhookType)
    secret = TokenColumn()
    eos_artifact_path = NullColumn(String(256), default=None)

    pipelines = relationship(f"{__name__}.Pipeline")


class Pipeline(Base):
    __tablename__ = "v2_pipelines"
    __table_args__ = (
        UniqueConstraint("project_id", "commit_sha", "mr_id", name="_pipeline_uc"),
    )

    id = Column(Integer, primary_key=True)
    project_id = Column(Integer, ForeignKey("v2_project_webhooks.project_id"))
    commit_sha = Column(String(40))
    commit_message = Column(UnicodeText(length=2**24 - 1))
    mr_id = Column(Integer)
    author = Column(String(32))
    created = DateNowColumn()
    status = EnumColumn(PipelineStatus, default=PipelineStatus.PENDING)
    status_info = Column(UnicodeText(length=2**24 - 1), default="")

    ci_runs = relationship(f"{__name__}.CIRun")


class CIRun(Base):
    __tablename__ = "v2_ci_runs"
    __table_args__ = (UniqueConstraint("pipeline_id", "name", name="_ci_run_uc"),)

    id = Column(Integer, primary_key=True)
    pipeline_id = Column(Integer, ForeignKey("v2_pipelines.id"))
    name = Column(String(64))
    status = EnumColumn(CIRunStatus, default=CIRunStatus.PENDING)
    status_info = Column(UnicodeText(length=2**24 - 1), default="")

    jobs = relationship(f"{__name__}.Job")


class Job(Base):
    __tablename__ = "v2_jobs"
    __table_args__ = (
        UniqueConstraint("request_id", "subrequest_id", name="idx_request_id"),
    )

    id = Column(Integer, primary_key=True)
    ci_run_id = Column(Integer, ForeignKey("v2_ci_runs.id"))
    name = Column(String(100))
    secret = TokenColumn(nullable=True)
    submitted_id = NullColumn(String(32), unique=True, default=None)
    request_id = NullColumn(Integer, default=None)
    subrequest_id = NullColumn(Integer, default=None)
    status = EnumColumn(JobStatus, default=JobStatus.PENDING)
    spec = Column(JSON)
    result = NullColumn(JSON, default=None)
    created = DateNowColumn()
    outdated = Column(Boolean, default=False)
    n_retries = Column(Integer, default=0)
