###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import importlib.resources
import json
import subprocess
from contextlib import contextmanager
from tempfile import NamedTemporaryFile

from cachetools import TTLCache, cached

from LbAPI.settings import dirac_host_cert, dirac_host_key, settings
from LbAPI.utils import ExceptionForGitlab


@cached(cache=TTLCache(maxsize=1, ttl=10 * 60))
def get_user_mapping():
    if dirac_host_cert is None or dirac_host_key is None:
        print("No host cert or key provided, skipping nickname lookup")
        stdout = subprocess.check_output(
            settings.lb_dirac + ["dirac-proxy-info", "--checkvalid"], text=True
        )
        lines = [line.split(":", 1) for line in stdout.split("\n") if line.strip()]
        proxy_info = {key.strip(): value.strip() for key, value in lines}
        return {proxy_info["username"]: proxy_info["username"]}

    with (
        importlib.resources.path(
            "LbAPI.ci.templates", "do_nickname_lookup.py"
        ) as lookup_script,
        NamedTemporaryFile() as mapping_file,
    ):
        subprocess.run(
            settings.lb_dirac
            + [
                "python",
                lookup_script,
                "--server-credentials",
                dirac_host_cert.path,
                dirac_host_key.path,
                f"--out={mapping_file.name}",
            ],
            check=True,
        )
        return json.loads(mapping_file.read())


def get_req_state(request_ids, return_states=False):
    with (
        importlib.resources.path(
            "LbAPI.ci.templates", "request_info.py"
        ) as request_script,
        NamedTemporaryFile() as mapping_file,
    ):
        subprocess.run(
            settings.lb_dirac
            + [
                "python",
                request_script,
                "--server-credentials",
                dirac_host_cert.path,
                dirac_host_key.path,
                f"--out={mapping_file.name}",
                *(["--return-states"] if return_states else []),
                "--req_ids",
            ]
            + [str(rid) for rid in request_ids],
            check=True,
        )
        return json.loads(mapping_file.read())


@cached(cache=TTLCache(maxsize=1024, ttl=60 * 60 * 12))
def get_proxy_data(cern_username, group):
    mapping = get_user_mapping()
    if cern_username not in mapping:
        raise ExceptionForGitlab(f"User {cern_username} not found in nickname mapping")
    nickname = mapping[cern_username]
    with NamedTemporaryFile() as proxy_file:
        cmd = settings.lb_dirac + ["dirac-admin-get-proxy"]
        if dirac_host_cert is not None and dirac_host_key is not None:
            cmd += [
                "--option=/DIRAC/Security/UseServerCertificate=yes",
                f"--option=/DIRAC/Security/CertFile={dirac_host_cert.path}",
                f"--option=/DIRAC/Security/KeyFile={dirac_host_key.path}",
            ]
        cmd += ["--out", proxy_file.name, nickname, group]
        try:
            # Set stdin so this still works even if there is more than one DN registered
            subprocess.check_output(
                cmd, text=True, stderr=subprocess.STDOUT, input="\n"
            )
        except subprocess.CalledProcessError as e:
            raise ExceptionForGitlab(
                f"Failed to get proxy for {nickname} in {group}\n\n"
                f"@{cern_username} you likely need to run `lhcb-proxy-init` "
                "on lxplus to be able to perform this action. See"
                "https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/Certificate for details"
            ) from e
        return proxy_file.read()


@contextmanager
def DownloadedProxyFile(username, group):
    with NamedTemporaryFile() as proxy_file:
        proxy_file.write(get_proxy_data(username, group))
        proxy_file.flush()
        proxy_file.seek(0)
        yield proxy_file
