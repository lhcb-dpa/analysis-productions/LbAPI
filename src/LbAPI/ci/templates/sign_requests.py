#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

import DIRAC
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--req-ids", nargs="+", required=True, type=int)
    parser.add_argument("--old-state", required=True, type=str)
    parser.add_argument("--new-state", required=True, type=str)
    args = parser.parse_args()

    DIRAC.initialize()
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
        ProductionRequestClient,
    )

    pmc = ProductionRequestClient()

    old_states = {
        k: v["RequestState"]
        for k, v in returnValueOrRaise(pmc.getProductionRequest(args.req_ids)).items()
    }

    for req in args.req_ids:
        if old_states[req] != args.old_state:
            print(f"Request {req} is not in state {args.old_state}, skipping")
        else:
            returnValueOrRaise(
                pmc.updateProductionRequest(int(req), {"RequestState": args.new_state})
            )


if __name__ == "__main__":
    parse_args()
