#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
import sys
from collections import defaultdict
from pathlib import Path

import DIRAC
from DIRAC.ConfigurationSystem.Client.Config import gConfig
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--server-credentials", nargs=2, required=True)
    parser.add_argument("--out", required=True)
    args = parser.parse_args()

    DIRAC.initialize(host_credentials=args.server_credentials)
    users = returnValueOrRaise(gConfig.getOptionsDictRecursively("/Registry/Users"))
    ccid_to_nick = defaultdict(list)
    for nickname, user_info in users.items():
        if user_info.get("CERNAccountType") in {"Primary", "Secondary", "Service"}:
            if ccid := user_info.get("PrimaryCERNAccount"):
                ccid_to_nick[ccid].append(nickname)
    for ccid, nicknames in list(ccid_to_nick.items()):
        if len(nicknames) == 1:
            ccid_to_nick[ccid] = ccid_to_nick[ccid][0]
        else:
            print(
                f"Removing {ccid} as they map to multiple nicknames: {nicknames}",
                file=sys.stderr,
            )
            ccid_to_nick.pop(ccid)
    Path(args.out).write_text(json.dumps(ccid_to_nick))


if __name__ == "__main__":
    parse_args()
