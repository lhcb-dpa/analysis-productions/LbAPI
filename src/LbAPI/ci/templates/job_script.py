#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import argparse
import asyncio
import json
import os
import shlex
import subprocess
from contextlib import AsyncExitStack, asynccontextmanager
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Any, Dict, Optional

import log_uploader  # pylint: disable=import-error
import requests

os.environ.pop("JOBID", None)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.parse_args()

    job_dir = Path(__file__).parent
    try:
        asyncio.run(main(job_dir, session=requests.Session()))
    except log_uploader.JobCancelled:
        # Suppress the exception that is raised when the Job is cancelled
        print("Job was cancelled")


async def run_job(job_dir, uploader, extra_env, extra_args=None):
    cmd = ["dirac-production-request-run-local", str(job_dir / "request.yaml")]
    if extra_args:
        cmd += extra_args
    dirac_log_path = job_dir / "DIRAC.log"
    print("Running", cmd)
    proc = await asyncio.create_subprocess_shell(
        f"set -euo pipefail; {shlex.join(cmd)} 2>&1 | tee {shlex.quote(str(dirac_log_path))}",
        executable="bash",
        env={**os.environ, **extra_env, "LBPRODRUN_PRMON_INTERVAL": "10"},
    )
    uploader.job_pid = proc.pid
    stdout, stderr = await proc.communicate()
    print("Command", cmd, "exited with", proc.returncode)
    return proc.returncode == 0


async def main(job_dir: Path, *, session: requests.Session, func=run_job):
    config = json.loads((job_dir / "config.json").read_text())

    uploader = log_uploader.LogUploader(job_dir, session, config)
    uploader.set_status("STARTED")

    success = True
    errors = []
    extra_env = {}
    async with AsyncExitStack() as stack:
        await stack.enter_async_context(uploader)

        uploader.set_status("RUNNING")

        if "data_pkg" in config:
            try:
                extra_env |= await stack.enter_async_context(
                    TestSpecificEnvironment(data_pkg=config["data_pkg"])
                )
            except Exception as e:
                print("Failed to checkout repository", e)
                errors.append("Failed to set up test environment")
                success = False

        # enable options dumping
        extra_env |= {"LBPRODRUN_OPTIONS_DUMP_DIR": "."}

        if success:
            print("Running job", func)
            success = await func(
                job_dir, uploader, extra_env, config.get("extra_test_cli_args")
            )
            uploader.set_status("VALIDATING")

    result = log_uploader.make_result(job_dir)
    if result.get("resource_usage", {}).get("peak_mem", 0) > 5 * 1024**3:
        result["errors"].append("Peak memory usage is too high")
        success = False
    result["errors"].extend(errors)

    if success:
        uploader.set_status("SUCCESS", result=result)
    else:
        uploader.set_status("FAILED", result=result)
        if (result or {}).get("recoverable"):
            uploader.retry_job()
            raise Exception(
                "Job failed but error was detected as recoverable", result["errors"]
            )


async def _handle_output(proc, stream_name, fp=None):
    stream = getattr(proc, stream_name)
    lines = []
    while line := await stream.readline():
        line = line.decode(errors="backslashreplace")
        print(f"{stream_name}: {line.rstrip()}")
        lines += [line]
        if fp:
            fp.write(line)
    return "".join(lines)


async def _setup_repository_dir(
    tmpdir: Path, project_name: str, repo_url: str, commit_sha: str
):
    repository_dir = tmpdir / project_name

    command = ["git", "clone", repo_url, repository_dir]
    print(f"Running: {command}")
    proc = await asyncio.create_subprocess_exec(
        *command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=120,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")

    command = ["git", "checkout", commit_sha]
    print(f"Running: {command}")
    proc = await asyncio.create_subprocess_exec(
        *command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=repository_dir
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=30,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")

    command = ["git", "lfs", "install", "--local"]
    print(f"Running: {command}")
    proc = await asyncio.create_subprocess_exec(
        *command,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        cwd=repository_dir,
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=60,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")

    command = ["git", "lfs", "pull"]
    print(f"Running: {command}")
    proc = await asyncio.create_subprocess_exec(
        *command,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE,
        cwd=repository_dir,
    )
    await asyncio.wait_for(
        asyncio.gather(
            _handle_output(proc, "stdout"), _handle_output(proc, "stderr"), proc.wait()
        ),
        timeout=60,
    )
    if proc.returncode != 0:
        raise RuntimeError(f"Exited with code {proc.returncode}")

    return repository_dir


def _setup_lbrun_environment(
    siteroot: Path, repository_dir: Path, version: str, setup_cmt: bool = True
) -> dict[str, str]:
    """Set up the fake siteroot for lb-run to use when testing.

    Args:
        siteroot: The directory where the lb-run fake siteroot will be created
        repository_dir: Repository directory
        setup_cmt: whether to use fallback hack for CMT-style projects
    """
    extra_env = {"CMAKE_PREFIX_PATH": str(siteroot)}
    fake_dbase = siteroot / "DBASE"
    fake_install_dir = fake_dbase / "AnalysisProductions" / version
    os.makedirs(fake_install_dir.parent)
    os.symlink(repository_dir, fake_install_dir)
    if setup_cmt:
        print("Applying fallback hacks for CMT style projects")
        extra_env["User_release_area"] = str(siteroot)
        LHCB_DBASE_ROOT = Path("/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE")
        for dname in os.listdir(LHCB_DBASE_ROOT):
            if dname == "AnalysisProductions":
                continue
            os.symlink(LHCB_DBASE_ROOT / dname, fake_dbase / dname)
    print(f"{extra_env=}")
    return extra_env


@asynccontextmanager
async def TestSpecificEnvironment(
    *, data_pkg: Optional[Dict[str, Any]] = None
) -> Dict[str, str]:
    extra_env = {}
    if data_pkg is None:
        yield extra_env
        return

    extra_env["LBAPI_HACK_XML_FILE_CATALOG"] = "x"

    with TemporaryDirectory() as tmp_dir:
        tmp_dir = Path(tmp_dir)

        repository_dir = await _setup_repository_dir(
            tmp_dir / "repo-clone",
            data_pkg["project_name"],
            data_pkg["repo_url"],
            data_pkg["commit_sha"],
        )

        dynamic_dir = repository_dir / "dynamic"
        dynamic_dir.mkdir(exist_ok=True, parents=True)
        for fn, content in data_pkg["dynamic_files"].items():
            fn_path = dynamic_dir / fn
            fn_path.parent.mkdir(exist_ok=True, parents=True)
            fn_path.write_text(content)

        extra_env |= _setup_lbrun_environment(
            tmp_dir / "env-hacks", repository_dir, data_pkg["version"]
        )
        yield extra_env


if __name__ == "__main__":
    parse_args()
