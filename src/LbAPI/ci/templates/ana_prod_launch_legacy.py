###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# pylint: skip-file
import argparse
import json
from datetime import datetime

import DIRAC
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise


def parse_args():
    parser = argparse.ArgumentParser(description="Launch requests in the LHCbDIRAC")
    parser.add_argument("--server-credentials", nargs=2, required=True)
    parser.add_argument("request_id", type=int)
    parser.add_argument("--plugin", type=str, required=True)
    parser.add_argument("--run-numbers", type=json.loads, required=True)
    parser.add_argument("--start-run", type=int, default=None)
    parser.add_argument("--end-run", type=int, default=None)
    parser.add_argument("--group-size", type=int, required=True)
    parser.add_argument("--append-name", type=str, default="1")
    parser.add_argument("--no-keep-running", action="store_true")
    args = parser.parse_args()

    DIRAC.initialize(host_credentials=args.server_credentials)
    launch_production(
        args.request_id,
        args.run_numbers,
        args.start_run,
        args.end_run,
        args.group_size,
        args.plugin,
        args.append_name,
        args.no_keep_running,
    )


def launch_production(
    request_id,
    run_numbers,
    start_run,
    end_run,
    group_size,
    plugin,
    append_name,
    no_keep_running,
):
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import (
        ProductionRequest,
    )
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
        ProductionRequestClient,
    )
    from LHCbDIRAC.ProductionManagementSystem.Utilities.ModelCompatibility import (
        configure_input,
    )
    from LHCbDIRAC.TransformationSystem.Client.TransformationClient import (
        TransformationClient,
    )

    prods = returnValueOrRaise(
        ProductionRequestClient().getProductionRequest([request_id])
    )
    data = prods[request_id]

    pr = ProductionRequest()
    pr.requestID = str(request_id)
    pr.appendName = append_name
    pr.visibility = "Yes"
    configure_input(pr, data, runs=run_numbers, startRun=start_run, endRun=end_run)
    pr.prodGroup = f"{pr.processingPass}/{json.loads(data['ProDetail'])['pDsc']}"[:50]
    pr.outConfigName = pr.configName
    pr.extraModulesList = ["FileUsage"]  # , "AnalyseFileAccess"]
    configure_steps(pr, data, group_size, input_plugin=plugin)
    transform_ids = returnValueOrRaise(pr.buildAndLaunchRequest())

    # If keep_running is False, we need to update the end date of the transformation
    # so files are only added if they exist at the time of launching the request
    if no_keep_running:
        tid = transform_ids[0]
        end_date = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        tc = TransformationClient()
        query = returnValueOrRaise(tc.getBookkeepingQuery(tid))
        new_query = query | {"EndDate": end_date}
        returnValueOrRaise(tc.deleteBookkeepingQuery(tid))
        returnValueOrRaise(tc.addBookkeepingQuery(tid, new_query))

    return transform_ids


def configure_steps(pr, data, input_group_size, input_plugin, *, skip_merging=False):
    pd = json.loads(data["ProDetail"])
    steps = {int(k[1 : -len("Step")]): v for k, v in pd.items() if k.endswith("Step")}
    pr.stepsList = [int(v) for k, v in sorted(steps.items())]
    pr.prodsTypeList = ["WGProduction"] if skip_merging else ["WGProduction", "Merge"]
    pr.stepsInProds = [list(range(1, len(pr.stepsList)))]
    if not skip_merging:
        pr.stepsInProds += [[len(pr.stepsList)]]

    pr.targets = ["", ""]
    pr.outputSEs = ["CERN-ANAPROD"]
    if not skip_merging:
        pr.outputSEs = ["CERN-BUFFER"] + pr.outputSEs
    pr.specialOutputSEs = [{}, {}]
    pr.removeInputsFlags = [False, True]
    pr.priorities = [2, 2]
    pr.inputs = [[], []]
    pr.inputDataPolicies = ["protocol", "download"]
    pr.outputFileSteps = [len(x) for x in pr.stepsInProds]
    pr.outputFileMasks = ["", ""]
    pr.multicore = ["True", "True"]
    pr.outputModes = ["Local", "Local"]
    pr.events = ["-1", "-1"]
    pr.ancestorDepths = [0, 0]
    pr.compressionLvl = ["LOW", "HIGH"]

    if input_plugin == "default":
        pr.plugins = ["APProcessing", "BySize"]
        pr.groupSizes = [input_group_size, 5]  # Sizes in GB when using BySize
    elif input_plugin == "by-run":
        pr.plugins = ["ByRunFileTypeSizeWithFlush", "ByRunFileTypeSizeWithFlush"]
        pr.groupSizes = [input_group_size, 5]
    else:
        raise NotImplementedError(input_plugin)
    pr.cpus = ["1000000", "1000000"]  # HS06 seconds

    pr.outputVisFlag = [{str(k): "N" for k in steps}]
    pr.outputVisFlag[0][str(max(steps) - 1 if skip_merging else max(steps))] = "Y"


if __name__ == "__main__":
    parse_args()
