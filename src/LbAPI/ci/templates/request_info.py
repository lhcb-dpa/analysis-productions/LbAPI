#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
from pathlib import Path

import DIRAC
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--server-credentials", nargs=2, required=True)
    parser.add_argument("--out", required=True)
    parser.add_argument("--return-states", action="store_true")
    parser.add_argument("--req_ids", nargs="+")
    args = parser.parse_args()

    DIRAC.initialize(host_credentials=args.server_credentials)
    req_infos = get_req_info(args.req_ids)

    req_state = {}
    for key in req_infos.keys():
        if args.return_states:
            req_state[key] = req_infos[key]["RequestState"]
        else:
            req_state[key] = req_infos[key]["RequestState"] in {"Done", "Rejected"}
    Path(args.out).write_text(json.dumps(req_state))


def get_req_info(req_ids: list):
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
        ProductionRequestClient,
    )

    pmc = ProductionRequestClient()
    ret = returnValueOrRaise(pmc.getProductionRequest(req_ids))
    return ret


if __name__ == "__main__":
    parse_args()
