#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
import io
import json
import os
import re
import shlex
import subprocess
import sys
import time
import xml.etree.ElementTree as ET
from collections import Counter
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from fnmatch import fnmatch
from pathlib import Path, PurePosixPath
from traceback import format_exc
from typing import Optional

import psutil
import requests
import zstandard


@dataclass
class LogFile:
    path: PurePosixPath
    name: str
    handle: io.IOBase
    buffer: bytes
    line_number: int = 0


class JobCancelled(Exception):
    pass


class LogUploader:
    def __init__(self, base: Path, session: requests.Session, config):
        self.base = base.absolute()
        self._session = session
        self._config = config
        self._do_finish = False
        self.files = {}
        self._job_dir = None
        self._log_data = []
        self._cancelled = False
        self.job_pid = None
        self._uploaded_data = set()
        self._upload_expires = datetime.now(tz=timezone.utc)

    async def __aenter__(self):
        self._task = asyncio.create_task(self._run())
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        self._do_finish = True
        await self._task
        self._do_upload(final=True)
        try:
            self._upload_output_data()
        except Exception as e:
            sys.stderr.write(f"Failed to upload output data: {format_exc(e)}\n")

    def _upload_output_data(self):
        if not self.job_dir:
            return
        if os.environ.get("LBAP_NO_UPLOAD_OUTPUT_DATA"):
            sys.stderr.write(
                "Skipping output data upload due to environment variable\n"
            )
            return

        sys.stderr.write("Uploading files from job directory to \n")
        result = make_result(local_dir=self.job_dir)
        # Upload the actual output data
        for step in result["steps"]:
            output_files = step["dirac"].get("bookkeeping_xml", {}).get("output", [])
            for output_file in output_files:
                self._upload_file(output_file["filename"])
        # Upload some ancillary files
        for pattern, compress in {
            ("*.zip", False),
            ("*.tck.json", False),
            ("*_dump.json", True),
            ("*_dump.py", True),
        }:
            for path in self.job_dir.glob(pattern):
                self._upload_file(path.name, compress=compress)

    def _have_upload_credentials(self):
        if self._upload_expires > datetime.now(tz=timezone.utc) + timedelta(minutes=5):
            return True

        r = self._send_request(
            self._session.get,
            "/ci/job/upload-credentials",
            data=None,
            attempts=5,
        )
        if r is None or r.status_code >= 400:
            sys.stderr.write("Failed to get credentials to upload output data\n")
            return False
        data = r.json()
        if not data["success"]:
            sys.stderr.write(
                "success=False when getting credentials to upload output data\n"
                f"Details: {data.get('detail')}\n"
            )
            return False
        self._upload_base_url = data["base_url"]
        self._upload_token = data["token"]
        self._upload_expires = datetime.fromisoformat(data["expires"])
        return True

    def _upload_file(self, filename: str, compress: bool = False) -> None:
        """
        Upload a file from the job directory. Optionally compresses the file using zstandard.

        Args:
            filename: Name of the file to be uploaded.
            compress: Whether to compress the file before uploading.
        """
        original_filename = filename
        original_path = self.job_dir / original_filename

        # Exit early if file was already uploaded or if upload credentials are missing
        if original_path in self._uploaded_data or not self._have_upload_credentials():
            return

        file_path = original_path  # This is the file that will eventually be uploaded

        if compress:
            # Create the compressed filename and path
            compressed_filename = f"{original_filename}.zst"
            compressed_path = self.job_dir / compressed_filename

            compressor = zstandard.ZstdCompressor(level=6)
            with open(original_path, "rb") as in_file, open(
                compressed_path, "wb"
            ) as out_file:
                compressor.copy_stream(in_file, out_file)

            file_path = compressed_path
            filename = compressed_filename  # update filename for constructing dest_path

        # Build destination URL and execute the upload command
        dest_path = f"{self._upload_base_url}{filename}{self._upload_token}"
        cmd = ["xrdcp", "--nopbar", str(file_path), dest_path]
        proc = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            check=False,
            text=True,
        )

        if proc.returncode == 0:
            sys.stderr.write(f"Uploaded file: {file_path}\n")
        else:
            sys.stderr.write(f"Failed to upload {file_path}:\n{proc.stdout}\n")

        # Mark the uploaded file in the set. If compressed, also mark the original file.
        self._uploaded_data.add(file_path)
        if compress:
            self._uploaded_data.add(original_path)

    @property
    def job_dir(self) -> Path | None:
        if self._job_dir is None:
            job_dirs = list(self.base.glob("Local_*_JobDir/"))
            if job_dirs:
                self._job_dir = job_dirs[0]
        return self._job_dir

    async def _run(self):
        while not self._do_finish:
            await asyncio.sleep(15)
            await asyncio.get_running_loop().run_in_executor(None, self._do_upload)

    def set_status(self, status, *, result=None):
        r = self._send_request(
            self._session.post,
            "/ci/job/status",
            params={"new_status": status},
            data=result,
        )
        if r is not None:
            if r.status_code < 400:
                sys.stderr.write(f"Status set to {status}\n")

    def retry_job(self):
        r = self._send_request(self._session.post, "/job/retry", data={})
        if r is not None and r.status_code < 400:
            sys.stderr.write(f"Job resubmitted as {r.json()['new_job_id']}\n")

    def _update_file_list(self):
        paths = [self.base / "DIRAC.log"]
        if self.job_dir:
            paths.extend(self.job_dir.glob("*.log"))
            paths.extend(self.job_dir.glob("prodConf_*.json"))
            paths.extend(self.job_dir.glob("prmon_*.txt"))
        for path in paths:
            if path in self.files or not path.exists():
                continue
            sys.stderr.write(f"Found new file: {path}\n")
            if fnmatch(path.name, "prodConf_*.json") and step_idx(path):
                result = make_result(local_dir=self.job_dir, skip_last=True)
                if not self._do_finish:
                    self.set_status("RUNNING", result=result)
            handle = path.open("rb")
            os.set_blocking(handle.fileno(), False)
            self.files[path] = LogFile(
                path=PurePosixPath(path.relative_to(self.base)),
                name=path.name,
                handle=handle,
                buffer=b"",
            )

    def _do_upload(self, *, final: bool = False):
        self._update_file_list()
        for file_info in self.files.values():
            self._log_data.extend(read_file(file_info, final=final))

        if not self._log_data:
            return
        sys.stderr.write(f"Sending {len(self._log_data)} lines\n")
        while self._log_data and not self._cancelled:
            chunk = self._log_data[:1000]
            r = self._send_request(
                self._session.post,
                "/ci/job/logs",
                data=chunk,
                attempts=20 if final else 1,
            )
            if r is not None:
                if r.status_code < 400:
                    sys.stderr.write("Sent successfully\n")
                    self._log_data = self._log_data[len(chunk) :]
                else:
                    sys.stderr.write(f"Error uploading logs: {r.text}\n")
                    return

    def _send_request(self, method, url, *, data, attempts=10, params=None):
        if self._cancelled:
            raise JobCancelled("Job already cancelled")
        for attempt in range(attempts):
            r = None
            try:
                r = method(
                    f"{self._config['base_url']}{url}",
                    headers={"Authorization": f"Bearer {self._config['token']}"},
                    params=params or {},
                    json=data,
                    timeout=60,
                )
            except requests.exceptions.ConnectionError as e:
                sys.stderr.write(f"Error connecting to LbAPI: {e}\n")
            if r is not None:
                if r.status_code == 410:
                    self._kill_job()
                    raise JobCancelled("Job cancelled")
                if r.status_code < 400:
                    break
                sys.stderr.write(
                    f"Error sending request to {url} {attempt=}: {r.text}\n"
                )
            time.sleep(attempt * 5)
        return r

    def _kill_job(self):
        sys.stderr.write("Killing job\n")
        self._cancelled = True
        if self.job_pid is None:
            return
        try:
            job = psutil.Process(self.job_pid)
        except psutil.NoSuchProcess:
            return
        for child in job.children(recursive=True):
            try:
                child.kill()
            except psutil.NoSuchProcess:
                pass
        try:
            job.kill()
        except psutil.NoSuchProcess:
            pass


def read_file(file_info: Path, *, final: bool = False):
    data = file_info.buffer + file_info.handle.read()
    lines = data.splitlines(keepends=True)
    if not lines:
        return
    if final or lines[-1].endswith((b"\n", b"\r\n", b"\r")):
        file_info.buffer = b""
    else:
        file_info.buffer = lines.pop(-1)
    for line in lines:
        yield {
            "line_number": file_info.line_number,
            "timestamp": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "path": str(file_info.path),
            "name": str(file_info.name),
            "data": line.decode(errors="backslashreplace").rstrip(),
        }
        file_info.line_number += 1


def step_idx(path: Path) -> Optional[int]:
    try:
        return int(path.with_suffix("").name.split("_")[-1])
    finally:
        pass


def gather_step_info(job_dir: Path, spec_path: Path):
    step_result = {"index": step_idx(spec_path), "gaudi": {}, "dirac": {}, "prmon": {}}

    step_result["time_start"] = (
        datetime.utcfromtimestamp(spec_path.stat().st_ctime)
        .replace(tzinfo=timezone.utc)
        .isoformat()
    )

    # If Gauss is the first step, parse the log to estimate the CPU time
    # without the initialisation overhead
    gauss_logs = list(job_dir.glob("Gauss_*_1.log"))
    if step_result["index"] == 1 and gauss_logs:
        step_result["gaudi"]["ChronoStatSvc"] = {}
        for line in gauss_logs[0].read_text().splitlines():
            match = re.search(
                r"UTC ([^:\s]+)[^\s]*\s+INFO Time User\s+: Tot=\s*(\d+(?:\.\d+)?)\s*\[(min|s)\]",
                line,
            )
            if match:
                name, time, unit = match.groups()
                step_result["gaudi"]["ChronoStatSvc"][name] = (
                    float(time) * {"s": 1, "min": 60}[unit]
                )

    # Parse the summary XML file
    spec = json.loads(spec_path.read_text())
    xml_summary_path = job_dir / spec["input"]["xml_summary_file"]
    if xml_summary_path.is_file():
        tree = ET.fromstring(xml_summary_path.read_text())
        step_result["gaudi"]["summary_xml"] = {
            "success": tree.findall("./success")[0].text == "True",
            "stat": [
                stat.attrib | {"value": float(stat.text)}
                for stat in tree.findall("./usage/stat")
            ],
            "input": [
                file.attrib | {"n_events": int(file.text)}
                for file in tree.findall("./input/file")
            ],
            "output": [
                file.attrib | {"n_events": int(file.text)}
                for file in tree.findall("./output/file")
            ],
        }
        for output in step_result["gaudi"]["summary_xml"]["output"]:
            if not output["name"].startswith("PFN:"):
                raise NotImplementedError(output)
            output_path = job_dir / output["name"][len("PFN:") :]
            if output_path.is_file():
                output["size"] = output_path.stat().st_size

    # Summarise the log file
    log_paths = list(job_dir.glob(f"*_{step_result['index']}.log"))
    if log_paths:
        cmd = (
            "import json;"
            "from pathlib import Path;"
            "from LbAPCommon.validators import count_log_messages;"
            f"n_warn, n_error, n_fatal = count_log_messages(Path('{log_paths[0]}').read_text());"
            "print(json.dumps({'warning': n_warn, 'error': n_error, 'fatal': n_fatal}))"
        )
        proc = subprocess.run(
            f". /cvmfs/lhcb.cern.ch/lib/LbEnv >/dev/null 2>&1 && python -c {shlex.quote(cmd)}",
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            check=False,
            text=True,
            shell=True,
        )
        if proc.returncode == 0:
            step_result["gaudi"]["log_summary"] = json.loads(proc.stdout)
        else:
            print(f"Failed to summarise log file {log_paths[0]}:\n{proc.stdout}")

    # Parse the prmon output
    prmon_paths = list(job_dir.glob(f"prmon_*_{step_result['index']}.txt"))
    if prmon_paths:
        headings, *data = [
            line.split("\t") for line in prmon_paths[0].read_text().splitlines()
        ]
        data = [[int(x) for x in row] for row in data]
        prmon_data = dict(zip(headings, list(zip(*data))))
        step_result["prmon"]["peak_mem"] = (
            max(map(sum, zip(prmon_data["rss"], prmon_data["swap"]))) * 1024
        )

    # Enrich the first step's list of input files with their sizes
    if step_result["index"] == 1 and step_result["gaudi"]["summary_xml"]["input"]:
        for input_file in step_result["gaudi"]["summary_xml"]["input"]:
            lfn = input_file["name"]
            if lfn.startswith("LFN:"):
                lfn = lfn[len("LFN:") :]
            proc = subprocess.run(
                ["dirac-dms-lfn-metadata", lfn],
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                check=False,
                text=True,
            )
            if proc.returncode == 0:
                assert "Failed" not in proc.stdout
                assert "Successful" in proc.stdout
                result = [
                    line.strip().split(" : ", 1)
                    for line in proc.stdout.split(lfn)[1].splitlines()
                ]
                lfn_info = {x[0]: x[1] for x in result if len(x) == 2}
                input_file["size"] = int(lfn_info["Size"])
            else:
                sys.stderr.write(
                    f"Failed to get LFN metadata for {lfn}:\n{proc.stdout}\n"
                )

    # Parse the bookkeeping XML file
    bk_xml_paths = list(job_dir.glob(f"bookkeeping_*_{step_result['index']}.xml"))
    if bk_xml_paths:
        tree = ET.fromstring(bk_xml_paths[0].read_text())
        step_result["dirac"]["bookkeeping_xml"] = {
            "exec_time": float(
                tree.find("./TypedParameter[@Name='ExecTime']").attrib["Value"]
            ),
            "job_start": tree.find("./TypedParameter[@Name='JobStart']").attrib[
                "Value"
            ],
            "job_end": tree.find("./TypedParameter[@Name='JobEnd']").attrib["Value"],
            "output": [
                {
                    "lfn": file.attrib["Name"],
                    "type": file.attrib["TypeName"],
                    "filename": Path(file.attrib["Name"]).name,
                    "size": int(
                        file.find("./Parameter[@Name='FileSize']").attrib["Value"]
                    ),
                }
                for file in tree.findall("./OutputFile")
                if file.attrib["TypeName"] != "LOG"
            ],
        }

    return step_result


def make_result(
    job_dir: Optional[Path] = None,
    *,
    local_dir: Optional[Path] = None,
    skip_last: bool = False,
) -> dict:
    result = {
        "steps": [],
        "errors": [],
        "warnings": [],
        "resource_usage": {},
        "recoverable": False,
    }
    if local_dir is None:
        local_dirs = list(job_dir.glob("Local_*_JobDir"))
        if local_dirs:
            local_dir = local_dirs[0]
    if local_dir:
        prod_conf_paths = sorted(local_dir.glob("prodConf_*.json"), key=step_idx)
        if skip_last:
            prod_conf_paths = prod_conf_paths[:-1]
        for spec_path in prod_conf_paths:
            try:
                result["steps"].append(gather_step_info(local_dir, spec_path))
            except Exception as e:
                result["warnings"].append(
                    f"Error while gathering step info from {spec_path}: {e}"
                )

    if not skip_last:
        result["resource_usage"]["exec_time"] = sum(
            x["dirac"].get("bookkeeping_xml", {}).get("exec_time", 0)
            for x in result["steps"]
        )
        mem_peaks = [
            x["prmon"]["peak_mem"] for x in result["steps"] if "peak_mem" in x["prmon"]
        ]
        if mem_peaks:
            result["resource_usage"]["peak_mem"] = max(mem_peaks)
        result["log_summary"] = dict(
            sum(
                [Counter(x["gaudi"].get("log_summary", {})) for x in result["steps"]],
                start=Counter(),
            )
        )

    if not skip_last and local_dir:
        # Check for recoverable errors
        for log_path in local_dir.glob("*.log"):
            message = check_recoverable_errors(log_path)
            if message:
                result["errors"].append(message)
                result["recoverable"] = True

    return result


RECOVERABLE_PATTERNS = [
    r"RuntimeError: failed to acquire lockfile .*/PyConf\.lock after \d+ attempts",
    r"lb-run:current host does not support platform",
    r"Could not write info to setgroups: Permission denied",
]
RECOVERABLE_PATTERNS = [re.compile(pattern) for pattern in RECOVERABLE_PATTERNS]


def check_recoverable_errors(log_path: Path) -> str | None:
    """Return a message if a recoverable error is found in the log file.

    If this returns None, any errors are considered unrecoverable.
    """
    with log_path.open("rt") as fh:
        for line in fh:
            for pattern in RECOVERABLE_PATTERNS:
                if pattern.search(line):
                    return f"Recoverable error in {log_path.name}"
