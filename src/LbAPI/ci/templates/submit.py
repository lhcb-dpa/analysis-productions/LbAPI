#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
import shlex
from pathlib import Path

import DIRAC
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--pipeline-type", type=str, required=True)
    parser.add_argument("--pipeline-id", type=int, required=True)
    parser.add_argument("--name", type=str, default="unnamed")
    parser.add_argument("job_script", type=Path)
    parser.add_argument("--platform", type=str, default="x86_64_v2-any")
    parser.add_argument("--extra-files", type=Path, nargs="+", default=[])
    parser.add_argument("--args", type=str, nargs="+", default=[])
    args = parser.parse_args()

    DIRAC.initialize(log_level=DIRAC.LogLevel.ERROR)
    submit_job(**vars(args))


def submit_job(
    pipeline_type: str,
    pipeline_id: int,
    name: str,
    platform: str,
    job_script: Path,
    extra_files: list[Path],
    args: list[str],
):
    from DIRAC.Interfaces.API.Dirac import Dirac
    from DIRAC.Interfaces.API.Job import Job

    for path in [job_script] + extra_files:
        if not path.is_file():
            raise FileNotFoundError(path)

    j = Job()
    j.setType("LbAPI")
    j.setName(f"LbAPI - {name[:50]}")
    j.setJobGroup(f"LbAPI:{pipeline_type}:{pipeline_id}")

    j.setExecutable(job_script.name, shlex.join(args))
    j.setInputSandbox([str(x) for x in [job_script] + extra_files])
    j._addParameter(
        j.workflow,
        "Platform",
        "JDL",
        platform,
        "Platform (host OS + micro arch)",
    )
    j.setCPUTime(20 * 60 * 60)
    # j.setTag(["/cvmfs/lhcbdev.cern.ch/"])
    # j.setOutputData(['output.tar.bz2'])
    j.setOutputSandbox(["StdOut", "StdErr", "DIRAC.log"])
    j.setBannedSites(
        [
            # Jobs get stuck in matched
            "DIRAC.SDumont.br",
            # Not a normal site
            "DIRAC.MareNostrum.es",
            # Generally seems to misbehave, probably networking issues to CERN
            "LCG.Beijing.cn",
        ]
    )

    dirac_id = int(returnValueOrRaise(Dirac().submitJob(j)))
    print(json.dumps({"success": True, "dirac_id": dirac_id}))


if __name__ == "__main__":
    parse_args()
