#!/usr/bin/env python
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
from datetime import datetime
from pathlib import Path

COLUMNS = [
    "RequestID",
    "RequestType",
    "RequestWG",
    "RequestName",
    "RequestType",
    "RequestAuthor",
    "ParentID",
    "EventType",
    "crTime",
]


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("request_type", choices=["Simulation"])
    parser.add_argument("author")
    parser.add_argument("--parent-id", default=0, type=int, required=False)
    parser.add_argument("--output", required=True, type=Path)
    parser.add_argument("--server-credentials", nargs=2, required=True)
    parser.add_argument("--all-states", action="store_true")
    args = parser.parse_args()

    import DIRAC

    DIRAC.initialize(host_credentials=args.server_credentials)

    result = main(args.request_type, args.author, args.parent_id, args.all_states)

    args.output.write_text(json.dumps(result))


def main(request_type, author, parent_id, all_states):
    from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
        ProductionRequestClient,
    )

    query = dict(RequestType=request_type, RequestAuthor=author)
    if not all_states:
        query["RequestState"] = "Submitted"
    result = returnValueOrRaise(
        ProductionRequestClient().getProductionRequestList(
            parent_id,  # Parent
            "RequestID",  # Sortby
            "DESC",  # Sort order
            0,  # Offset
            0,  # Max results
            query,
        )
    )
    results = []
    for row in result["Rows"]:
        row_data = {}
        for k in COLUMNS:
            if isinstance(row[k], datetime):
                row_data[k] = row[k].isoformat()
            else:
                row_data[k] = row[k]
        if row["ProDetail"]:
            row_data["FullProcPass"] = [row["SimCondition"]] + [
                v
                for k, v in sorted(json.loads(row["ProDetail"]).items())
                if k.endswith("Pass")
            ]
        results.append(row_data)
    return results


if __name__ == "__main__":
    parse_args()
