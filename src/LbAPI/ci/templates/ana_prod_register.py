#!/usr/bin/env python
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
import logging
import os
import re

import DIRAC
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise

logger = logging.getLogger(__name__)


def getAutoTags(pID, bkPath):
    from LHCbDIRAC.BookkeepingSystem.Client.BKQuery import BKQuery

    queryDict = BKQuery().buildBKQuery(bkPath)
    autoTags = {}
    autoTags["config"] = queryDict["ConfigName"]
    if "MagDown" in queryDict["ConditionDescription"]:
        autoTags["polarity"] = "MagDown"
    elif "MagUp" in queryDict["ConditionDescription"]:
        autoTags["polarity"] = "MagUp"
    else:
        logger.warn(
            "Failed to handle ConditionDescription in %s for request ID %s",
            bkPath,
            pID,
        )
    autoTags["eventtype"] = queryDict["EventType"]
    match = re.match(r"^(?:20|[A-Za-z]+)(\d\d)$", queryDict["ConfigVersion"])
    if match:
        autoTags["datatype"] = f"20{match.groups()[0]}"
    elif queryDict["ConfigVersion"] not in ["Dev"]:
        logger.warn(
            "Failed to handle ConfigVersion in %s for request ID %s",
            bkPath,
            pID,
        )
    return autoTags


def parse_args():
    parser = argparse.ArgumentParser(description="Register requests in LHCbDIRAC")
    parser.add_argument("--server-credentials", nargs=2, required=True)
    parser.add_argument("--mr-url", type=str, required=True)
    parser.add_argument("--issue-url", type=str, required=True)
    parser.add_argument("--version", type=str, required=True)
    parser.add_argument("--wg", type=str, required=True)
    parser.add_argument("--analysis", type=str, required=True)
    parser.add_argument("--prod-ids", type=json.loads, required=True)
    parser.add_argument("--extra-tags", type=json.loads, required=False, default={})
    parser.add_argument("--maybe-owners", type=json.loads, default={})
    args = parser.parse_args()

    DIRAC.initialize(host_credentials=args.server_credentials)
    register_requests(
        args.mr_url,
        args.issue_url,
        args.version,
        args.wg,
        args.analysis,
        args.prod_ids,
        args.maybe_owners,
        args.extra_tags,
    )


# TODO this should really use the original YAML
def get_request_filetypes(prod_request):
    proDetail = json.loads(prod_request["ProDetail"])
    return {
        oft.strip()
        for key, value in proDetail.items()
        if key.endswith("Pass") and value == "merged"
        for oft in proDetail[key.replace("Pass", "OFT")].split(",")
    }


def register_requests(
    mr_url, issue_url, version, wg, analysis, prod_ids, maybe_owners, extra_tags
):
    from LHCbDIRAC.ProductionManagementSystem.Client.AnalysisProductionsClient import (
        AnalysisProductionsClient,
    )
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
        ProductionRequestClient,
    )

    apc = AnalysisProductionsClient()
    prc = ProductionRequestClient()

    existing_productions = returnValueOrRaise(
        apc.getProductions(
            wg=wg,
            analysis=analysis,
            version=version,
            with_lfns=False,
            with_pfns=False,
            with_transformations=False,
        )
    )
    if existing_productions:
        prod_ids = prod_ids.copy()
        for prod in existing_productions:
            if str(prod["request_id"]) in prod_ids:
                logger.info(f"Production {prod['request_id']} already exists, skipping")
                del prod_ids[str(prod["request_id"])]
        if not prod_ids:
            logger.info(
                f"Production {wg}/{analysis}/{version} already exists, skipping"
            )
            return

    prods_info = returnValueOrRaise(prc.getProductionRequest(list(prod_ids)))
    prods_info = {str(k): v for k, v in prods_info.items()}

    requests = []
    for prod_id, name in prod_ids.items():
        simCondDetail = json.loads(prods_info[prod_id]["SimCondDetail"])
        output_filetypes = get_request_filetypes(prods_info[prod_id])
        if not output_filetypes:
            raise NotImplementedError(prod_id)

        # register one sample per filetype
        for filetype in output_filetypes:
            filetype_suffix = filetype.lower().removesuffix(".root")
            sample = dict(
                request_id=prod_id,
                filetype=filetype,
                name=f"{name}_{filetype_suffix}" if len(output_filetypes) > 1 else name,
                version=version,
                wg=wg,
                analysis=analysis,
            )
            sample["validity_start"] = prods_info[prod_id]["crTime"]
            sample["extra_info"] = {"transformations": []}
            sample["extra_info"]["merge_request"] = mr_url
            sample["extra_info"]["jira_task"] = issue_url

            productionInputQuery = os.path.join(
                "/",
                simCondDetail["configName"],
                simCondDetail["configVersion"],
                prods_info[prod_id]["SimCondition"],
                simCondDetail["inProPass"],
                prods_info[prod_id]["EventType"],
                simCondDetail["inFileType"],
            )

            autoTags = getAutoTags(prod_id, productionInputQuery)
            sample["auto_tags"] = [dict(name=k, value=v) for k, v in autoTags.items()]
            sample["auto_tags"].append(dict(name="filetype", value=filetype))

            requests.append(sample)

    returnValueOrRaise(apc.registerRequests(requests))
    if not returnValueOrRaise(apc.getOwners(wg=wg, analysis=analysis)):
        if maybe_owners:
            returnValueOrRaise(
                apc.setOwners(wg=wg, analysis=analysis, owners=sorted(maybe_owners))
            )
            print("Set owners to be", maybe_owners)
        else:
            print("No potential owners")


if __name__ == "__main__":
    parse_args()
