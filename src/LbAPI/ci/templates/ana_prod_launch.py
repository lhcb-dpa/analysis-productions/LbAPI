###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# pylint: skip-file
import argparse
import json
from datetime import datetime
from pathlib import Path

import DIRAC
import yaml
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from LHCbDIRAC.ProductionManagementSystem.Utilities.Models import DataProduction


def parse_args():
    parser = argparse.ArgumentParser(description="Launch requests in the LHCbDIRAC")
    parser.add_argument(
        "--server-credentials",
        nargs=2,
    )
    parser.add_argument("request_id", type=int)
    parser.add_argument("request_yaml", type=Path)
    parser.add_argument("--keep-running", action="store_true")
    parser.add_argument("--append-name", type=str, default="1")
    parser.add_argument("--input-group-size", type=int, default=-1)
    args = parser.parse_args()

    DIRAC.initialize(host_credentials=args.server_credentials)
    launch_production(
        args.request_id,
        args.request_yaml,
        args.keep_running,
        args.append_name,
        args.input_group_size if args.input_group_size > 0 else None,
    )


def launch_production(
    request_id,
    request_yaml,
    keep_running,
    append_name,
    input_group_size,
):
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import (
        ProductionRequest,
    )
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequestClient import (
        ProductionRequestClient,
    )
    from LHCbDIRAC.ProductionManagementSystem.Utilities.ModelCompatibility import (
        configure_input,
    )
    from LHCbDIRAC.ProductionManagementSystem.Utilities.Models import (
        parse_obj as parse_request_dict,
    )
    from LHCbDIRAC.TransformationSystem.Client.TransformationClient import (
        TransformationClient,
    )

    request_yaml = yaml.safe_load(request_yaml.read_text())

    prods = returnValueOrRaise(
        ProductionRequestClient().getProductionRequest([request_id])
    )
    data = prods[request_id]

    if len(request_yaml) != 1:
        raise NotImplementedError()
    request = parse_request_dict(request_yaml[0])

    pr = ProductionRequest()
    pr.requestID = str(request_id)
    pr.appendName = append_name
    pr.visibility = "Yes"
    pr.disableOptionalCorrections = True
    pr.bkSampleMax = request.input_dataset.launch_parameters.sample_max_md5
    pr.bkSampleSeed = request.input_dataset.launch_parameters.sample_seed_md5
    configure_input(
        pr,
        data,
        runs=request.input_dataset.launch_parameters.run_numbers,
        startRun=request.input_dataset.launch_parameters.start_run,
        endRun=request.input_dataset.launch_parameters.end_run,
    )
    pr.prodGroup = f"{pr.processingPass}/{json.loads(data['ProDetail'])['pDsc']}"[:50]
    pr.outConfigName = pr.configName
    pr.extraModulesList = ["FileUsage"]  # , "AnalyseFileAccess"]
    configure_steps(pr, data, request, input_group_size=input_group_size)
    ourStepsInProds = pr.stepsInProds
    transform_ids = returnValueOrRaise(pr.buildAndLaunchRequest())

    if ourStepsInProds != pr.stepsInProds:
        raise ValueError(
            "Something went horribly wrong: stepsInProds was changed after buildAndLaunchRequest. "
            "Check that applyOptionalCorrections didn't meddle in our business!\n"
            f"The following transformations should be stopped and cleaned: {transform_ids!r}"
        )

    # If keep_running is False, we need to update the end date of the transformation
    # so files are only added if they exist at the time of launching the request
    new_query = None
    tid = transform_ids[0]
    if not keep_running:
        end_date = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        new_query = (new_query or {}) | {"EndDate": end_date}

    if (
        request.input_dataset.launch_parameters.sample_max_md5
        and request.input_dataset.launch_parameters.sample_seed_md5
    ):
        new_query = (new_query or {}) | {
            "SampleMax": request.input_dataset.launch_parameters.sample_max_md5,
            "SampleSeedMD5": request.input_dataset.launch_parameters.sample_seed_md5,
        }

    if new_query is not None:
        tc = TransformationClient()
        new_query = returnValueOrRaise(tc.getBookkeepingQuery(tid)) | new_query
        returnValueOrRaise(tc.deleteBookkeepingQuery(tid))
        returnValueOrRaise(tc.addBookkeepingQuery(tid, new_query))
        print(f"Modified input query for transformation {tid} to: {new_query}")
    return transform_ids


def find_previousProds(trf_idx, request: DataProduction, stepsInProds):
    trf = request.submission_info.transforms[trf_idx]
    if trf_idx == 0:
        return None
    from_step_idxs = {x.step_idx for x in request.steps[trf.steps[0]].input}
    if len(from_step_idxs) != 1:
        raise NotImplementedError("Please don't")
    from_step_idx = from_step_idxs.pop()
    for trf_idx_, stepsInProd in enumerate(stepsInProds):
        if from_step_idx + 1 in stepsInProd:
            return trf_idx_ + 1
    raise NotImplementedError("Something went very wrong")


def configure_steps(pr, data, request: DataProduction, input_group_size=None):
    pd = json.loads(data["ProDetail"])
    steps_stepIDs = {
        int(k[1 : -len("Step")]): int(v) for k, v in pd.items() if k.endswith("Step")
    }
    pr.stepsList = [int(v) for k, v in sorted(steps_stepIDs.items())]

    pr.previousProds = []
    for trf_idx, trf in enumerate(request.submission_info.transforms):
        pr.prodsTypeList += [trf.type]
        # which stepIDs are in this transform
        pr.stepsInProds += [
            [x + 1 for x in trf.steps]  # this is actually 1-indexed in DIRAC 😱😭
        ]
        # where do the outputs get uploaded to
        pr.outputSEs += [trf.output_se]
        # specifies how we access our input files in the transform
        pr.inputDataPolicies += [trf.input_data_policy]
        # the input file plugin to use for the transform
        pr.plugins += [trf.input_plugin]
        # Just the number of steps in this transform.
        pr.outputFileSteps += [len(trf.steps)]
        # the input file group size to the transform.
        if trf.input_plugin not in ["APProcessingByFileTypeSize"]:
            pr.groupSizes += [
                trf.group_size if input_group_size is None else input_group_size
            ]
        else:
            pr.groupSizes += [trf.group_size]
        # whether the output is going to be saved?
        output_visibilities = {}
        for trf_step_idx in trf.steps:
            visibilities = {x.visible for x in request.steps[trf_step_idx].output}
            if len(visibilities) != 1:
                raise NotImplementedError("No idea how to make this work...")
            output_visibilities[str(trf_step_idx + 1)] = {True: "Y", False: "N"}[
                visibilities.pop()
            ]
        pr.outputVisFlag.append(output_visibilities)

        # previousProds is a list of indices (ONE-INDEXED!!!)
        # each pointing to the transformation to take input from
        # referenced in the stepsInProds list.
        # we need to do this otherwise the transforms will not find the correct
        # output to use as input.
        # e.g.
        # 1 = take input from (1-1=0)th transform referenced in stepsInProds
        # 2 = take input from (2-1=1)st transform referenced in stepsInProds
        # 3 = take input from (3-1=2)st transform referenced in stepsInProds
        # n = take input from (n-1)th transform referenced in stepsInProds
        pr.previousProds.append(find_previousProds(trf_idx, request, pr.stepsInProds))
        if trf_idx > 0:
            pr.bkQueries.append("fromPreviousProd")

        # left blank
        pr.targets += [""]
        # left blank
        pr.specialOutputSEs += [{}]
        # whether the input files should be removed when used by
        # the transform - need to be very careful with this
        pr.removeInputsFlags += [trf.remove_inputs_flags]
        # leave as is
        pr.priorities += [trf.priority]
        # left blank
        pr.inputs += [[]]
        # ???
        pr.outputFileMasks += [trf.output_file_mask]
        # left as is
        pr.multicore += [str(trf.multicore)]
        # ???
        pr.outputModes += [trf.output_mode]
        # we process all events
        pr.events += [trf.events]
        # leave as is as not relevant
        pr.ancestorDepths += [trf.ancestor_depth]
        # compression is not handled at this point but in our
        # application config
        pr.compressionLvl += ["LOW"]
        # left as is
        pr.cpus += [trf.cpu]
        # processors
        pr.processors += [trf.processors]

    # DO NOT REMOVE THIS!
    pr.removeInputsFlags[0] = False
    assert not pr.removeInputsFlags[0], "you are playing with a very large fire"
    assert len(pr.previousProds) == len(pr.prodsTypeList), "you are playing with fire"


if __name__ == "__main__":
    parse_args()
