#!/usr/bin/env python
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json

import uproot


def dump_tree_info(eos_path):
    with uproot.open({eos_path: None}) as rf:
        return json.dumps(
            {
                ttree_key: [branch for branch in rf[ttree_key].keys()]
                for ttree_key in rf.keys()
                if rf.classnames()[ttree_key] == "TTree"
            }
        )


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--eos-path", required=True)
    args = parser.parse_args()

    print(dump_tree_info(args.eos_path))


if __name__ == "__main__":
    parse_args()
