###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging

from LbAPI.ci import actions
from LbAPI.ci.queries import create_pipeline
from LbAPI.enums import ALLOWED_JOB_TRANSITIONS, JobStatus, WebhookType
from LbAPI.settings import (
    gitlab,
)

from .actions.common import make_pipeline_url

logger = logging.getLogger(__name__)


async def merge_request_hook(project_type, project_id, body):
    mr_id = body["object_attributes"]["iid"]
    commit_info = body["object_attributes"]["last_commit"]
    commit_sha = commit_info["id"]
    action = body["object_attributes"]["action"]
    logger.debug("Action for %s!%s is %s", project_id, mr_id, action)
    if action in ["open", "update"]:
        from LbAPI.celery import start_pipeline

        pipeline_id = await create_pipeline(
            project_id,
            commit_sha,
            commit_info["message"],
            mr_id,
            body["user"]["username"],
        )
        if pipeline_id is None:
            return "Pipeline already exists"
        logger.info(
            "Created pipeline %s for MR %s (%s)",
            pipeline_id,
            mr_id,
            commit_sha,
        )
        set_commit_status(
            project_type,
            project_id,
            commit_sha,
            pipeline_id,
            "pending",
        )
        start_pipeline.delay(project_id, commit_sha, mr_id)
        return f"Created {pipeline_id}"
    elif action == "merge":
        target_branch = body["object_attributes"]["target_branch"]
        target_default_branch = body["object_attributes"]["target"]["default_branch"]
        if target_branch != target_default_branch:
            return f"Not submitting as target is {target_branch}"
        from LbAPI.celery import pre_submission_tasks

        logger.debug(
            "Submitting productions for %s %s !%s as %s",
            project_id,
            commit_sha,
            mr_id,
            body["user"]["username"],
        )
        task = pre_submission_tasks.delay(
            project_id, commit_sha, mr_id, body["user"]["username"], dry_run=False
        )

        return f"Submitting as {task}"


async def note_hook(body):
    supported_note_types = {
        "MergeRequest": "merge_request",
        "Issue": "issue",
    }

    project_id = body["project_id"]

    noteable_type = body["object_attributes"]["noteable_type"]
    if noteable_type not in supported_note_types.keys():
        logger.debug(
            "Note for %s is of type %s, not supported",
            project_id,
            noteable_type,
        )
        return "Note hook not handled"

    note = body["object_attributes"]["note"]
    logger.debug("Note for %s of type %s is %s", project_id, noteable_type, note)
    note_body = None

    if "@lhcbdpa" in note:
        username = body["user"]["username"]
        note_body = f"""Hi @{username}! This is an automated, unmonitored account.
        If you need a hand with something, please get in contact with the liaison(s) for your working group.
        They can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/LHCbWGLiaisons).
        """
    else:
        note_body = await actions.handle_note_hook(body)

    if note_body is None:
        return "No note action required"

    project = gitlab.api.projects.get(project_id)
    discussion_id = body["object_attributes"]["discussion_id"]

    iid = body[supported_note_types[noteable_type]]["iid"]

    resource = (
        project.mergerequests.get(iid)
        if noteable_type == "MergeRequest"
        else project.issues.get(iid)
    )
    resource_d = resource.discussions.get(discussion_id)
    resource_d.notes.create({"body": note_body})

    return "Note event handled"


def set_commit_status(
    project_type: WebhookType,
    project_id: int,
    commit_sha: str,
    pipeline_id: int,
    state: str,
    *,
    description: str = "",
):
    from LbAPI.celery import update_discussion

    if state not in ["pending", "running", "success", "failed", "canceled"]:
        raise ValueError(f"Invalid commit status {state}")

    status = {
        "name": "LbAPI Pipeline",
        "state": state,
        "description": description,
        "context": "AnalysisProductions",
    }
    if pipeline_id:
        status["target_url"] = make_pipeline_url(project_type, pipeline_id)
    logger.info("Creating commit status for %s %s: %s", project_id, commit_sha, status)

    if gitlab.token is not None:
        try:
            project = gitlab.api.projects.get(project_id)
            commit = project.commits.get(commit_sha)
            commit_status = commit.statuses.create(status)
            logger.info("Created commit status as %s", commit_status.to_json())
        except Exception:
            logger.exception("Failed to create commit status")

        if pipeline_id:
            update_discussion.delay(pipeline_id)
    else:
        logger.warning("No GitLab token set, cannot set commit status")


def maybe_finish_pipeline(
    project_type, project_id, commit_sha, mr_id, pipeline_id, job_statuses
):
    if any(ALLOWED_JOB_TRANSITIONS[k] for k, v in job_statuses.items() if v):
        # There are still jobs running
        return
    if set(job_statuses) == {JobStatus.SUCCESS}:
        from LbAPI.celery import pre_submission_tasks

        # Do a dry run submission before setting the pipeline to success
        pre_submission_tasks.delay(project_id, commit_sha, mr_id, None, dry_run=True)
        return
    elif JobStatus.CANCELLED in job_statuses:
        state = "canceled"
    else:
        state = "failed"
    logger.info("Setting pipeline %s to %s: %s", pipeline_id, state, repr(job_statuses))
    set_commit_status(project_type, project_id, commit_sha, pipeline_id, state)


def maybe_restart_pipeline(
    project_type, project_id, commit_sha, pipeline_id, job_statuses
):
    if sum(v for k, v in job_statuses.items() if ALLOWED_JOB_TRANSITIONS[k]) == 1:
        # There is exactly one job running
        set_commit_status(project_type, project_id, commit_sha, pipeline_id, "running")
