###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging
from datetime import datetime, timezone
from typing import Optional

import sqlalchemy

from LbAPI.database import SessionLocal, retryable_query
from LbAPI.database.webhooks import CIRun, Job, Pipeline, ProjectWebhook
from LbAPI.enums import (
    ALLOWED_JOB_TRANSITIONS,
    CIRunStatus,
    JobStatus,
    PipelineStatus,
    WebhookType,
)
from LbAPI.utils import Token

logger = logging.getLogger(__name__)
MAX_AUTO_RETRIES = 3


async def _update_status(session, table, row_id, status, append_log):
    query = sqlalchemy.select(table.status_info).where(table.id == row_id)
    status_info = (await session.execute(query)).one()[0]

    timestamp = datetime.now(tz=timezone.utc).isoformat()
    if isinstance(append_log, list):
        append_log = "\n".join(append_log)

    status_info += f"[{timestamp}]\n{append_log}\n"
    if len(status_info) >= 2**21:
        status_info = (
            status_info[: 2**21] + "\n... truncated ...\n" + status_info[-(2**21) :]
        )

    result = await session.execute(
        sqlalchemy.update(table)
        .where(table.id == row_id)
        .values(status=status, status_info=status_info)
    )
    if result.rowcount != 1:
        raise NotImplementedError(
            f"Failed to update status for {table.__name__} {row_id}"
        )


@retryable_query()
async def create_webhook(project_id: int, name: str, type: WebhookType) -> str:
    """Create a new webhook for the given project."""
    raw_token, token = Token.generate("webhook")
    async with SessionLocal.begin() as session:
        project = ProjectWebhook(
            project_id=project_id, name=name, type=type, secret=token.hashed_token
        )
        session.add(project)
        await session.commit()
    return raw_token


@retryable_query()
async def create_pipeline(
    project_id: int, commit_sha: str, commit_message: str, mr_id: int, author: str
) -> Optional[int]:
    """Create a new pipeline for the given project and commit.

    Returns the pipeline id if the pipeline was created, None otherwise.
    If the pipeline is created a new celery task is started to process it.
    """
    async with SessionLocal.begin() as session:
        pipeline = Pipeline(
            project_id=project_id,
            commit_sha=commit_sha,
            commit_message=commit_message,
            mr_id=mr_id,
            author=author,
        )
        result = await session.execute(
            sqlalchemy.select(Pipeline.id)
            .where(Pipeline.project_id == project_id)
            .where(Pipeline.commit_sha == commit_sha)
            .where(Pipeline.mr_id == mr_id)
        )
        if result.first() is not None:
            return
        session.add(pipeline)
        await session.flush()
        pipeline_id = pipeline.id
        await session.commit()
    return pipeline_id


@retryable_query()
async def mark_pipeline_failed(pipeline_id: int, append_log: str):
    async with SessionLocal.begin() as session:
        await _update_status(
            session, Pipeline, pipeline_id, PipelineStatus.ERROR, append_log
        )
        await session.commit()


@retryable_query()
async def insert_ci_runs(pipeline_id: int, ci_run_names: list[str]):
    async with SessionLocal.begin() as session:
        ci_runs = [CIRun(pipeline_id=pipeline_id, name=name) for name in ci_run_names]

        await _update_status(
            session, Pipeline, pipeline_id, PipelineStatus.PROCESSED, "Success"
        )
        session.add_all(ci_runs)
        await session.commit()


@retryable_query()
async def mark_ci_run_failed(ci_run_id: int, append_log: str):
    async with SessionLocal.begin() as session:
        await _update_status(session, CIRun, ci_run_id, CIRunStatus.ERROR, append_log)
        await session.commit()


@retryable_query()
async def mark_all_ci_runs_failed(pipeline_id: int, append_log: str):
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(CIRun.id).where(CIRun.pipeline_id == pipeline_id)
        for (ci_run_id,) in await session.execute(query):
            await _update_status(
                session, CIRun, ci_run_id, CIRunStatus.ERROR, append_log
            )
        await session.commit()


@retryable_query()
async def reset_ci_run(pipeline_id: int, username: str):
    """Reset all CI runs for the given pipeline to the PROCESSED status.

    This is done so we can retry the submission dry-run.
    """
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(CIRun.id).where(CIRun.pipeline_id == pipeline_id)
        for (ci_run_id,) in await session.execute(query):
            await _update_status(
                session,
                CIRun,
                ci_run_id,
                CIRunStatus.PROCESSED,
                f"Submission dry-run retry from {username}",
            )
        await session.commit()


@retryable_query()
async def mark_ci_run_ready_to_submit(pipeline_id: int):
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(CIRun.id).where(CIRun.pipeline_id == pipeline_id)
        for (ci_run_id,) in await session.execute(query):
            await _update_status(
                session,
                CIRun,
                ci_run_id,
                CIRunStatus.READY_TO_SUBMIT,
                "Dry-run submission successful",
            )
        await session.commit()


@retryable_query()
async def insert_jobs(ci_run_id, ci_run_log, job_specs) -> dict[int, str]:
    async with SessionLocal.begin() as session:
        query = sqlalchemy.update(Job)
        query = query.where(Job.ci_run_id == ci_run_id)
        query = query.where(Job.name.in_(job_specs))
        query = query.where(Job.outdated.is_(False))
        # query = query.where(Job.request_id.is_(None))
        query = query.values(outdated=True)
        result = await session.execute(query)
        logger.debug("Marked %s rows as outdated", result.rowcount)

        jobs = [
            Job(ci_run_id=ci_run_id, name=job_name, spec=job_spec)
            for job_name, job_spec in job_specs.items()
        ]

        await _update_status(
            session, CIRun, ci_run_id, CIRunStatus.PROCESSED, ci_run_log
        )
        session.add_all(jobs)
        await session.flush()
        jobs = {job.id: job.name for job in jobs}
        await session.commit()
    return jobs


@retryable_query()
async def set_job_status(job_id: int, new_status: JobStatus, extra_values=None):
    from LbAPI.ci.webhook import maybe_finish_pipeline

    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(
            ProjectWebhook.type,
            Pipeline.project_id,
            Pipeline.commit_sha,
            Pipeline.mr_id,
            CIRun.pipeline_id,
            Job.status,
            Job.outdated,
        )
        query = query.where(ProjectWebhook.project_id == Pipeline.project_id)
        query = query.where(Pipeline.id == CIRun.pipeline_id)
        query = query.where(Job.ci_run_id == CIRun.id)
        query = query.where(Job.id == job_id)
        project_type, project_id, commit_sha, mr_id, pipeline_id, status, outdated = (
            await session.execute(query)
        ).one()
        if status == new_status:
            return
        if outdated:
            raise NotImplementedError("Cannot set status on job marked outdated")
        if new_status not in ALLOWED_JOB_TRANSITIONS[status]:
            raise NotImplementedError(
                f"Cannot transition from {status} to {new_status}"
            )

        query = sqlalchemy.update(Job)
        query = query.where(Job.id == job_id)
        query = query.values(status=new_status, **(extra_values or {}))
        await session.execute(query)

        status_summary = await _make_status_summary(session, pipeline_id)
        logger.debug("Status summary after setting job status: %s", status_summary)

        await session.commit()

    maybe_finish_pipeline(
        project_type, project_id, commit_sha, mr_id, pipeline_id, status_summary
    )


@retryable_query()
async def resubmit_job(job_id: int, *, automatic: bool = False) -> Optional[int]:
    from LbAPI.ci.webhook import maybe_restart_pipeline

    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(
            ProjectWebhook.type,
            Pipeline.project_id,
            Pipeline.commit_sha,
            CIRun.pipeline_id,
            Job.name,
            Job.ci_run_id,
            Job.status,
            Job.spec,
            Job.n_retries,
        )
        query = query.where(ProjectWebhook.project_id == Pipeline.project_id)
        query = query.where(Pipeline.id == CIRun.pipeline_id)
        query = query.where(CIRun.id == Job.ci_run_id)
        query = query.where(Job.id == job_id)
        ref = (await session.execute(query)).one()
        project_type, project_id, commit_sha, pipeline_id = ref[:4]
        if ref.status not in [JobStatus.FAILED, JobStatus.CANCELLED]:
            raise ValueError(f"Cannot resubmit job {job_id} in status {ref.status}")
        if automatic and ref.n_retries >= MAX_AUTO_RETRIES:
            return None

        query = sqlalchemy.update(Job)
        query = query.where(Job.ci_run_id == ref.ci_run_id)
        query = query.where(Job.id == job_id)
        query = query.where(Job.outdated.is_(False))
        query = query.values(outdated=True)
        result = await session.execute(query)
        if result.rowcount != 1:
            raise ValueError(f"Failed to mark job {job_id} as outdated")
        logger.debug("Marked %s rows as outdated", result.rowcount)

        job = Job(
            ci_run_id=ref.ci_run_id,
            name=ref.name,
            spec=ref.spec,
            n_retries=ref.n_retries + 1 if automatic else 0,
        )
        session.add(job)
        await session.flush()
        new_job_id = job.id
        logger.debug("Resubmitting job %s as %s", job_id, new_job_id)

        status_summary = await _make_status_summary(session, ref.pipeline_id)
        logger.debug("Status summary after setting job status: %s", status_summary)

        await session.commit()

    maybe_restart_pipeline(
        project_type, project_id, commit_sha, pipeline_id, status_summary
    )
    return new_job_id


async def _make_status_summary(session, pipeline_id):
    query = sqlalchemy.select(
        Job.status, sqlalchemy.func.count(Job.id).label("status_count")
    )
    query = query.where(CIRun.pipeline_id == pipeline_id)
    query = query.where(CIRun.id == Job.ci_run_id)
    query = query.where(Job.outdated.is_(False))
    query = query.group_by(Job.status)
    query = query.with_for_update(read=True)
    return dict((await session.execute(query)).all())
