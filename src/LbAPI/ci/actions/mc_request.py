###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

__all__ = (
    "find_ciruns",
    "find_jobs",
    "make_requests",
    "issue_job_table",
    "do_note_hook",
    "make_mr_summary_info",
)

import argparse
import asyncio
import logging
import re
from collections import Counter, defaultdict
from copy import deepcopy
from pathlib import Path, PurePosixPath
from tempfile import TemporaryDirectory
from textwrap import dedent
from typing import Any

import yaml
from gitlab import GitlabGetError

from LbAPI.ci.actions.submission import clean_repo, fix_request
from LbAPI.ci.actions.submission import post_submission_tasks as post_submit
from LbAPI.dirac import DownloadedProxyFile
from LbAPI.settings import (
    gitlab,
    settings,
)
from LbAPI.utils import get_package_version, run

from .common import ProdStates, sign_productions

SIMULATION_EXPERTS_GROUP_ID = 174547
CONVENERS_GROUP_ID = 202512

logger = logging.getLogger(__name__)


async def find_ciruns(files: list[dict[str, str]]) -> set[str]:
    analyses = set()
    for file_info in files:
        if file_info["type"] != "blob":
            continue
        path = PurePosixPath(file_info["path"])
        if len(path.parts) < 2 or path.parts[0].startswith("."):
            continue
        analyses.add(path.parts[0])
    return analyses


def construct_bkk_path(request: dict[str, Any]) -> str:
    # construct BKK query path to check if request already exists
    bkk_path = Path("/MC") / request["mc_config_version"] / request["sim_condition"]

    for proc_pass in request["steps"]:
        if not proc_pass["visible"]:
            continue

        bkk_path /= proc_pass["processing_pass"]

    # Assume file type comes from last processing pass
    file_type = proc_pass["output"][0]["type"]
    bkk_path = bkk_path / request["event_types"][0]["id"] / file_type
    return str(bkk_path)


async def get_previous_bkk_paths_from_request(
    request: dict[str, Any], author: str, all_sim_versions=False
) -> list:
    # Run BKK query to get processing passes matching this one
    bkk_path = construct_bkk_path(request)

    # wildcard Sim version
    if all_sim_versions:
        bkk_path_toks = bkk_path.split("/")
        bkk_path = str(
            Path("/")
            / "/".join(bkk_path_toks[:4])
            / "Sim*"
            / "/".join(bkk_path_toks[5:])
        )

    with DownloadedProxyFile(author, "lhcb_user") as proxy_file:
        cmd = settings.lb_dirac + [
            "dirac-bookkeeping-get-processing-passes",
            "--BKQuery",
            bkk_path,
        ]

        # NOTE: calling code should handle timeout exception
        returncode, stdout, stderr = await run(
            *cmd, extra_env={"X509_USER_PROXY": proxy_file.name}, timeout=120
        )

    # record any returned processing passes
    lns = stdout.split("\n")
    passes = []
    for ln in lns[2:]:
        if ln.lower().startswith("/sim"):
            prev_bkk_path = (
                Path("/MC")
                / request["mc_config_version"]
                / request["sim_condition"]
                / ln
            )
            passes.append(str(prev_bkk_path))

    return passes


async def find_jobs(
    project, mr_id: int, ci_run_id: int, author: str, commit_sha: str, name: str
) -> tuple[bool, str, dict[str, Any], set[str]]:
    to_process = dict()

    log = ""
    error = False

    for file_info in project.repository_tree(
        iterator=True, ref=commit_sha, recursive=True, path=f"{name}/"
    ):
        if file_info["type"] != "blob":
            continue
        path = PurePosixPath(file_info["path"])
        if len(path.parts) == 2:
            if path.suffix == ".yaml":
                to_process[path] = file_info["id"]
            elif path.suffix == ".yml":
                error = True
                log += "Found suffix named .yml. Please change it to .yaml\n"
            else:
                error = True
                log += f"Unknown suffix found: {path.suffix}."

    job_specs = {}
    mr = project.mergerequests.get(mr_id)
    mr_labels = set()
    name_counts = Counter(_parse_filename(path)[0] for path in to_process)
    if set(name_counts.values()) != {1}:
        error = True
        log += "Found files with the same name but different stages:\n"
        log += f"    {[k for k, v in name_counts.items() if v > 1]!r}\n"

    max_events = {"DST": 20e6, "Other": 100e6, "generated": 500e6}
    num_events_requested = {"DST": 0, "Other": 0, "generated": 0}

    for path, blob_id in to_process.items():
        name, input_stage = _parse_filename(path)
        blob_data = project.repository_raw_blob(blob_id)

        if input_stage != 0:
            _add_expert_label(mr_labels, "not stage 0")

        with TemporaryDirectory() as tmpdir:
            input_path = Path(tmpdir) / "input.yaml"
            output_path = Path(tmpdir) / "output.yaml"
            input_path.write_bytes(blob_data)

            if input_stage == 6:
                output_path.write_bytes(input_path.read_bytes())
            else:
                cmd = [
                    "lb-mc",
                    f"--in-stage={input_stage}",
                    str(input_path),
                    str(output_path),
                ]
                retcode, stdout, stderr = await run(
                    *cmd, log_stdout=True, timeout=600, cwd=tmpdir
                )
                log += f"Exit code from {cmd} was {retcode}\n"
                log += f"{'=' * 80}\n{stderr}\n{'=' * 80}\n"
                error |= retcode != 0
                if retcode != 0:
                    continue
            raw_productions = yaml.safe_load(output_path.read_text())
            if len({p["name"] for p in raw_productions}) != len(raw_productions):
                error = True
                log += f"Found {len({p['name'] for p in raw_productions})} names for {len(raw_productions)} jobs\n"
                log += "Names are not unique \n"
            for production in raw_productions:
                production["author"] = author
                for event_type in production["event_types"]:
                    request = deepcopy(production)
                    request["event_types"] = [event_type]

                    request["comment"] += f" \n lhcb-simulation/mc-requests!{mr_id}"

                    # update request with paths of any previous requests
                    prev_proc_paths = []
                    try:
                        prev_proc_paths = await get_previous_bkk_paths_from_request(
                            request, author, all_sim_versions=True
                        )
                    except asyncio.exceptions.TimeoutError:
                        log += f"Timeout hit when querying for previous requests for '{construct_bkk_path(request)}'.\n"
                        prev_proc_paths = [
                            "Timeout when retrieving previous processing path info"
                        ]

                    job_name = f"{path.name}:{production['name']}:{event_type['id']}"
                    if job_name in job_specs:
                        raise NotImplementedError(job_name)
                    job_specs[job_name] = {"request": [request]}
                    if input_stage != 6:
                        job_specs[job_name]["LbMCSubmit_Version"] = get_package_version(
                            "LbMCSubmit"
                        )

                    job_specs[job_name]["prev_proc_paths"] = prev_proc_paths
                    filetype = production["steps"][-1]["output"][0]["type"]

                    if filetype[-4:] == ".DST":
                        num_events_requested["DST"] += event_type["num_events"]
                    else:
                        num_events_requested["Other"] += event_type["num_events"]
                    num_events_requested["generated"] += (
                        event_type["num_events"] / production["retention_rate"]
                    )

                    # Check if production is 2024 and digi
                    # Temporary, delete when the issue is resolved
                    datatype_block_w_digi = [
                        "2024.W25.27",
                        "expected-2024.Q3.4",
                        "2024.Q1.2",
                        "2024.W29.30",
                        "2024.W31.34",
                    ]
                    if any(
                        data_type in production["name"]
                        for data_type in datatype_block_w_digi
                    ) and ("DIGI" in filetype):
                        _add_expert_label(mr_labels, "DIGI 2024")

                    # Set all xDIGI to be expert only (see LbAPI!168#note_8484332)
                    if "XDIGI" in filetype:
                        _add_expert_label(mr_labels, "XDIGI")

            input_production = yaml.safe_load(input_path.read_text())
            if input_stage == 0:
                for input_sample in input_production["samples"]:
                    if "stripping" not in input_sample:
                        continue
                    filtering_keys = ["filtering-script", "filter"]
                    if any(key in input_sample["stripping"] for key in filtering_keys):
                        mr_labels.add("MCFiltering")

    if any(num_events_requested[i] > max_events[i] for i in max_events.keys()):
        mr_labels.add("PPG Approval Required")

    expert_label_count = len(
        [mr_label for mr_label in mr_labels if mr_label.startswith("Expert")]
    )

    if expert_label_count > 1:
        mr_labels = {
            mc_label for mc_label in mr_labels if not mc_label.startswith("Expert")
        }
        mr_labels.add("Expert only::Multiple")

    try:
        _add_expert_approval(mr, required=expert_label_count > 0)
    except Exception:
        logger.exception("Failed to add expert approval")

    return error, log, job_specs, mr_labels


def _add_expert_label(mr_labels, suffix):
    mr_labels.add(f"Expert Only::{suffix}")

    return None


def _add_expert_approval(mr, *, required: bool):
    for rule in mr.approval_rules.list():
        if rule.name == "experts":
            if not required:
                rule.delete()
            break
    else:
        if required:
            approval_data = {
                "approvals_required": 1,
                "name": "experts",
                "rule_type": "regular",
                "user_ids": [],
                "group_ids": [SIMULATION_EXPERTS_GROUP_ID],
            }
            mr.approval_rules.create(approval_data)


def make_requests(submitting_user, jobs):
    jobs = deepcopy(jobs)
    grouped_jobs = defaultdict(dict)
    # TODO: The str(eventtype) should be removed and replaced with a
    # proper validation of stage 6 yaml files.
    for job in jobs:
        file_name, prod_name, eventtype = job.name.split(":")
        key = (job.ci_run_name, file_name, prod_name)
        request = job.spec["request"][0]
        if len(request["event_types"]) != 1:
            raise NotImplementedError(request["event_types"])
        if eventtype != str(request["event_types"][0]["id"]):
            raise NotImplementedError(eventtype, request["event_types"])
        if eventtype in grouped_jobs[key]:
            raise NotImplementedError(eventtype, key)
        grouped_jobs[key][eventtype] = (job.id, request)

    names = []
    requests = []
    for eventtypes in grouped_jobs.values():
        event_types = []
        event_type_to_jobid = {}
        for eventtype, (job_id, request) in eventtypes.items():
            event_type_to_jobid[eventtype] = job_id
            event_types.append(request["event_types"].pop(0))
            event_types[-1]["id"] = str(event_types[-1]["id"])

        if not (event_types and all(r == request for _, r in eventtypes.values())):
            raise NotImplementedError("Not all requests are the same")
        request["event_types"] = event_types
        request["author"] = submitting_user
        names.append(event_type_to_jobid)
        requests.append(request)

    return names, requests


async def post_submission_tasks(
    project_id: int, commit_sha: str, mr_id: int, request_ids: list[int]
) -> None:
    # Get approval information, check if we need to run it (only works on Convener approval)
    rule_name = "conveners"

    # TODO: make this section tighter
    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)
    approval_rules = mr.approval_state.get().rules
    approval_flags = [rule_name in rule["name"] for rule in approval_rules]
    approval_flag = any(approval_flags)

    logger.info(
        "Approval flag is %s, possibly attempting to sign request for ids %s",
        approval_flag,
        request_ids,
    )
    if approval_flag:
        if None in request_ids:
            raise TypeError(f"None in requests_id: {request_ids} ")
        signing_user = "cburr"
        await sign_productions(
            "Submitted", ProdStates.PPG_OK, signing_user, request_ids
        )


def issue_job_table(jobs):
    job_table = []
    prev_request_id = None
    for job in sorted(jobs, key=lambda j: j.subrequest_id or 0):
        file_name, job_name, eventtype = job.name.split(":")
        if prev_request_id and prev_request_id == job.request_id:
            job_table.append(
                [
                    "-",
                    "-",
                    "-",
                    eventtype,
                    f'{ job.spec["request"][0]["event_types"][0]["num_events"]:,}',
                    "-",
                    "-",
                    job.subrequest_id,
                ]
            )
        else:
            prev_request_id = job.request_id
            job_table.append(
                [
                    job.ci_run_name,
                    file_name,
                    job_name,
                    eventtype,
                    f'{ job.spec["request"][0]["event_types"][0]["num_events"]:,}',
                    job.spec["request"][0]["priority"],
                    job.request_id,
                    job.subrequest_id or "-",
                ]
            )
    job_table_heading = [
        "Production name",
        "Filename".ljust(35),
        "Request name".ljust(40),
        "Event type",
        "Num events",
        "Priority",
        "Request ID",
        "Subrequest ID",
    ]
    job_table_heading = [job_table_heading, ["-" * len(x) for x in job_table_heading]]
    job_table = "\n".join(
        f"| {' | '.join(str(x).ljust(len(heading)) for heading, x in zip(job_table_heading[0], row))} |"
        for row in job_table_heading + job_table
    )

    return job_table


def _parse_filename(path: PurePosixPath) -> tuple[str, int]:
    """Parse the name and stage of a file."""
    pattern = r"(.+?)(?:\.stage(\d))?\.yaml"
    if match := re.fullmatch(pattern, path.name):
        name, input_stage = match.groups()
        return name, 0 if input_stage is None else int(input_stage)
    raise NotImplementedError(f"{path.name} does not match {pattern}")


## Note hook handling


def parse_comment(comment, trigger_list):
    TRIGGER_LINE_RE = r"^\s*{}($|\s+)".format("|".join(str(i) for i in trigger_list))

    commands = [
        line.strip() for line in comment.splitlines() if re.match(TRIGGER_LINE_RE, line)
    ]
    if not commands:
        return None
    elif len(commands) == 1:
        return commands[0].split()
    else:
        return "Comment includes multiple triggers, please choose one!"


async def do_note_hook(body):
    trigger_func_map = {
        "/bot-help": help_msg,
        "/submit-failed": fix_req,
        "/retry-signing": retry_signing,
        "/clean-repo": clean_req,
    }

    note = body["object_attributes"]["note"]
    commands = parse_comment(note, list(trigger_func_map.keys()))
    if commands is None:
        return None

    trigger = commands[0]

    func = trigger_func_map.get(trigger)
    if func is not None:
        if asyncio.iscoroutinefunction(func):
            return await func(body, commands)
        else:
            return func(body, commands)
    return None


def add_emoji(project_id, mr_iid, note_id, emoji="robot"):
    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_iid)
    note = mr.notes.get(note_id)
    note.awardemojis.create({"name": emoji})


def parse_notehook_body(body):
    project_id = body["project_id"]
    mr_id = body["merge_request"]["iid"]
    commit_sha = body["merge_request"]["last_commit"]["id"]
    note_id = body["object_attributes"]["id"]
    member_id = body["user"]["id"]
    return project_id, mr_id, commit_sha, note_id, member_id


def check_note_auth(member_id):
    # Checks if user is authorised
    group = gitlab.api.groups.get(SIMULATION_EXPERTS_GROUP_ID)
    try:
        group.members.get(member_id)
        return None
    except GitlabGetError as e:
        if e.response_code == 404:
            return "You are not authorised to do this! Contact the MC Liaisons channel"
        return (
            f"Failed with exception {e}. Contact MC Liaisons channel to sort this out"
        )
    except Exception as e:
        return (
            f"Failed with exception {e}. Contact MC Liaisons channel to sort this out"
        )


async def retry_signing(body, commands):
    project_id, mr_id, commit_sha, note_id, member_id = parse_notehook_body(body)
    add_emoji(project_id, mr_id, note_id)
    auth_msg = check_note_auth(member_id)
    if auth_msg is not None:
        return auth_msg

    parser = argparse.ArgumentParser()
    parser.add_argument("--no-clean", action="store_true")
    args = parser.parse_args(commands[1:])
    await post_submit(project_id, commit_sha, mr_id)

    if args.no_clean:
        return "Retry signing done!"
    await clean_repo(project_id, commit_sha, mr_id)
    return "Retry signing done! Repo cleaned!"


async def clean_req(body, *_):
    project_id, mr_id, commit_sha, note_id, member_id = parse_notehook_body(body)
    add_emoji(project_id, mr_id, note_id)
    auth_msg = check_note_auth(member_id)
    if auth_msg is not None:
        return auth_msg
    await clean_repo(project_id, commit_sha, mr_id)
    return "Repo cleaned!"


async def fix_req(body, commands):
    project_id, mr_id, commit_sha, note_id, member_id = parse_notehook_body(body)
    add_emoji(project_id, mr_id, note_id)
    auth_msg = check_note_auth(member_id)
    if auth_msg is not None:
        return auth_msg

    parser = argparse.ArgumentParser()
    parser.add_argument("--do-for-real", action="store_true")
    parser.add_argument("--allow-submit", action="store_true")
    args = parser.parse_args(commands[1:])

    dry_run = not args.do_for_real
    allow_submit = args.allow_submit

    return await fix_request(project_id, mr_id, commit_sha, dry_run, allow_submit)


def help_msg(*_):
    msg = """
    Heya! I'm able to help you with a couple of things regarding simulation requests.
    I can't do much right now but I will have more features later on!
    Just summon me by typing /[command].

    Commands available:

    bot-help : Prints this message!
    submit-failed : Used to resubmit failed requests
    retry-signing : Used to retry signing a request
    clean-repo : Used to clean the repo after a failed submission
    """
    return dedent(msg)


def request_job_table(jobs):
    job_table = []
    prev_job_name = None

    num_events_requested = {"DST": 0, "Other": 0, "generated": 0}

    for job in sorted(jobs, key=lambda j: j.name.split(":")[1]):
        file_name, job_name, eventtype = job.name.split(":")
        num_evt = job.spec["request"][0]["event_types"][0]["num_events"]
        file_type = job.spec["request"][0]["steps"][-1]["output"][0]["type"].split(".")[
            -1
        ]
        retention_rate = job.spec["request"][0]["retention_rate"]

        # TODO: This is duplicated from above, see if duplication can be reduced
        if file_type[-4:] == "DST":
            num_events_requested["DST"] += num_evt
        else:
            num_events_requested["Other"] += num_evt
        num_events_requested["generated"] += num_evt / retention_rate

        bkk_path = construct_bkk_path(job.spec["request"][0])

        if prev_job_name and prev_job_name == job_name:
            job_table.append(
                ["-", "-", "-", "-", eventtype, f"{ num_evt:,}", file_type, bkk_path]
            )
        else:
            prev_job_name = job_name
            job_table.append(
                [
                    job.ci_run_name,
                    file_name,
                    job_name,
                    job.spec["request"][0]["priority"],
                    eventtype,
                    f"{ num_evt:,}",
                    file_type,
                    bkk_path,
                ]
            )

    job_table_heading = [
        "Production name",
        "Filename".ljust(35),
        "Request name".ljust(40),
        "Priority",
        "Event type",
        "Num events",
        "Filetype",
        "BKK Path",
    ]
    job_table_heading = [job_table_heading, ["-" * len(x) for x in job_table_heading]]
    job_table = "\n".join(
        f"| {' | '.join(str(x).ljust(len(heading)) for heading, x in zip(job_table_heading[0], row))} |"
        for row in job_table_heading + job_table
    )

    return job_table, num_events_requested


ISSUE_SUMMARY = """
:wave: Welcome to MC Requests!

Below is a summary table of the request you are about to submit.

**Total statistics of the request is as follows**

* Total number of DST events requested: {DST}
* Total number of other events requested: {mDST}
* Total number of generated events requested: {gen}

**Full Job Table listed below**

<details>

{table}

</details>


"""


def make_mr_summary_info(jobs):
    job_table, job_events = request_job_table(jobs)

    dst = f'{ job_events["DST"]:,}'
    mdst = f'{ job_events["Other"]:,}'
    gen = f'{ job_events["generated"]:,}'

    body = ISSUE_SUMMARY.format(
        table=job_table,
        DST=dst,
        mDST=mdst,
        gen=gen,
    )
    # TODO: For emmuhamm: Resurrect this part of the code.
    # for job in sorted(jobs, key=lambda j: j.name.split(":")[1]):
    #     if job.spec["request"][0].get("prev_proc_paths"):
    #         job_name = job.name.split(":")[1]

    #         # check for issues with getting previous processing paths
    #         if not job.spec["request"][0]["prev_proc_paths"][0].startswith("/"):
    #             body += f"\n**NOTE for '{job_name}':**\n* {job.spec['request'][0]['prev_proc_paths'][0]}\n"
    #             continue

    #         body += f"\n**Previous request(s) found for '{job_name}':**\n"
    #         for p in job.spec["request"][0]["prev_proc_paths"]:
    #             body += f"* {p}"

    #             # report if we have exactly the same request
    #             if p.find(job.spec["request"][0]["sim_condition"]) != -1:
    #                 body += "  **(Exact match to request)**"

    #             body += "\n"

    return body
