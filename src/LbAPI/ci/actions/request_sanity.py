###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import logging
from collections import defaultdict

logger = logging.getLogger(__name__)

AP_EXPERTS_GROUP_ID = 222779
AP_OUTPUT_SIZE_LIMIT = 10e12  # 10 TB!
AP_INPUT_SIZE_LIMIT = 1e15  # 1 PB !!


def calculate_output_size(job):
    total_job_insize = job.spec["input-dataset"]["size"]

    steps = (job.result or {}).get("steps", [])
    if not steps:
        return 0
    if "bookkeeping_xml" not in steps[-1].get("dirac", {}):
        return 0

    job_insize = sum(if_["size"] for if_ in steps[0]["gaudi"]["summary_xml"]["input"])
    job_outsize = sum(
        of_["size"] for of_ in steps[-1]["dirac"]["bookkeeping_xml"]["output"]
    )
    est_total_outsize = total_job_insize / job_insize * job_outsize

    return est_total_outsize


def check_output_large(jobs):
    for job in jobs:
        outputsize = calculate_output_size(job)
        if outputsize > AP_OUTPUT_SIZE_LIMIT:
            return {"blocker": True, "add_label": True}

    return {"blocker": False, "add_label": False}


def check_duplicate_processing(jobs):
    path_processing_count = defaultdict(int)

    for job in jobs:
        input_dataset = job.spec.get("input-dataset", None)
        if not input_dataset:
            continue

        # The path only counts if we process Run 3 data.
        conditions_dict = get_conddict(job)
        if "LHCb" in conditions_dict.get(
            "configName", ""
        ) and "Collision2" in conditions_dict.get("configVersion", ""):
            path_processing_count[json.dumps(input_dataset, sort_keys=True)] += 1

    found = False
    for duped_path, processing_count in path_processing_count.items():
        logger.info(f"Processes path: {duped_path!r}")
        if processing_count > 1:
            logger.warning(
                f"Duplicate processed path ({processing_count} times): {duped_path!r}"
            )
            found = True

    return {"add_label": found, "blocker": found}


def check_RAW_processing(jobs):
    for job in jobs:
        input_dataset = job.spec.get("input-dataset", None)
        if not input_dataset:
            continue
        conditions_dict = input_dataset.get("conditions_dict", {})
        if conditions_dict.get("inFileType", "") == "RAW":
            return {"add_label": True, "blocker": True}
    return {"add_label": False, "blocker": False}


def validation_path(jobs):
    for job in jobs:
        conditions_dict = get_conddict(job)
        if conditions_dict.get("configName", "") == "validation":
            return {"add_label": True, "blocker": True}
    return {"add_label": False, "blocker": False}


def check_MC_processing(jobs):
    for job in jobs:
        input_dataset = job.spec.get("input-dataset", None)
        if not input_dataset:
            continue
        conditions_dict = input_dataset.get("conditions_dict", {})

        # check this is processing Run3 MC.
        if "MC" in conditions_dict.get("configName", "") and (
            "202" in conditions_dict.get("configVersion", "")
            or "Upgrade" in conditions_dict.get("configVersion", "")
        ):
            for req in job.spec.get("request", []):
                for step in req.get("steps", []):
                    app_name = step["application"]["name"]
                    if app_name in ["Moore", "Allen"]:
                        process = step["options"]["extra_options"].get("process", None)
                        is_sprucing_job = process == "Spruce" and app_name == "Moore"
                        if not is_sprucing_job:
                            return {"add_label": True, "blocker": True}

    return {"add_label": False, "blocker": False}


def get_conddict(job):
    input_dataset = job.spec["input-dataset"]
    return input_dataset.get("conditions_dict", {})


def check_run3_dat(jobs):
    for job in jobs:
        conditions_dict = get_conddict(job)
        if "LHCb" in conditions_dict.get(
            "configName", ""
        ) and "Collision2" in conditions_dict.get("configVersion", ""):
            return {"add_label": True, "blocker": False}


def check_run2_dat(jobs):
    for job in jobs:
        conditions_dict = get_conddict(job)
        if "LHCb" in conditions_dict.get(
            "configName", ""
        ) and "Collision1" in conditions_dict.get("configVersion", ""):
            return {"add_label": True, "blocker": False}


def check_run3_mc(jobs):
    for job in jobs:
        conditions_dict = get_conddict(job)
        if "MC" in conditions_dict.get("configName", ""):
            if "202" in conditions_dict.get(
                "configVersion", ""
            ) or "Upgrade" in conditions_dict.get("configVersion", ""):
                return {"add_label": True, "blocker": False}


def check_run2_mc(jobs):
    for job in jobs:
        conditions_dict = get_conddict(job)
        if "MC" in conditions_dict.get(
            "configName", ""
        ) and "201" in conditions_dict.get("configVersion", ""):
            return {"add_label": True, "blocker": False}


def check_input_dataset_info_available(jobs):
    if not jobs:
        return False
    for job in jobs:
        if not job.result or not job.spec:
            return False
        input_dataset = job.spec.get("input-dataset", None)
        if not input_dataset:
            return False
        cond_dict = input_dataset.get("conditions_dict", None)
        if not cond_dict:
            return False
        if not cond_dict.get("configName", None) or not cond_dict.get(
            "configVersion", None
        ):
            return False
    return True


def determine_request_sanity(mr, jobs):
    sanity_checks = {
        "duplicated inputs": check_duplicate_processing,
        "RAW processing": check_RAW_processing,
        "MC reconstruction": check_MC_processing,
        "data::run3": check_run3_dat,
        "data::run1/2": check_run2_dat,
        "MC::run1/2": check_run2_mc,
        "MC::run3": check_run3_mc,
        "Output too large": check_output_large,
        "Validation": validation_path,
        # "Input too large": check_input_large,
    }

    blocked = False
    labels = set(mr.labels)
    has_dataset_and_conditions = check_input_dataset_info_available(jobs)

    if has_dataset_and_conditions:
        for label_name, checker in sanity_checks.items():
            result = checker(jobs)
            if not result:
                continue
            blocked |= result.get("blocker", False)
            if result.get("add_label", False):
                labels.add(label_name)
            elif label_name in labels:
                labels.remove(label_name)

        if blocked:
            labels.add("expert approval")
        elif "expert approval" in labels:
            labels.remove("expert approval")
        mr.labels = list(labels)
        mr.save()

        _add_expert_approval(mr, required=blocked)


def _add_expert_approval(mr, *, required: bool):
    for rule in mr.approval_rules.list():
        if rule.name == "Expert":
            if not required:
                rule.delete()
            break
    else:
        if required:
            approval_data = {
                "name": "Expert",
                "approvals_required": 1,
                "applies_to_all_protected_branches": True,
                "rule_type": "regular",
                "user_ids": [199, 20156],
                "group_ids": [AP_EXPERTS_GROUP_ID],
            }
            mr.approval_rules.create(approval_data)
