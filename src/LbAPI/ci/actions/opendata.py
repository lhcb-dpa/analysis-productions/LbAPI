###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "get_eos_path",
    "extract_name",
    "deploy_md_to_eos",
    "md_from_od_request",
)
import logging
import re
from pathlib import Path
from tempfile import TemporaryDirectory

from LbAPI.settings import (
    settings,
)
from LbAPI.utils import eos_cp, get_krb5_creds  # TODO: use eos_artifact_path

logger = logging.getLogger(__name__)

LB_OD_USER = (
    "lbodreq"  # TODO: make a setting for lbodreq username. this is hardcoded and Bad
)


def get_md_eos_path(mr):
    return (
        f"/eos/opendata/lhcb/upload/opendata-lhcb-ntupling-service/analysis-productions/"
        f"merge-requests/{mr.iid}/outputs/test-production/test-results.md"
    )


def get_eos_path(ci_id, name, job_id, job_name, file_name):
    eos_prefix = "root://eoslhcb.cern.ch/"
    eos_filename = f"/eos/lhcb/wg/dpa/wp2/ci/{ci_id}/{name}/{job_id}/{job_id}#{job_name}/{file_name}"
    return f"{eos_prefix}{eos_filename}"


def extract_name(job_spec_name):
    match = re.search(r"#([^#]+)#", job_spec_name)
    if match:
        return match.group(1)
    return AttributeError("No name matched from job spec name - please check")


async def deploy_md_to_eos(md_eos_path, body):
    WELCOME_MESSAGE_OPENDATA = f"""
:wave: Welcome to the **LHCb** Open Data service!

Congratulations! Your request has been approved by the *LHCb Open Data Team* and submitted to
the *LHCb Analysis Productions service*!

The internal testing system has been successfully completed, creating a small test sample.

Please take a moment to assess the test statistic below and consider the following:

1. Is the estimated output size within a feasible range for you?
2. Do all expected variables exist in the ROOT file?

If the test statistic fulfills the above criteria and you are satisfied with the results, please confirm
your request here.

**Next Steps:**
* Once you confirm you are satisfied with the test result, the **LHCb** Open Data Team will send
the request to full production
* You will be notified once the production is finished. The ROOT files are then transfered to the Open Data Portal.
* You can then access the data freely. Have fun on your Analysis!


Thank you for your patience.
If you have further questions or concerns, please feel free to reach out by leaving a comment
on your request, by sending an email to [opendata-support@cern.ch](mailto:lopendata-support@cern.ch) or by opening
a ticket on the [Opendata Data Forum](https://opendata-forum.cern.ch).

{body}

---
**N.B.** this is an automated message from the friendly Analysis Productions Bot.
"""

    with TemporaryDirectory() as tmpdir:
        odmsg_fn = Path(tmpdir) / "test-production.md"
        odmsg_fn.write_text(WELCOME_MESSAGE_OPENDATA)
        krb5_cache = await get_krb5_creds(
            settings.lbodreq_user, settings.lbodreq_password, "CERN.CH"
        )
        await eos_cp(
            odmsg_fn,
            f"root://eospublic.cern.ch/{md_eos_path}",
            krb5_cache=krb5_cache.name,
        )


async def md_from_od_request(jobs, ci_id):
    dropdown_body = ""
    opendata_production_summary = ""
    total_insize = 0
    est_total_outsize = 0

    if not jobs:
        return None

    prod_name = ""
    for job in jobs or []:
        prod_name = extract_name(job.spec["request"][0]["name"])
        # filename = job.result["steps"][-1]["dirac"]["bookkeeping_xml"]["output"][0][
        #     "filename"
        # ]
        # eos_path = get_eos_path(ci_id, prod_name, job.id, job.name, filename)
        total_job_insize = job.spec["input-dataset"]["size"] / 1e9
        total_insize += total_job_insize
        job_insize = (
            job.result["steps"][0]["gaudi"]["summary_xml"]["input"][0]["size"] / 1e9
        )
        job_outsize = (
            job.result["steps"][-1]["dirac"]["bookkeeping_xml"]["output"][0]["size"]
            / 1e9
        )
        est_total_outsize += total_job_insize / job_insize * job_outsize

        # FIXME: X509_USER_PROXY authentication not working with xrootd/uproot.
        # needs investigation.
        # resource = importlib.resources.path(
        #     "LbAPI.ci.templates", "opendata_inspect_output_file.py"
        # )
        # with (resource as opendata_inspect_output_file,):
        #     cmd = [
        #         "python",
        #         opendata_inspect_output_file,
        #         f"--eos-path={eos_path}",
        #     ]
        #     with DownloadedProxyFile(LB_OD_USER, "lhcb_user") as proxy_file:
        #         returncode, stdout, stderr = await run(
        #             *cmd,
        #             extra_env={"X509_USER_PROXY": proxy_file.name},
        #         )
        #     assert returncode == 0, f"Problem accessing output file: {stderr}"

        # output_json = json.loads(stdout)

        # dropdown_body += """<details>\n"""
        # dropdown_body += f"""<summary>{job.name}</summary>\n"""
        # for key, branches in output_json.items():
        #     ##
        #     dropdown_body += """<details>\n"""
        #     dropdown_body += (
        #         f"""<summary>{key} ({len(branches)} Branches)</summary>\n\n"""
        #     )
        #     for branch in branches:
        #         dropdown_body += f"""   {branch}  \n"""
        #     dropdown_body += """</details>\n"""
        #     ##
        # dropdown_body += """</details>\n"""

    opendata_production_summary += (
        f"`{prod_name}` will process approximately {total_insize:.1f} GB of data "
        f"and is expected to generate around {est_total_outsize:.1f} GB of output.\n"
        f"Please note that the actual size of the output files may vary from the estimate."
    ) + "\n\n"

    #     if dropdown_body:
    #         dropdown_body = f"""
    # <details>
    # <summary>
    #     <b>See Branches in Production Tuples</b>
    # </summary>
    # {dropdown_body}
    # </details>
    # """

    return f"""
{opendata_production_summary}

{dropdown_body}
"""


async def opendata_req(jobs, pipeline_id, mr):
    md_eos_path = get_md_eos_path(mr)
    body_od = await md_from_od_request(jobs, pipeline_id)
    if body_od:
        await deploy_md_to_eos(md_eos_path, body_od)

    return body_od is not None
