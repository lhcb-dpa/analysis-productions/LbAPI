###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import logging
import os
import sys
from os.path import join
from pathlib import Path
from tempfile import TemporaryDirectory

import git

from LbAPI.settings import settings, ssh_key

logger = logging.getLogger(__name__)


class LbAPGitRepo:
    def __init__(self, ssh_url, git_rev):
        self._tmpdir = TemporaryDirectory()  # pylint: disable=consider-using-with
        # OpenSSH reads the passwd file to determine if the permissions on SSH keys
        # are appropriate. As OpenShift runs the container as a random UID we have
        # to use nss_wrapper to fake the user and group information
        self._write_passwd_file()
        self._write_group_file()
        logger.debug("Cloning sources into %s from %s", self.path, ssh_url)
        self.repo = git.Repo.clone_from(ssh_url, self.path, env=self.git_env)
        self.repo.config_writer().set_value(
            "user", "name", settings.tagging.actor_name
        ).release()
        self.repo.config_writer().set_value(
            "user", "email", settings.tagging.actor_email
        ).release()
        logger.debug("Checking out %s in %s", git_rev, self.path)
        self.repo.git.checkout(git_rev)

    def _write_passwd_file(self):
        with open(self.passwd_fn, "w") as fp:
            fp.write(
                f"lbapuser:x:{os.getuid()}:{os.getgid()}:Lb AP:/tmp/home:/bin/false\n"
                "root:x:0:0:root:/root:/bin/bash"
            )

    def _write_group_file(self):
        with open(self.group_fn, "w") as fp:
            fp.write(f"lbapuser:x:{os.getgid()}:\nroot:x:0:")

    def __enter__(self):
        self._tmpdir.__enter__()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._tmpdir.__exit__(exc_type, exc_value, traceback)

    @property
    def path(self):
        return join(self._tmpdir.name, "repo")

    @property
    def passwd_fn(self):
        return join(self._tmpdir.name, "passwd")

    @property
    def group_fn(self):
        return join(self._tmpdir.name, "group")

    @property
    def git_env(self):
        env = {
            "NSS_WRAPPER_PASSWD": self.passwd_fn,
            "NSS_WRAPPER_GROUP": self.group_fn,
        }
        if ssh_key:
            env["GIT_SSH_COMMAND"] = (
                f"ssh -i {ssh_key.path} -o StrictHostKeyChecking=no"
            )
        libnss_wrapper = (
            Path(sys.executable).parent.parent / "lib" / "libnss_wrapper.so"
        )
        if libnss_wrapper.exists():
            env["LD_PRELOAD"] = str(libnss_wrapper)
        return env
