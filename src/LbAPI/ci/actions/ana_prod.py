###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "find_ciruns",
    "find_jobs",
    "make_requests",
    "post_submission_tasks",
    "issue_job_table",
    "do_note_hook",
    "make_mr_summary_info",
)
import asyncio
import importlib.resources
import json
import logging
import os
import subprocess
from collections import defaultdict
from copy import deepcopy
from datetime import timedelta
from functools import lru_cache
from itertools import chain
from pathlib import Path, PurePosixPath
from tempfile import TemporaryDirectory
from typing import Any

import ldap3
import sqlalchemy
import yaml
from gitlab import GitlabGetError

from LbAPI.ci.queries import _update_status
from LbAPI.database import SessionLocal, webhooks
from LbAPI.enums import CIRunStatus, WebhookType
from LbAPI.settings import (
    dirac_host_cert,
    dirac_host_key,
    settings,
)
from LbAPI.utils import get_package_version, run

from .common import ProdStates, find_mr_and_issue, sign_productions

logger = logging.getLogger(__name__)


@lru_cache(maxsize=128)
def _cern_username_to_emails(commonName: str) -> list[str]:
    try:
        server = ldap3.Server("ldap://xldap.cern.ch")
        connection = ldap3.Connection(
            server, client_strategy=ldap3.SAFE_SYNC, auto_bind=ldap3.AUTO_BIND_NO_TLS
        )
        status, result, response, _ = connection.search(
            "OU=Users,OU=Organic Units,DC=cern,DC=ch",
            f"(CN={commonName})",
            attributes=["mail"],
        )
        if not status:
            raise ValueError(f"Bad status from LDAP search: {result}")
        if len(response) != 1:
            raise ValueError(
                f"Expected exactly one match for CN={commonName} but found {len(response)}"
            )
        return response[0]["attributes"]["mail"]
    except Exception:
        logger.exception("Failed to convert %s to email", commonName)
        return []


async def find_ciruns(files_iterator: list[dict[str, str]]) -> set[str]:
    analyses = set()
    for file_info in files_iterator:
        if file_info["type"] != "blob":
            continue
        path = PurePosixPath(file_info["path"])
        if len(path.parts) < 2 or path.parts[0].startswith("."):
            continue
        analyses.add(path.parts[0])
    return analyses


async def find_jobs(
    project, mr_id: int, ci_run_id: int, author: str, commit_sha: str, name: str
) -> tuple[bool, str, dict[str, Any], set[str]]:
    log = ""
    error = False
    job_specs = {}
    mr = project.mergerequests.get(mr_id)
    allowed_labels = set()
    mr_labels = allowed_labels.intersection(mr.labels)
    data_pkg_version = f"v1r{mr_id}"

    info_yaml_name = f"{name}/info.yaml"
    try:
        raw_info_yaml = project.files.raw(info_yaml_name, commit_sha)
    except GitlabGetError:
        error = True
        log += f"Failed to find {info_yaml_name}\n"
        return error, log, job_specs, mr_labels

    with TemporaryDirectory() as tmpdir:
        input_path = Path(tmpdir) / "input.yaml"
        output_path = Path(tmpdir) / "output.json"
        input_path.write_bytes(raw_info_yaml)

        cmd = [
            "python",
            "-mLbAPCommon",
            name,
            f"--ap-pkg-version={data_pkg_version}",
            f"--input={input_path}",
            f"--output={output_path}",
        ]
        if dirac_host_cert and dirac_host_key:
            cmd += [
                "--server-credentials",
                dirac_host_cert.path,
                dirac_host_key.path,
            ]

        async def _handle_output(
            filename, is_done: asyncio.Event, send_log_interval=10
        ):
            offset = 0
            finished = False
            while not finished:
                # Log file might not have been created yet
                if os.path.exists(filename):
                    off_incr = 0
                    with open(filename, "r") as stream:
                        stream.seek(offset)
                        lines_to_send = []
                        for line in stream.readlines():
                            off_incr += len(line)
                            lines_to_send.append(line.rstrip())
                    if lines_to_send:
                        async with SessionLocal.begin() as session:
                            await _update_status(
                                session,
                                webhooks.CIRun,
                                ci_run_id,
                                CIRunStatus.PENDING,
                                lines_to_send,
                            )
                            await session.commit()
                        # increment the offset only after we update
                        # the database
                        offset += off_incr
                    if is_done.is_set():
                        break
                await asyncio.sleep(send_log_interval)

        async def set_when_done(task, event):
            result = await task
            event.set()
            return result

        try:
            log_path = str(Path(tmpdir) / "dirac_query.log")
            is_done = asyncio.Event()
            (
                retcode,
                stdout,
                stderr,
            ), _ = await asyncio.gather(
                set_when_done(
                    run(
                        *cmd,
                        log_stdout=True,
                        timeout=3600,
                        cwd=tmpdir,
                        write_logfile=log_path,
                        extra_env={
                            "LBAP_COMMON_SENTRY_DSN": settings.common_sentry_dsn,
                            "LBAP_MERGE_VERSION": "2025-02-27",
                        },
                    ),
                    is_done,
                ),
                _handle_output(
                    log_path,
                    is_done,
                ),
            )
        except TimeoutError:
            log += f"\n!!! Timeout while running {cmd}. Consider increasing timeout."
            error |= True
        else:
            log += f"Exit code from {cmd} was {retcode}\n"
            log += f"{'=' * 80}\n{stderr}\n{'=' * 80}\n"
            error |= retcode != 0
            if retcode == 0:
                result = json.loads(output_path.read_text())
                for job_name, production in result["productions"].items():
                    if job_name in job_specs:
                        raise NotImplementedError(job_name)
                    job_spec = deepcopy(production)
                    job_spec["request"][0]["author"] = author
                    job_spec["check_data"] = result["check_data"]
                    job_spec["LbAPCommon_Version"] = get_package_version("LbAPCommon")
                    job_spec["data_pkg"] = {
                        "project_name": "AnalysisProductions",
                        "repo_url": project.http_url_to_repo,
                        "commit_sha": commit_sha,
                        "version": data_pkg_version,
                    }

                    job_spec["extra_test_cli_args"] = [
                        f"--num-test-lfns={job_spec['input-dataset']['n_test_lfns']}"
                    ]
                    if job_spec["input-dataset"].get("run-numbers"):
                        job_spec["extra_test_cli_args"] += [
                            f"--test-runs={','.join(job_spec['input-dataset']['run-numbers'])}"
                        ]
                    if job_spec["input-dataset"].get("start-run"):
                        job_spec["extra_test_cli_args"] += [
                            f"--start-run={job_spec['input-dataset']['start-run']}"
                        ]
                    if job_spec["input-dataset"].get("end-run"):
                        job_spec["extra_test_cli_args"] += [
                            f"--end-run={job_spec['input-dataset']['end-run']}"
                        ]
                    job_specs[job_name] = job_spec

    return error, log, job_specs, mr_labels


def make_requests(submitting_user, jobs):
    jobs = deepcopy(jobs)

    names = []
    requests = []
    for job in jobs:
        request = job.spec["request"][0]
        request["author"] = submitting_user
        requests.append(request)
        names.append({None: job.id})

    return names, requests


async def post_submission_tasks(
    project_id: int, commit_sha: str, mr_id: int, request_ids: list[int]
) -> None:
    await register_requests(project_id, commit_sha, mr_id, request_ids)

    from LbAPI import celery

    required_paths = set()
    async with SessionLocal() as session:
        query = sqlalchemy.select(webhooks.Job.spec["data_pkg"])
        query = query.where(webhooks.Job.request_id.in_(request_ids))
        for data_pkg in (await session.execute(query)).scalars():
            if not data_pkg:
                continue
            required_paths.add(
                "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/AnalysisProductions/"
                f"{data_pkg['version']}/AnalysisProductions.xenv"
            )

    celery.launch_productions.delay(
        WebhookType.ANA_PROD, request_ids, sorted(required_paths)
    )


async def launch_production(request_id, append_name):

    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.Job.spec["input-dataset"]["run-numbers"],
            webhooks.Job.spec["input-dataset"]["start-run"],
            webhooks.Job.spec["input-dataset"]["end-run"],
            webhooks.Job.spec["input-dataset"]["input_plugin"],
            webhooks.Job.spec["input-dataset"]["keep_running"],
            webhooks.Job.spec["input-dataset"]["n_test_lfns"],  # number of test LFNs
            webhooks.Job.result["resource_usage"]["exec_time"],  # in seconds
            webhooks.Job.spec["request"],
        )
        query = query.where(webhooks.CIRun.pipeline_id == webhooks.Pipeline.id)
        query = query.where(webhooks.Job.ci_run_id == webhooks.CIRun.id)
        query = query.where(webhooks.Job.outdated.is_(False))
        query = query.where(webhooks.Job.request_id == request_id)
        (
            run_numbers,
            start_run,
            end_run,
            input_plugin,
            keep_running,
            n_test_lfns,
            exec_time,
            request,
        ) = (await session.execute(query)).one()

    exec_time_per_lfn = 0
    if exec_time and n_test_lfns and exec_time > 0 and n_test_lfns >= 1:
        exec_time_per_lfn = exec_time / n_test_lfns

    def old_launcher():
        # Launch the productions
        resource = importlib.resources.path(
            "LbAPI.ci.templates", "ana_prod_launch_legacy.py"
        )
        with resource as launch_requests:
            cmd = settings.lb_dirac[:]
            cmd += [
                "python",
                launch_requests,
                "--server-credentials",
                dirac_host_cert.path,
                dirac_host_key.path,
                f"{request_id}",
                f"--plugin={input_plugin or 'default'}",
                f"--run-numbers={json.dumps(run_numbers)}",
                "--group-size=10",
            ]
            if start_run:
                cmd += [f"--start-run={start_run}"]
            if end_run:
                cmd += [f"--end-run={end_run}"]
            if append_name:
                cmd += [f"--append-name={append_name}"]
            if not keep_running:
                cmd += ["--no-keep-running"]

            logger.info("Launching with %s", cmd)
            with TemporaryDirectory() as tmpdir:
                # Must be ran from within a writable directory
                subprocess.run(cmd, check=True, cwd=tmpdir)

    def new_launcher():
        with TemporaryDirectory() as temp_dir:
            # dump the request yaml from the DB into the temporary directory so we can launch from the submission info.
            request_yaml_path = str(Path(temp_dir) / "request.yaml")
            with open(request_yaml_path, "w") as rf:
                yaml.dump(request, rf)

            # Launch the productions
            resource = importlib.resources.path(
                "LbAPI.ci.templates", "ana_prod_launch.py"
            )
            with resource as launch_requests:
                cmd = settings.lb_dirac[:]
                cmd += [
                    "python",
                    launch_requests,
                    "--server-credentials",
                    dirac_host_cert.path,
                    dirac_host_key.path,
                    f"{request_id}",
                    f"{request_yaml_path}",
                ]
                if append_name:
                    cmd += [f"--append-name={append_name}"]
                if keep_running:
                    cmd += ["--keep-running"]

                if exec_time_per_lfn < 1800:  # < 30 minutes
                    cmd += ["--input-group-size=20"]
                elif exec_time_per_lfn < 3600:  # 0.5-1 hour
                    cmd += ["--input-group-size=10"]
                elif exec_time_per_lfn < (2 * 3600):  # 1-2 hour
                    cmd += ["--input-group-size=5"]
                else:  # more than 2 hours
                    cmd += ["--input-group-size=2"]

                logger.info("Launching with %s", cmd)

                # Must be ran from within a writable directory
                subprocess.run(cmd, check=True, cwd=temp_dir)

    # Sign the Request IDs
    await sign_productions("Submitted", ProdStates.PPG_OK, "cburr", [request_id])
    await sign_productions("PPG OK", ProdStates.ACCEPTED, "cburr", [request_id])

    if "submission_info" in request[0]:
        logger.info(f"Running new-style launcher for request {request_id}")
        new_launcher()
    else:
        logger.info(f"Running legacy launcher for request {request_id}")
        old_launcher()

    # Mark the productions as active
    await sign_productions("Accepted", ProdStates.ACTIVE, "cburr", [request_id])


async def register_requests(project_id, commit_sha, mr_id, request_ids):
    mr, issue = find_mr_and_issue(project_id, commit_sha, mr_id)
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.Job.request_id,
            webhooks.CIRun.name.label("analysis"),
            webhooks.Job.name,
            webhooks.Job.spec["request"][0]["wg"].label("wg"),
            webhooks.Job.spec["data_pkg"]["version"].label("version"),
            webhooks.Pipeline.author,
        )
        query = query.where(webhooks.CIRun.pipeline_id == webhooks.Pipeline.id)
        query = query.where(webhooks.Job.ci_run_id == webhooks.CIRun.id)
        query = query.where(webhooks.Job.request_id.in_(request_ids))
        rows = (await session.execute(query)).all()

    analyses = defaultdict(list)
    for row in rows:
        analyses[(row.wg, row.analysis)].append(row)

    with importlib.resources.path(
        "LbAPI.ci.templates", "ana_prod_register.py"
    ) as register_script:
        for (wg, analysis), rows in analyses.items():
            versions = {row.version for row in rows}
            if not len(versions) == 1:
                raise NotImplementedError(versions)
            maybe_owners = set(
                chain(*(_cern_username_to_emails(row.author) for row in rows))
            )
            cmd = settings.lb_dirac + [
                "python",
                register_script,
                "--server-credentials",
                dirac_host_cert.path,
                dirac_host_key.path,
                f"--mr-url={mr.attributes['web_url']}",
                f"--issue-url={issue.attributes['web_url']}",
                f"--version={versions.pop()}",
                f"--wg={wg}",
                f"--analysis={analysis}",
                "--prod-ids",
                json.dumps({row.request_id: row.name for row in rows}),
                "--maybe-owners",
                json.dumps(list(maybe_owners)),
            ]
            subprocess.run(cmd, check=True)


def issue_job_table(jobs):
    table = [
        "| Step | Production ID | Num Test LFNs | Run time | Estimated Output Size (MB) |",
        "| ---- | ------------- | ------------- | -------- | -------------------------- |",
    ]
    for job in jobs:
        if job.result and job.result.get("resource_usage"):
            exec_time = timedelta(seconds=job.result["resource_usage"]["exec_time"])
        else:
            exec_time = "ERROR!"
        if not job.result or not job.result.get("steps"):
            out_size = "ERROR!"
        elif (
            not job.result["steps"][-1]
            .get("dirac", {})
            .get("bookkeeping_xml", {})
            .get("output")
        ):
            out_size = "ERROR!"
        else:
            out_files = job.result["steps"][-1]["dirac"]["bookkeeping_xml"]["output"]
            out_size = f"{sum(x['size'] for x in out_files) / 1024**2:.1f}"
        n_test_lfns = job.spec["input-dataset"]["n_test_lfns"]
        table.append(
            f"| {job.name} | {job.id} | {n_test_lfns} | {exec_time} | {out_size} |"
        )

    return "\n".join(table)


async def do_note_hook(body):
    raise NotImplementedError()


def make_mr_summary_info(jobs):
    lines = [":wave: Welcome to Analysis Productions!", ""]
    if jobs:
        lines += [
            "This is a summary of the productions requested in this merge request:",
            "",
        ]
        lines.append(issue_job_table(jobs))
    return "\n".join(lines)
