###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import importlib.resources
import logging
import os
import subprocess
from enum import StrEnum

from LbAPI.dirac import DownloadedProxyFile
from LbAPI.enums import WebhookType
from LbAPI.settings import (
    gitlab,
    settings,
)

log = logging.getLogger(__name__)

URL_BASE = "https://lhcb-productions.web.cern.ch/"


class ProdStates(StrEnum):
    PPG_OK = "PPG OK"
    ACCEPTED = "Accepted"
    ACTIVE = "Active"


signature_groups = {
    ProdStates.PPG_OK: "lhcb_ppg",
    ProdStates.ACCEPTED: "lhcb_tech",
    ProdStates.ACTIVE: "lhcb_prmgr",
}


async def sign_productions(
    old_state: str, new_state: ProdStates, signing_user: str, request_ids: list[int]
):
    log.info("Signing as %s", signing_user)

    # Sign the Request IDs
    resource = importlib.resources.path("LbAPI.ci.templates", "sign_requests.py")
    with (
        resource as sign_requests,
        DownloadedProxyFile(signing_user, signature_groups[new_state]) as proxy_file,
    ):
        cmd = settings.lb_dirac[:]
        cmd += [
            "python",
            sign_requests,
            f"--old-state={old_state}",
            f"--new-state={new_state}",
        ]
        cmd += ["--req-ids"] + [str(rid) for rid in request_ids]

        subprocess.run(
            cmd, env={**os.environ, "X509_USER_PROXY": proxy_file.name}, check=True
        )


def make_pipeline_url(project_type: WebhookType, pipeline_id: int) -> str:
    if project_type == WebhookType.ANA_PROD:
        url_dir = "ana-prod"
    elif project_type == WebhookType.MC_REQUEST:
        url_dir = "simulation"
    else:
        raise NotImplementedError(f"Invalid project type {project_type}")

    return f"{URL_BASE}/{url_dir}/pipelines/?id={pipeline_id}"


def find_mr_and_issue(project_id, commit_sha, mr_id):
    """Find the merge request and production tracking issue for a given pipeline."""
    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)
    issues = project.issues.list(search=f"{commit_sha} in !{mr_id}")
    if len(issues) > 1:
        raise NotImplementedError(issues)
    return mr, issues[0] if issues else None
