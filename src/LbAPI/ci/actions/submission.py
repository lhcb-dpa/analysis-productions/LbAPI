###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = ("submit_productions", "clean_repo", "fix_request")
import importlib.resources
import json
import logging
import os
import subprocess
import tempfile
from collections import defaultdict
from datetime import datetime
from pathlib import Path
from typing import Optional

import sqlalchemy
import yaml
from sqlalchemy import select

from LbAPI.ci.actions.data_pkg_tagging import LbAPGitRepo
from LbAPI.database import SessionLocal, webhooks
from LbAPI.dirac import DownloadedProxyFile, get_user_mapping
from LbAPI.enums import JobStatus, WebhookType
from LbAPI.settings import (
    dirac_host_cert,
    dirac_host_key,
    gitlab,
    settings,
    ssh_key,
)

logger = logging.getLogger(__name__)


type RequestIds = list[tuple[int, int | None, dict[str, int | None]]]


async def load_requests(
    project_type,
    pipeline_id,
    *,
    submitting_user=None,
    ci_run_name=None,
    validate=True,
    allow_partial=False,
):
    from . import MODULES_MAPPING

    if submitting_user:
        dirac_nickname = get_user_mapping()[submitting_user]
    else:
        dirac_nickname = submitting_user

    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.Job.id,
            webhooks.CIRun.name.label("ci_run_name"),
            webhooks.Job.name,
            webhooks.Job.status,
            webhooks.Job.spec,
            webhooks.Job.result,
            webhooks.Job.request_id,
            webhooks.Job.subrequest_id,
        )
        query = query.where(webhooks.CIRun.pipeline_id == pipeline_id)
        query = query.where(webhooks.Job.ci_run_id == webhooks.CIRun.id)
        if ci_run_name is not None:
            query = query.where(webhooks.CIRun.name == ci_run_name)
        query = query.where(webhooks.Job.outdated.is_(False))
        if allow_partial:
            query = query.where(webhooks.Job.request_id.is_(None))
        jobs = (await session.execute(query)).all()

    if validate:
        if not all(JobStatus.SUCCESS == job.status for job in jobs):
            raise NotImplementedError("Not all jobs are successful")
        if not allow_partial and not all(job.request_id is None for job in jobs):
            raise NotImplementedError("Some jobs already have a request_id")
        if not all(job.subrequest_id is None for job in jobs):
            raise NotImplementedError("Some jobs already have a subrequest_id")

    names, requests = MODULES_MAPPING[project_type].make_requests(dirac_nickname, jobs)

    # Discover if a data package needs to be tagged
    data_pkg = None
    dynamic_files = {}
    for job in jobs:
        if job.spec.get("data_pkg"):
            if data_pkg is None:
                data_pkg = job.spec["data_pkg"]
            elif data_pkg != job.spec["data_pkg"]:
                raise NotImplementedError(
                    "Data package mismatch", data_pkg, job.spec["data_pkg"]
                )
        for k, v in job.spec.get("dynamic_files", {}).items():
            if k not in dynamic_files:
                dynamic_files[k] = v
            elif dynamic_files[k] != v:
                raise NotImplementedError(
                    "Dynamic file mismatch", k, dynamic_files[k], v
                )
    if data_pkg is not None:
        data_pkg["dynamic_files"] = dynamic_files
    elif dynamic_files:
        raise NotImplementedError("Dynamic files without data_pkg")

    return names, requests, data_pkg


async def pre_submission_tasks(
    project_id: int,
    commit_sha: str,
    mr_id: int,
    submitting_user: str,
    *,
    dry_run=False,
) -> bool:
    """Run pre-submission tasks for a given project.

    If this function returns True, the productions are ready to be submitted.

    If this function returns False, it should be called again later until it
    returns True.
    """

    async with SessionLocal() as session:
        query = sqlalchemy.select(webhooks.ProjectWebhook.type, webhooks.Pipeline.id)
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        project_type, pipeline_id = (await session.execute(query)).one()

    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)

    names, requests, data_pkg = await load_requests(
        project_type,
        pipeline_id,
        submitting_user=submitting_user,
        allow_partial=False,
    )

    if mr.merge_commit_sha is None:
        if not dry_run:
            raise NotImplementedError(
                "Cannot submit a merge request without a merge commit"
            )
    elif data_pkg is not None:
        if data_pkg["repo_url"] == project.http_url_to_repo:
            # If the MR has been merged then we need to use the merge commit ref
            # as the reference used for testing will no longer exist.
            data_pkg["commit_sha"] = mr.merge_commit_sha
        tag_data_package(data_pkg)

    # TODO: Poll for deployment?
    return True


def tag_data_package(data_pkg):
    url = data_pkg["repo_url"]
    if not url.startswith("https://gitlab.cern.ch/"):
        raise ValueError(url)
    if ssh_key:
        url = url.replace(
            "https://gitlab.cern.ch/", "ssh://git@gitlab.cern.ch:7999/", 1
        )
    tag_name = data_pkg["version"]
    with LbAPGitRepo(url, data_pkg["commit_sha"]) as tmp:
        if tag_name in tmp.repo.tags:
            logger.info("Tag %s already exists, skipping...", tag_name)
            return tag_name

        # Add the dynamic options to the repo
        for sub_path, data in data_pkg["dynamic_files"].items():
            path = Path(tmp.path) / "dynamic" / sub_path
            path.parent.mkdir(parents=True, exist_ok=True)
            logger.debug("Writing dynamic options to %s", path)
            path.write_text(data)

        # Prevent the .gitignore from stopping the dynamic options from being committed
        (Path(tmp.path) / ".gitignore").unlink(missing_ok=True)

        # Commit the dynamic options
        for entry in tmp.repo.index.add(tmp.repo.untracked_files):
            logger.info("Added %s to staging area", entry.path)
        actor = settings.tagging.actor
        commit = tmp.repo.index.commit(
            "Add dynamic options", author=actor, committer=actor
        )
        logger.info("Commited options in %s", commit.hexsha)
        # Create the corrosponding tag and push
        tmp.repo.create_tag(
            tag_name,
            ref=commit,
            message="Automatic tag from LbAPI",
        )
        logger.info("Created tag %s from %s", tag_name, commit.hexsha)
        tmp.repo.remotes.origin.push(tag_name)
        logger.info("Pushed %s to remote", tag_name)
        return tag_name


async def submit_productions(
    project_id: int,
    commit_sha: str,
    mr_id: int,
    submitting_user: str,
    *,
    dry_run=False,
    allow_partial=False,
) -> RequestIds | None:
    async with SessionLocal() as session:
        query = sqlalchemy.select(webhooks.ProjectWebhook.type, webhooks.Pipeline.id)
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        project_type, pipeline_id = (await session.execute(query)).one()

    names, requests, data_pkg = await load_requests(
        project_type,
        pipeline_id,
        submitting_user=submitting_user,
        allow_partial=allow_partial,
    )

    with tempfile.TemporaryDirectory() as tmpdir:
        yaml_fn = Path(tmpdir) / "requests.yaml"
        yaml_fn.write_text(yaml.safe_dump(requests, sort_keys=False))
        logger.debug("Submitting with:\n%s", yaml_fn.read_text())
        ids_fn = Path(tmpdir) / "ids.json"
        cmd = settings.lb_dirac + ["dirac-production-request-submit"]
        cmd += [str(yaml_fn), f"--output-json={ids_fn}", "--create-filetypes"]
        if not dry_run:
            cmd += ["--submit"]

        with DownloadedProxyFile(submitting_user, "lhcb_user") as proxy_file:
            subprocess.check_call(
                cmd, env={**os.environ, "X509_USER_PROXY": proxy_file.name}
            )

        request_ids: RequestIds = json.loads(ids_fn.read_text())

    update_params = []
    for i, request_id, sub_request_ids in request_ids:
        et_to_jobid = names[i]
        if sub_request_ids:
            if set(et_to_jobid) != set(sub_request_ids):
                raise NotImplementedError(i, et_to_jobid, request_id, sub_request_ids)
            for event_type, sub_request_id in sub_request_ids.items():
                job_id = et_to_jobid[event_type]
                logger.info(
                    "job_id=%s was submitted as subrequest %s of request_id %s",
                    job_id,
                    sub_request_id,
                    request_id,
                )
                update_params.append(
                    {
                        "id": job_id,
                        "request_id": request_id,
                        "subrequest_id": sub_request_id,
                    }
                )
        elif len(et_to_jobid) == 1:
            job_id = list(et_to_jobid.values())[0]
            logger.info("job_id=%s was submitted as request %s", job_id, request_id)
            update_params.append({"id": job_id, "request_id": request_id})
        else:
            raise NotImplementedError(i, et_to_jobid, request_id, sub_request_ids)

    if dry_run:
        return None
    else:
        async with SessionLocal() as session:
            await session.execute(sqlalchemy.update(webhooks.Job), update_params)
            await session.commit()

        return request_ids


async def post_submission_tasks(
    project_id: int,
    commit_sha: str,
    mr_id: int,
) -> None:
    """Run post-submission tasks for a given project."""
    from . import MODULES_MAPPING

    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.ProjectWebhook.type,
            webhooks.Pipeline.id,
            sqlalchemy.func.group_concat(webhooks.Job.request_id),
        )
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        query = query.where(webhooks.CIRun.pipeline_id == webhooks.Pipeline.id)
        query = query.where(webhooks.Job.ci_run_id == webhooks.CIRun.id)
        query = query.group_by(webhooks.ProjectWebhook.type, webhooks.Pipeline.id)
        project_type, pipeline_id, raw_request_ids = (
            await session.execute(query)
        ).one()

    logger.info(
        "Running post submission tasks for %s %s %s %s",
        project_id,
        commit_sha,
        mr_id,
        raw_request_ids,
    )

    if raw_request_ids is None:
        raise NotImplementedError("No request_ids found")

    request_ids = {int(x) for x in raw_request_ids.split(",")}

    return await MODULES_MAPPING[project_type].post_submission_tasks(
        project_id, commit_sha, mr_id, list(request_ids)
    )


async def launch_production(
    project_type: WebhookType, request_id: int, append_name: str | None
):
    from . import MODULES_MAPPING

    return await MODULES_MAPPING[project_type].launch_production(
        request_id, append_name=append_name
    )


async def clean_repo(project_id: int, commit_sha: str, mr_id: int):
    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)
    if mr.squash:
        merge_commit = project.commits.get(mr.squash_commit_sha)
    else:
        merge_commit = project.commits.get(mr.merge_commit_sha)
    revert_info = merge_commit.revert(mr.target_branch)
    logger.info("Reverted %s %s !%s as %s", project_id, commit_sha, mr_id, revert_info)


def load_submitted_requests(
    request_type: str,
    request_author: str,
    *,
    parent_id: Optional[int] = None,
    all_states: bool = False,
):
    with (
        importlib.resources.path(
            "LbAPI.ci.templates", "lookup_submitted.py"
        ) as lookup_script,
        tempfile.NamedTemporaryFile() as output_file,
    ):
        cmd = settings.lb_dirac + [
            "python",
            lookup_script,
            "--server-credentials",
            dirac_host_cert.path,
            dirac_host_key.path,
            f"--output={output_file.name}",
            request_type,
            request_author,
        ]
        if parent_id:
            cmd += ["--parent-id", str(parent_id)]
        if all_states:
            cmd += ["--all-states"]
        subprocess.run(cmd, check=True)
        return json.loads(output_file.read())


async def fix_request(project_id, mr_id, commit_sha, dry_run, allow_submit):
    from .issue_opening import create_gitlab_issue

    note_out = "Attempting to fix request. Find details below \n"

    async with SessionLocal() as session:
        query = sqlalchemy.select(webhooks.Pipeline.id)
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        pipeline_id = (await session.execute(query)).one()[0]

    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)

    mr_merge = mr.merged_at
    if mr_merge is None:
        return "Merge request is not merged! This command won't work :) "

    merged_at = datetime.fromisoformat(mr_merge).replace(tzinfo=None)

    names, requests, data_pkg = await load_requests(
        "MC_REQUEST",
        pipeline_id,
        submitting_user=mr.merged_by["username"],
        validate=False,
    )

    request_types = {x["type"] for x in requests}
    assert len(request_types) == 1, request_types
    request_authors = {x["author"] for x in requests}
    assert len(request_authors) == 1, request_authors

    request_type = request_types.pop()
    request_author = request_authors.pop()

    all_states = True
    raw_existing_requests = load_submitted_requests(
        request_type, request_author, all_states=all_states
    )

    existing_requests = defaultdict(list)
    for req in raw_existing_requests:
        existing_requests[
            (
                req["RequestName"],
                req["RequestWG"],
                req["EventType"],
                tuple(req["FullProcPass"]),
            )
        ].append(
            (
                req["ParentID"],
                req["RequestID"],
                datetime.fromisoformat(req["crTime"]),
                req,
            )
        )

    found_request_ids = {}
    for et_to_jobid, request in zip(names, requests):
        full_proc_pass = (request["sim_condition"],) + tuple(
            x["processing_pass"] for x in request["steps"]
        )
        for event_type, ci_job_id in et_to_jobid.items():
            key = (request["name"], request["wg"], event_type, full_proc_pass)
            if key not in existing_requests:
                parent_key = (request["name"], request["wg"], None, full_proc_pass)
                if parent_key not in existing_requests:
                    continue
                assert len(existing_requests[parent_key]) == 1, (
                    "a",
                    parent_key,
                    existing_requests[parent_key],
                )
                _, parent_id, _, raw = existing_requests[parent_key][0]
                raw_existing_requests += [
                    raw | {k: v for k, v in req.items() if v is not None}
                    for req in load_submitted_requests(
                        request_type,
                        request_author,
                        parent_id=parent_id,
                        all_states=all_states,
                    )
                ]
                existing_requests = defaultdict(list)
                for req in raw_existing_requests:
                    existing_requests[
                        (
                            req["RequestName"],
                            req["RequestWG"],
                            req["EventType"],
                            tuple(req["FullProcPass"]),
                        )
                    ].append(
                        (
                            req["ParentID"],
                            req["RequestID"],
                            datetime.fromisoformat(req["crTime"]),
                            req,
                        )
                    )
            assert len(existing_requests[key]) == 1, ("b", key, existing_requests[key])
            parent_id, request_id, created_time, _ = existing_requests[key][0]
            (created_time - merged_at).total_seconds()
            # assert creation_delay > 0 and creation_delay < 60 * 60, creation_delay
            found_request_ids[ci_job_id] = (parent_id, request_id)

    update_params = []
    missed_submissions = []
    found_submissions = []

    async with SessionLocal() as session:
        query = select(
            webhooks.CIRun.name.label("ci_run_name"),
            webhooks.Job.id,
            webhooks.Job.status,
            webhooks.Job.request_id,
            webhooks.Job.subrequest_id,
            webhooks.Job.name.label("job_name"),
        )
        query = query.where(webhooks.CIRun.id == webhooks.Job.ci_run_id)
        query = query.where(webhooks.CIRun.pipeline_id == pipeline_id).where(
            ~webhooks.Job.outdated
        )
        for row in (await session.execute(query)).all():
            if row.request_id is not None:
                continue
            if found_request_ids.get(row.id) is None:
                missed_submissions.append(row)
                continue

            found_submissions.append(
                (row.id, row.ci_run_name, row.job_name, found_request_ids[row.id])
            )

            parent_id, request_id = found_request_ids[row.id]
            if parent_id:
                update_params.append(
                    {
                        "id": row.id,
                        "request_id": parent_id,
                        "subrequest_id": request_id,
                    }
                )
            else:
                update_params.append({"id": row.id, "request_id": request_id})

    if found_submissions:
        join_found = "\n\n".join(str(i) for i in found_submissions)
        note_out += "I have found a couple of submissions already submitted \n\n"
        note_out += join_found

    if missed_submissions and not allow_submit:
        note_out += "\n\n"
        note_out += "Some submissions appear to not have been submitted yet \n\n"

        join_missed = "\n".join(str(i) for i in missed_submissions)
        note_out += join_missed
        note_out += (
            "\n\nCheck carefully and then pass --allow-submit to allow resubmission\n\n"
        )
        note_out += "and --do-for-real if you want to clean up for real\n\n"
        return note_out
    else:
        note_out += "\n\n No missed submissions, proceed to clean up the merge request by passing --do-for-real\n\n"

    if dry_run:
        return note_out

    if update_params:
        async with SessionLocal() as session:
            await session.execute(sqlalchemy.update(webhooks.Job), update_params)
            await session.commit()

    if allow_submit and missed_submissions:
        await submit_productions(
            project_id, mr.sha, mr_id, mr.merged_by["username"], allow_partial=True
        )
        await post_submission_tasks(project_id, mr.sha, mr_id)

    await create_gitlab_issue(project_id, mr.sha, mr_id)
    await clean_repo(project_id, mr.sha, mr_id)

    note_out += "Clean up complete!"
    return note_out
