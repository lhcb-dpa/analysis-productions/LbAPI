###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import json
import logging

import sqlalchemy

from LbAPI.database import SessionLocal, webhooks
from LbAPI.settings import (
    gitlab,
    settings,
)

from .common import find_mr_and_issue, make_pipeline_url

ISSUE_BODY = """A series of productions have been created from {commit_sha} in !{mr_id}.

cc {inform}

### Production Details for Pipeline {pipeline_id}

* [Pipeline Summary]({pipeline_url})

#### Summary table

{job_table}

<details><summary>Metadata</summary>

```json
{metadata}
```
</details>
"""

logger = logging.getLogger(__name__)


async def create_gitlab_issue(project_id: int, commit_sha: str, mr_id: int):
    from . import MODULES_MAPPING

    mr, issue = find_mr_and_issue(project_id, commit_sha, mr_id)
    if issue:
        logger.info(
            "Issue already exists for %s %s %s: %s",
            project_id,
            commit_sha,
            mr_id,
            issue.web_url,
        )
        return

    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.ProjectWebhook.type,
            webhooks.ProjectWebhook.name,
            webhooks.Pipeline.id,
            webhooks.Pipeline.author,
        )
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        project_type, project_name, pipeline_id, author = (
            await session.execute(query)
        ).one()

    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.Job.id,
            webhooks.CIRun.name.label("ci_run_name"),
            webhooks.Job.name,
            webhooks.Job.spec,
            webhooks.Job.result,
            webhooks.Job.request_id,
            webhooks.Job.subrequest_id,
        )
        query = query.where(webhooks.CIRun.pipeline_id == pipeline_id)
        query = query.where(webhooks.Job.ci_run_id == webhooks.CIRun.id)
        query = query.where(webhooks.Job.outdated.is_(False))
        jobs = (await session.execute(query)).all()

    project = gitlab.api.projects.get(project_id)

    job_table = MODULES_MAPPING[project_type].issue_job_table(jobs)
    metadata = json.dumps(
        {
            "ci_ids": [j.id for j in jobs],
            "request_ids": list({j.request_id for j in jobs}),
        }
    )
    # TODO: Try to look up email addresses if possible
    inform = [
        f"@{x}"
        for x in jobs[-1].spec["request"][0]["inform"] + [author]
        if "@" not in x
    ]

    body = ISSUE_BODY.format(
        pipeline_url=make_pipeline_url(project_type, pipeline_id),
        commit_sha=commit_sha,
        mr_id=mr_id,
        inform=" ".join(inform),
        pipeline_id=pipeline_id,
        settings=settings,
        metadata=metadata,
        job_table=job_table,
    )

    mr = project.mergerequests.get(mr_id)
    labels = set(mr.labels) | {"Production::New"}
    wgs = {r["wg"] for j in jobs for r in j.spec["request"]}
    ci_runs = {j.ci_run_name for j in jobs}
    title = f"Productions for {'/'.join(wgs)}: {'/'.join(ci_runs)}"

    logger.info(
        "Creating issue for %s %s %s: %s\n%s",
        project_id,
        commit_sha,
        mr_id,
        title,
        body,
    )

    issue = project.issues.create({"title": title, "description": body})
    issue.labels = list(labels)
    issue.save()

    return title, body, labels
