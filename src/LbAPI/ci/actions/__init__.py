###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    "create_pipeline",
    "start_pipeline",
    "start_cirun",
    "start_job",
    "create_gitlab_issue",
    "pre_submission_tasks",
    "submit_productions",
    "post_submission_tasks",
    "launch_production",
    "clean_repo",
    "handle_note_hook",
)

import asyncio
import importlib.resources
import json
import logging
import os
import secrets
import shutil
import tempfile
from pathlib import Path
from textwrap import dedent
from typing import Any, Optional

import sqlalchemy
import yaml

from LbAPI.ci.actions import ana_prod, mc_request
from LbAPI.ci.actions.common import make_pipeline_url
from LbAPI.ci.actions.issue_opening import create_gitlab_issue
from LbAPI.ci.actions.opendata import LB_OD_USER, opendata_req
from LbAPI.ci.actions.request_sanity import determine_request_sanity
from LbAPI.ci.actions.submission import (
    clean_repo,
    launch_production,
    post_submission_tasks,
    pre_submission_tasks,
    submit_productions,
)
from LbAPI.ci.queries import (
    create_pipeline,
    insert_ci_runs,
    insert_jobs,
    mark_ci_run_failed,
    mark_pipeline_failed,
    set_job_status,
)
from LbAPI.ci.webhook import set_commit_status
from LbAPI.database import SessionLocal, webhooks
from LbAPI.dirac import DownloadedProxyFile, get_proxy_data
from LbAPI.enums import CIRunStatus, JobStatus, PipelineStatus, WebhookType
from LbAPI.settings import (
    gitlab,
    settings,
)
from LbAPI.utils import ExceptionForGitlab, Token, run

logger = logging.getLogger(__name__)


MODULES_MAPPING = {
    webhooks.WebhookType.ANA_PROD: ana_prod,
    webhooks.WebhookType.MC_REQUEST: mc_request,
}
N_SUBMIT_RETRIES = 5


async def start_pipeline(
    project_id: int, commit_sha: str, mr_id: int
) -> tuple[WebhookType, int, set]:
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.ProjectWebhook.type, webhooks.Pipeline.id, webhooks.Pipeline.author
        )
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        project_type, pipeline_id, author = (await session.execute(query)).one()

    async def fail_pipeline(reason: str):
        """Helper function to mark pipeline as failed and set commit status."""
        await mark_pipeline_failed(pipeline_id, reason)
        set_commit_status(project_type, project_id, commit_sha, pipeline_id, "failed")
        return project_type, pipeline_id, set()

    try:
        # Make sure the user has a proxy available
        get_proxy_data(author, "lhcb_user")

        project = gitlab.api.projects.get(project_id)
        mr = project.mergerequests.get(mr_id)

        base_sha = mr.attributes["diff_refs"]["base_sha"]
        changed_paths = set()
        for file in project.repository_compare(base_sha, commit_sha)["diffs"]:
            if file["deleted_file"]:
                continue
            changed_paths.add(file["new_path"])
        changed_files = [
            f
            for f in project.repository_tree(
                iterator=True, ref=commit_sha, recursive=True
            )
            if f["path"] in changed_paths
        ]

        ci_run_names = await MODULES_MAPPING[project_type].find_ciruns(changed_files)

        # Check if there are files without folders in them
        files_outside_dir = [f for f in changed_paths if "/" not in f]
        if files_outside_dir:
            return await fail_pipeline(
                f"File(s) found that do not reside in a folder: {files_outside_dir}. Please move them inside a folder!",
            )
        for name in ci_run_names:
            if not name.isascii():
                return await fail_pipeline(
                    f"Name ({name!r}) contains unicode characters. Please remove them!"
                )
            if len(name) > 64:
                return await fail_pipeline(
                    f"Name ({name!r}) should be less than 64 characters"
                )
        await insert_ci_runs(pipeline_id, ci_run_names)
    except ExceptionForGitlab as e:
        await fail_pipeline(str(e))
        raise
    except Exception:
        await fail_pipeline("Unknown error")
        logger.exception(
            f"Unknown exception caught while starting pipeline {pipeline_id} in {project_id}!{mr_id} {commit_sha}"
        )
        raise

    for name in ci_run_names:
        logger.info("Created ci run: %s %s", pipeline_id, name)

    return project_type, pipeline_id, ci_run_names


async def start_cirun(pipeline_id: int, name: str) -> dict[int, str]:
    try:
        return await _start_cirun_inner(pipeline_id, name)
    except Exception as e:
        await mark_pipeline_failed(
            pipeline_id, f"Failed to start {name!r} with error: {e}"
        )
        async with SessionLocal.begin() as session:
            query = sqlalchemy.select(
                webhooks.ProjectWebhook.type,
                webhooks.ProjectWebhook.project_id,
                webhooks.Pipeline.commit_sha,
            )
            query = query.where(
                webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
            )
            query = query.where(webhooks.Pipeline.id == pipeline_id)
            project_type, project_id, commit_sha = (await session.execute(query)).one()
        set_commit_status(project_type, project_id, commit_sha, pipeline_id, "failed")
    return {}


async def _start_cirun_inner(pipeline_id: int, name: str) -> dict[int, str]:
    logger.info("Starting ci run for: %s %s", pipeline_id, name)

    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(
            webhooks.ProjectWebhook.project_id,
            webhooks.ProjectWebhook.type,
            webhooks.Pipeline.mr_id,
            webhooks.Pipeline.commit_sha,
            webhooks.Pipeline.author,
            webhooks.CIRun.id,
        )
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.id == webhooks.CIRun.pipeline_id)
        query = query.where(webhooks.CIRun.pipeline_id == pipeline_id)
        query = query.where(webhooks.CIRun.name == name)
        project_id, project_type, mr_id, commit_sha, author, ci_run_id = (
            await session.execute(query)
        ).one()

    error = False
    log = ""
    project = gitlab.api.projects.get(project_id)
    try:
        error, log, job_specs, mr_labels = await MODULES_MAPPING[
            project_type
        ].find_jobs(project, mr_id, ci_run_id, author, commit_sha, name)
        invalid_job_names = [name for name in job_specs if len(name) > 100]
        if invalid_job_names:
            error = True
            log += (
                f"Job names should be less than 100 characters: {invalid_job_names!r}"
            )

        wgs = {r["wg"] for spec in job_specs.values() for r in spec["request"]}
        if wgs or mr_labels:
            mr = project.mergerequests.get(mr_id)
            existing_mr_labels = (
                set(mr.labels)
                if project_type == webhooks.WebhookType.ANA_PROD
                else set()  # cc roneil/cburr this is added for MCR benefit, as we want wiping labels :)
            )
            mr.labels = list(wgs | mr_labels | existing_mr_labels)
            try:
                mr.save()
            except Exception:
                logger.exception(
                    "Failed to update MR labels for %s!%s", project_id, mr_id
                )

    except Exception as e:
        error = True
        log += (
            f"Failed to start CI Run [{ci_run_id}] while finding jobs for {name}: {e}\n"
        )
        logger.exception(
            "find_jobs threw exception while starting CI Run [%d] for %s %s!%s",
            ci_run_id,
            name,
            project_id,
            mr_id,
        )

    if error:
        logger.debug(log)
        logger.warning("Failed to start ci run for: %s %s", pipeline_id, name)
        await mark_ci_run_failed(ci_run_id, log)
        set_commit_status(project_type, project_id, commit_sha, pipeline_id, "failed")
        return None

    logger.info("Creating %s jobs for %s %s", len(job_specs), pipeline_id, name)
    jobs = await insert_jobs(ci_run_id, log, job_specs)

    logger.info("Jobs created for %s %s with ids %r", pipeline_id, name, jobs)
    return jobs


async def start_job(job_id: int):
    raw_token, token = Token.generate("job")

    # Get information about the job
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.ProjectWebhook.project_id,
            webhooks.ProjectWebhook.name,
            webhooks.ProjectWebhook.type,
            webhooks.Pipeline.id,
            webhooks.Pipeline.author,
            webhooks.Pipeline.commit_sha,
            webhooks.Job.name,
            webhooks.Job.submitted_id,
            webhooks.Job.spec,
            webhooks.Job.outdated,
        )
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.id == webhooks.CIRun.pipeline_id)
        query = query.where(webhooks.CIRun.id == webhooks.Job.ci_run_id)
        query = query.where(webhooks.Job.id == job_id)
        (
            project_id,
            project_name,
            project_type,
            pipeline_id,
            author,
            commit_sha,
            job_name,
            submitted_id,
            spec,
            outdated,
        ) = (await session.execute(query)).one()
        if outdated:
            logger.info("Skipping outdated job %s", job_id)
            return

    await set_job_status(job_id, JobStatus.SUBMITTING, {"secret": token.hashed_token})

    data_pkg = None
    if spec.get("data_pkg"):
        data_pkg = spec["data_pkg"] | {"dynamic_files": spec["dynamic_files"]}

    # Submit the job to LHCbDIRAC
    status = JobStatus.SUBMITTED
    try:
        submitted_id = await _submit_job(
            project_type,
            pipeline_id,
            author,
            job_name,
            spec["request"],
            raw_token,
            data_pkg,
            spec.get("extra_test_cli_args"),
        )
    except Exception:
        logger.exception("Failed to submit job %s", job_id)
        status = JobStatus.CANCELLED
        submitted_id = f"error submitting {job_id}"

    await set_job_status(job_id, status, {"submitted_id": submitted_id})

    return submitted_id


async def _submit_job(
    pipeline_type: WebhookType,
    pipeline_id: int,
    author: str,
    job_name: str,
    request,
    token: str,
    data_pkg: Optional[dict[str, Any]] = None,
    extra_test_cli_args: Optional[list[str]] = None,
) -> str:
    config = {
        "token": token,
        "base_url": settings.lbapi_url,
    }
    if data_pkg:
        config["data_pkg"] = data_pkg
    if extra_test_cli_args:
        config["extra_test_cli_args"] = extra_test_cli_args

    with tempfile.TemporaryDirectory() as _tmpdir:
        tmpdir = Path(_tmpdir)

        request_yaml = tmpdir / "request.yaml"
        request_yaml.write_text(yaml.safe_dump(request, sort_keys=False))

        log_uploader = tmpdir / "log_uploader.py"
        log_uploader.write_text(
            importlib.resources.read_text("LbAPI.ci.templates", "log_uploader.py")
        )
        log_uploader.chmod(0o755)

        config_json = tmpdir / "config.json"
        config_json.write_text(json.dumps(config))

        job_script = tmpdir / "job_script.py"
        job_script.write_text(
            importlib.resources.read_text("LbAPI.ci.templates", "job_script.py")
        )
        job_script.chmod(0o755)

        extra_files = [request_yaml, log_uploader, config_json]

        if settings.ci_job_backend == "dirac":
            submit_func = _submit_dirac_job
        elif settings.ci_job_backend == "local":
            submit_func = _submit_local_job
        else:
            raise NotImplementedError(settings.ci_job_backend)
        job_id = await submit_func(
            pipeline_type,
            pipeline_id,
            job_name,
            tmpdir,
            author,
            job_script,
            extra_files,
        )

    return job_id


async def _submit_dirac_job(
    pipeline_type: WebhookType,
    pipeline_id: int,
    job_name: str,
    work_dir: os.PathLike,
    author: str,
    job_script: Path,
    extra_files: list[Path],
) -> str:
    for attempt in range(N_SUBMIT_RETRIES):
        with (
            importlib.resources.path(
                "LbAPI.ci.templates", "submit.py"
            ) as submit_script,
            DownloadedProxyFile(author, "lhcb_user") as proxy_file,
        ):
            cmd = settings.lb_dirac[:] + ["python", str(submit_script)]
            cmd += ["--pipeline-type", pipeline_type.name]
            cmd += ["--pipeline-id", str(pipeline_id)]
            cmd += ["--name", job_name]
            cmd += [str(job_script)]
            cmd += ["--extra-files"] + [str(x) for x in extra_files]

            returncode, stdout, stderr = await run(
                *cmd, cwd=work_dir, extra_env={"X509_USER_PROXY": proxy_file.name}
            )
            if returncode == 0:
                try:
                    result = json.loads(stdout)
                except json.JSONDecodeError as e:
                    logger.error("Failed to parse job submission result: %s", stdout)
                    raise RuntimeError(f"Invalid command output: {stdout}") from e
                if not result["success"]:
                    logger.error("Job submission status was not success: %s", result)
                    raise NotImplementedError(result)
                return f"dirac:{result['dirac_id']}"
        logger.error(
            "Failed to submit job %s %s with return code %s",
            pipeline_id,
            job_name,
            returncode,
        )
        logger.debug("cmd: %s", cmd)
        logger.debug("stdout: %s", stdout)
        logger.debug("stderr: %s", stderr)
        await asyncio.sleep(max(2**attempt, 60) + 5)

    raise RuntimeError(cmd, returncode, stdout, stderr)


async def _submit_local_job(
    pipeline_type: WebhookType,
    pipeline_id: int,
    job_name: str,
    work_dir: os.PathLike,
    author: str,
    job_script: Path,
    extra_files: list[Path],
) -> str:
    job_id = secrets.token_urlsafe(8)
    job_dir = settings.ci_job_local_dir / f"lbapi-{job_id}"
    job_dir.mkdir(parents=True)
    for path in extra_files + [job_script]:
        shutil.copy(path, job_dir)

    return f"local:{job_id}"


async def update_discussion(pipeline_id: int):
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            webhooks.ProjectWebhook.name,
            webhooks.ProjectWebhook.type,
            webhooks.ProjectWebhook.project_id,
            webhooks.Pipeline.commit_sha,
            webhooks.Pipeline.mr_id,
            webhooks.Pipeline.status,
        )
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.id == pipeline_id)
        project_name, project_type, project_id, commit_sha, mr_id, pipeline_status = (
            await session.execute(query)
        ).one()

    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)

    if mr.sha != commit_sha:
        logging.warning(
            f"update_discussion: MR SHA {mr.sha} does not match commit_sha "
            f"{commit_sha} of Pipeline - should run on the latest pipelines only - noop."
        )
        return

    header = """<!-- LbAPI Bot Summary -->"""

    # Make the body of the message
    body = f"""
    The pipeline is currently pending.
    This message will update as the pipeline progresses. You may also monitor the status
    [here]({make_pipeline_url(project_type, pipeline_id)}).
    """

    body = dedent(body).strip()
    if pipeline_status == PipelineStatus.ERROR:
        body = f"""
        It looks like the pipeline has failed to start.
        Please check the pipeline
        [here]({make_pipeline_url(project_type, pipeline_id)}) for the error message.

        This message will update when the pipeline is fixed :)
        """
        body = dedent(body).strip()
    elif pipeline_status == PipelineStatus.PROCESSED:
        async with SessionLocal() as session:
            query = sqlalchemy.select(
                webhooks.Job.id,
                webhooks.CIRun.name.label("ci_run_name"),
                webhooks.CIRun.status.label("ci_run_status"),
                webhooks.Job.name,
                webhooks.Job.spec,
                webhooks.Job.result,
                webhooks.Job.request_id,
                webhooks.Job.subrequest_id,
            )
            query = query.where(webhooks.CIRun.pipeline_id == pipeline_id)
            query = query.where(webhooks.Job.ci_run_id == webhooks.CIRun.id)
            query = query.where(webhooks.Job.outdated.is_(False))
            jobs = (await session.execute(query)).all()

        # update request sanity
        if project_type == webhooks.WebhookType.ANA_PROD:
            try:
                determine_request_sanity(mr, jobs)
            except Exception:
                logger.exception("Error while determining request sanity")

        for job in jobs:
            if job.ci_run_status == CIRunStatus.ERROR:
                body = f"""
                The pipeline has failed to start.
                Please check the pipeline
                [here]({make_pipeline_url(project_type, pipeline_id)}) for the error message.

                This message will update when the pipeline is fixed :)
                """
                body = dedent(body).strip()
                break

            if job.ci_run_status != CIRunStatus.READY_TO_SUBMIT:
                break
        else:
            body = MODULES_MAPPING[project_type].make_mr_summary_info(jobs)
            if "OpenData" in mr.labels and mr.author["username"] == LB_OD_USER:
                logger.info(
                    "Processing as OpenData request for %s!%s",
                    project_id,
                    mr_id,
                )
                try:
                    success = await opendata_req(jobs, pipeline_id, mr)
                    if not success:
                        logger.warning(
                            "OpenData request %s!%s - could not generate and upload Markdown file as jobs was empty.",
                            project_id,
                            mr_id,
                        )
                except Exception:
                    logger.exception(
                        "OpenData request %s!%s - could not generate and upload Markdown file due to exception.",
                        project_id,
                        mr_id,
                    )
    logger.info("Updating MR discussion for %s!%s:\n%s", project_id, mr_id, body)

    # Update the MR discussion
    try:
        for discussion in mr.notes.list(iterator=True):
            current_body = discussion.body
            if current_body == body:
                break
            if current_body.startswith(header):
                discussion.body = "\n".join([header, body])
                discussion.save()
                break
        else:
            discussion = mr.discussions.create({"body": "\n".join([header, body])})
    except Exception:
        logger.exception("Failed to update MR discussion for %s!%s", project_id, mr_id)


async def handle_note_hook(body) -> str:
    #! So far, only support Merge Request command hooks
    if body["object_attributes"]["noteable_type"] != "MergeRequest":
        return None

    project_id = body["project_id"]
    mr_id = body["merge_request"]["iid"]
    commit_sha = body["merge_request"]["last_commit"]["id"]

    async with SessionLocal() as session:
        query = sqlalchemy.select(webhooks.ProjectWebhook.type)
        query = query.where(
            webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
        )
        query = query.where(webhooks.Pipeline.project_id == project_id)
        query = query.where(webhooks.Pipeline.commit_sha == commit_sha)
        query = query.where(webhooks.Pipeline.mr_id == mr_id)
        project_type = (await session.execute(query)).one()[0]

    return await MODULES_MAPPING[project_type].do_note_hook(body)
