###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging
import os

import DIRAC
import sqlalchemy
from DIRAC.Core.Utilities.ReturnValues import SErrorException
from fastapi import FastAPI, Request, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from timing_asgi import TimingClient, TimingMiddleware
from timing_asgi.integrations import StarletteScopeToName

from LbAPI.settings import (
    dirac_host_cert,
    dirac_host_key,
    settings,
)

from .database import create_engine, dispose_engine
from .routers import (
    ci,
    eos_proxy,
    gitlab,
    pipelines,
    pipelines2,
    productions,
    stable,
    user,
)

logger = logging.getLogger(__name__)

description = """
This is the API that is used by the various components of the Analysis
Productions framework.

If you are using this service directly please contact
lhcb-dpa-wp2-managers@cern.ch so you can be informed of any changes.

## Usage

### Usage on this page

To interact with this page click "Authorize" and then login using the
"OpenIdConnect (OAuth2, authorization_code)" option.

### Usage from Python

Local kerberos credentials can be used to get a token using the
[`auth-get-sso-cookie`](https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie.git)
package.

```python
import requests
from auth_get_sso_cookie.cern_sso import device_authorization_login

api_url = "https://lbap.app.cern.ch"
token = device_authorization_login(f"{api_url}", "lhcb-analysis-productions", True, "auth.cern.ch", "cern")
r = requests.get(f"{api_url}/user", headers={"Authorization": f"Bearer {token['access_token']}"})
r.raise_for_status()
r.json()
```

## Pagination

Some endpoints use pagination to return partial responses. In this case the
`Content-Range` header is used to indicate the 0-based index of the first and
last result returned followed by the total number of results. Some examples are:

```
# The first 100 results
Content-Range: jobs 0-99/1234
# The last page of results
Content-Range: jobs 1200-1233/1234
# A single result
Content-Range: jobs 0-0/1
# An empty response
Content-Range: jobs */0
# If there are too many results to return the a trailing astrix may be used
Content-Range: pipelines 0-0/*
```

More information can be found in
[RFC-2616](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.35)
as well as
[Range header, I choose you (for pagination)!](http://otac0n.com/blog/2012/11/21/range-header-i-choose-you.html).
"""


if settings.sentry_dsn:
    from importlib.metadata import version

    import sentry_sdk

    release_string = version("LbAPI")
    sentry_sdk.init(  # pylint: disable=abstract-class-instantiated
        settings.sentry_dsn,
        release=release_string,
    )

app = FastAPI(
    title="API for LHCb Analysis Productions",
    description=description,
    version="0.0.1",
    contact={
        "name": "DPA-WP2",
        "url": "https://mattermost.web.cern.ch/lhcb/channels/dpa-wp2-analysis-productions",
        "email": "lhcb-dpa-wp2@cern.ch",
    },
    # license_info={
    #     "name": "Apache 2.0",
    #     "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    # },
    responses={
        status.HTTP_401_UNAUTHORIZED: {"description": "Could not validate credentials"},
    },
    swagger_ui_init_oauth={
        "clientId": "lhcb-analysis-productions",
        "scopes": "openid profile email",
    },
    openapi_tags=[
        {"name": "user"},
        {
            "name": "productions",
            "description": "Running/completed Analysis Productions that are known to LHCbDIRAC.",
        },
        {
            "name": "stable",
            "description": "Long-term and version interface for analysis preservation, "
            "primarily used by the local data clients.",
        },
        {
            "name": "pipelines",
            "description": "Testing data from the Analysis Production's data package CI.",
        },
    ],
)
app.include_router(user.router)
app.include_router(pipelines2.router)
app.include_router(pipelines.router)
app.include_router(productions.router)
app.include_router(stable.router)
app.include_router(gitlab.router)
app.include_router(ci.router)
app.include_router(eos_proxy.router)


class PrintTimings(TimingClient):
    logger = logger.getChild("timing")

    def timing(self, metric_name, timing, tags):
        self.logger.debug("Timing of %s (%s) was %s", metric_name, tags, timing)


app.add_middleware(
    TimingMiddleware,
    client=PrintTimings(),
    metric_namer=StarletteScopeToName(prefix="", starlette_app=app),
)

origins = [
    "http://127.0.0.1:3000",
    "http://127.0.0.1:5000",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:8080",
    "http://localhost:3000",
    "http://localhost:5000",
    "http://localhost:8000",
    "http://localhost:8080",
    "https://lhcb-analysis-productions-preview.web.cern.ch",
    "https://lhcb-analysis-productions.web.cern.ch",
    "https://lhcb-productions.web.cern.ch",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
async def startup():
    await create_engine()

    if dirac_host_cert and dirac_host_key:
        DIRAC.initialize(
            host_credentials=(
                dirac_host_cert.path,
                dirac_host_key.path,
            ),
        )
    elif "X509_USER_PROXY" in os.environ:
        DIRAC.initialize()


@app.on_event("shutdown")
async def shutdown():
    await dispose_engine()


@app.exception_handler(sqlalchemy.exc.NoResultFound)
async def no_result_handler(request: Request, exc: sqlalchemy.exc.NoResultFound):
    return JSONResponse(
        status_code=status.HTTP_404_NOT_FOUND,
        content={"message": "No result found in DB"},
    )


@app.exception_handler(SErrorException)
async def dirac_exception_handler(request: Request, exc: SErrorException):
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={"message": f"Got DIRAC error: {exc.result['Message']}"},
    )
