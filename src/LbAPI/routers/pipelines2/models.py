###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

__all__ = (
    "PipelineInfo",
    "CIRunInfo",
    "JobInfo",
)

from datetime import datetime
from typing import Annotated, Any, Optional, Union

import pydantic

from LbAPI.enums import CIRunStatus, JobStatus, PipelineStatus, WebhookType


class BaseModel(pydantic.BaseModel, extra="forbid"):
    pass


class PipelineInfo(BaseModel):
    class CommitInfo(BaseModel):
        sha: str
        message: str
        url: pydantic.HttpUrl

    class MRInfo(BaseModel):
        id: str
        name: str
        url: pydantic.HttpUrl

    class CIRunSummary(BaseModel):
        id: int
        name: str
        status: CIRunStatus
        job_statuses: dict[JobStatus, int]

    id: int
    project_id: int
    type: WebhookType
    created: datetime
    status: PipelineStatus
    author: str
    commit: PipelineInfo.CommitInfo
    mr: PipelineInfo.MRInfo
    ci_runs: list[PipelineInfo.CIRunSummary]


class MCTestInfo(BaseModel):
    n_generated: Optional[int] = None
    n_stored: Optional[int] = None
    size: Optional[int] = None
    cpu: Optional[float] = None


class MCOutputInfo(BaseModel):
    n_generated: Optional[int] = None
    n_requested: int
    size: Optional[int] = None
    gauss_cpu: Optional[float] = None


class APTestInfo(BaseModel):
    n_events: Optional[int] = None
    n_log: dict[str, int] = {}
    in_size: Optional[int] = None
    n_test_lfns: Optional[int] = None
    size: Optional[int] = None
    cpu: Optional[float] = None
    peak_mem: Optional[int] = None


class APOutputInfo(BaseModel):
    in_size: Optional[int] = None
    in_n_lfns: Optional[int] = None
    est_size: Optional[int] = None


class CIRunInfo(BaseModel):
    class JobSummary(BaseModel):
        id: int
        name: str
        status: JobStatus
        test: Union[MCTestInfo, APTestInfo]
        output: Union[MCOutputInfo, APOutputInfo]

    type: WebhookType
    id: int
    name: str
    status: CIRunStatus
    jobs: list[CIRunInfo.JobSummary]


class BaseJobSpec(pydantic.BaseModel, extra="allow"):
    request: Annotated[list[dict[str, Any]], pydantic.Field(min_length=1, max_length=1)]


class BaseJobResult(pydantic.BaseModel, extra="allow"):
    class StepInfo(BaseModel, extra="allow"):
        # class ResourceUsageSummary(BaseModel):
        #     class ResourceUsageMetric(BaseModel):
        #         max: float
        #         average: float

        #     rss: ResourceUsageMetric
        #     pss: ResourceUsageMetric
        #     vmem: ResourceUsageMetric
        #     swap: ResourceUsageMetric
        #     time_per_event: ResourceUsageMetric

        index: int
        # input:
        # output:
        # log_summary:
        # resources: Optional[BaseJobResult.StepInfo.ResourceUsageSummary]

    steps: list[BaseJobResult.StepInfo]


class JobInfo(BaseModel, from_attributes=True):
    id: int
    name: str
    created: datetime
    status: JobStatus
    submitted_id: Optional[str]
    spec: BaseJobSpec
    result: Optional[BaseJobResult]
    request_id: Optional[int]
    n_attempts: int = 0
