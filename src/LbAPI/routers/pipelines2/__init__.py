###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import logging
import re
import sys
import time
import urllib.parse
from datetime import datetime, timezone
from typing import Annotated, Optional

import opensearchpy.helpers
import sqlalchemy
import yaml
from fastapi import APIRouter, Depends, HTTPException, Query, status
from fastapi.responses import PlainTextResponse, StreamingResponse
from gitlab import GitlabGetError

from LbAPI.auth import User, get_current_user
from LbAPI.celery import pre_submission_tasks, start_job
from LbAPI.ci.queries import reset_ci_run, resubmit_job, set_job_status
from LbAPI.database import SessionLocal
from LbAPI.database import webhooks as db_models
from LbAPI.enums import CIRunStatus, JobStatus, WebhookType
from LbAPI.settings import (
    gitlab,
    opensearch,
    settings,
)
from LbAPI.utils import eos_artifact_path, eos_ls, generate_eos_token

from .models import (
    APOutputInfo,
    APTestInfo,
    CIRunInfo,
    JobInfo,
    MCOutputInfo,
    MCTestInfo,
    PipelineInfo,
)
from .queries import find_job_id, select_pipelines

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/pipelines2",
    tags=["pipelines2"],
    dependencies=[Depends(get_current_user)],
)


@router.get(
    "/",
    response_model=list[PipelineInfo],
    summary="List recently created pipelines",
)
async def list_pipelines(
    type: Optional[WebhookType] = None, project_id: Optional[int] = None
):
    async with SessionLocal() as session:
        results = await select_pipelines(
            session, project_type=type, project_id=project_id
        )
    return results


@router.get(
    "/{pipeline_id}/",
    response_model=PipelineInfo,
    summary="Get information about a specific pipeline",
)
async def pipeline_info(pipeline_id: int):
    async with SessionLocal() as session:
        results = await select_pipelines(session, pipeline_id)
    if not results:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return results[0]


@router.post(
    "/{pipeline_id}/retry-submission-check",
    summary="Retry the submission check for a pipeline",
)
async def retry_submission_check(
    pipeline_id: int, user: User = Depends(get_current_user)
):
    async with SessionLocal() as session:
        query = sqlalchemy.select(db_models.CIRun.name, db_models.CIRun.status)
        query = query.where(db_models.CIRun.pipeline_id == pipeline_id)
        async for name, ci_status in await session.stream(query):
            if ci_status != CIRunStatus.ERROR:
                raise HTTPException(
                    status_code=status.HTTP_409_CONFLICT,
                    detail=f"CI run {name} is in status {ci_status}",
                )

        query = sqlalchemy.select(db_models.Job.status).distinct()
        query = query.where(db_models.Job.ci_run_id == db_models.CIRun.id)
        query = query.where(db_models.CIRun.pipeline_id == pipeline_id)
        job_statuses = (await session.execute(query)).scalars().all()
        if job_statuses != [JobStatus.SUCCESS]:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=f"Jobs are not all in status SUCCESS: {job_statuses}",
            )

        query = sqlalchemy.select(
            db_models.Pipeline.project_id,
            db_models.Pipeline.commit_sha,
            db_models.Pipeline.mr_id,
        )
        query = query.where(db_models.Pipeline.id == pipeline_id)
        project_id, commit_sha, mr_id = (await session.execute(query)).one()

    await reset_ci_run(pipeline_id, user.username)
    task = pre_submission_tasks.delay(project_id, commit_sha, mr_id, None, dry_run=True)
    return {"success": True, "task_id": task.task_id}


@router.get(
    "/{pipeline_id}/log",
    response_class=PlainTextResponse,
    summary="Get the processing log for a pipeline",
)
async def pipeline_log(pipeline_id: int) -> str:
    async with SessionLocal() as session:
        query = sqlalchemy.select(db_models.Pipeline.status_info)
        query = query.where(db_models.Pipeline.id == pipeline_id)
        return (await session.execute(query)).one()[0]


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/",
    response_model=CIRunInfo,
    summary="Get information about a specific ci run",
)
async def ci_run_info(pipeline_id: int, ci_run_name: str):
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            db_models.ProjectWebhook.type.label("project_type"),
            db_models.CIRun.id,
            db_models.CIRun.name,
            db_models.CIRun.status,
        )
        query = query.where(
            db_models.ProjectWebhook.project_id == db_models.Pipeline.project_id
        )
        query = query.where(db_models.CIRun.pipeline_id == db_models.Pipeline.id)
        query = query.where(db_models.CIRun.pipeline_id == pipeline_id)
        query = query.where(db_models.CIRun.name == ci_run_name)
        ci_run = (await session.execute(query)).one()
        info = CIRunInfo(
            type=ci_run.project_type,
            id=ci_run.id,
            name=ci_run.name,
            status=ci_run.status,
            jobs=[],
        )

        last_row = sqlalchemy.text("'$[last]'")
        request = db_models.Job.spec["request"][0]
        steps = db_models.Job.result["steps"]

        to_select = [
            db_models.Job.id,
            db_models.Job.name,
            db_models.Job.status,
        ]
        if ci_run.project_type == WebhookType.MC_REQUEST:
            gauss_info = steps[0]["gaudi"]
            output_file = steps[last_row]["gaudi"]["summary_xml"]["output"][0]
            to_select += [
                request["event_types"][0]["num_events"].label("n_requested"),
                gauss_info["ChronoStatSvc"]["GiGa.GiGaMgr"].label("gauss_evt_time"),
                gauss_info["summary_xml"]["output"][0]["n_events"].label("n_generated"),
                output_file["size"].label("size_stored"),
                output_file["n_events"].label("n_stored"),
            ]
        elif ci_run.project_type == WebhookType.ANA_PROD:
            input_dataset = db_models.Job.spec["input-dataset"]
            resource_usage = db_models.Job.result["resource_usage"]
            to_select += [
                steps[0]["gaudi"]["summary_xml"]["input"].label("input_files"),
                steps[last_row]["dirac"]["bookkeeping_xml"]["output"].label(
                    "output_files"
                ),
                input_dataset["size"].label("full_in_size"),
                input_dataset["num-lfns"].label("full_in_n_lfns"),
                input_dataset["n_test_lfns"].label("n_test_lfns"),
                resource_usage["exec_time"].label("exec_time"),
                resource_usage["peak_mem"].label("peak_mem"),
                db_models.Job.result["log_summary"].label("log_summary"),
            ]
        else:
            raise NotImplementedError(ci_run.project_type)

        query = sqlalchemy.select(*to_select)
        query = query.where(db_models.Job.ci_run_id == ci_run.id)
        query = query.where(db_models.Job.outdated.is_(False))
        query = query.order_by(db_models.Job.name.desc())
        async for row in await session.stream(query):
            success = row.status == JobStatus.SUCCESS

            if ci_run.project_type == WebhookType.MC_REQUEST:
                test = MCTestInfo(
                    n_generated=row.n_generated,
                    n_stored=row.n_stored if success else None,
                    size=row.size_stored if success else None,
                )
                output = MCOutputInfo(n_requested=row.n_requested)
                if success and row.n_generated and row.n_stored:
                    n_generated = row.n_requested * row.n_stored / row.n_generated
                    output.n_generated = n_generated
                    if row.size_stored:
                        output.size = row.size_stored / row.n_stored * row.n_requested
                    if row.gauss_evt_time:
                        output.gauss_cpu = row.gauss_evt_time * n_generated
            elif ci_run.project_type == WebhookType.ANA_PROD:
                test = APTestInfo(
                    n_log=row.log_summary or {},
                    n_test_lfns=row.n_test_lfns,
                    cpu=row.exec_time,
                    peak_mem=row.peak_mem,
                )
                # The size isn't known until the end of the job
                if row.input_files and all("size" in f for f in row.input_files):
                    test.n_events = sum(f["n_events"] for f in row.input_files)
                    test.in_size = sum(f["size"] for f in row.input_files)
                if row.output_files:
                    test.size = sum(f["size"] for f in row.output_files)

                output = APOutputInfo(
                    in_n_lfns=row.full_in_n_lfns,
                    in_size=row.full_in_size,
                )
                if test.size and test.in_size:
                    output.est_size = row.full_in_size / test.in_size * test.size
            else:
                raise NotImplementedError(ci_run.project_type)

            info.jobs.append(
                CIRunInfo.JobSummary(
                    id=row.id,
                    name=row.name,
                    status=row.status,
                    output=output,
                    test=test,
                )
            )

    return info


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/log",
    response_class=PlainTextResponse,
    summary="Get the processing log for a ci run",
)
async def ci_run_log(pipeline_id: int, ci_run_name: str):
    async with SessionLocal() as session:
        query = sqlalchemy.select(db_models.CIRun.status_info)
        query = query.where(db_models.CIRun.pipeline_id == pipeline_id)
        query = query.where(db_models.CIRun.name == ci_run_name)
        return (await session.execute(query)).one()[0]


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/",
    response_model=JobInfo,
    summary="Get information about a specific job",
)
async def job_info(pipeline_id: int, ci_run_name: str, job_name: str):
    async with SessionLocal() as session:
        query = sqlalchemy.select(db_models.Job)
        query = query.where(db_models.CIRun.pipeline_id == pipeline_id)
        query = query.where(db_models.CIRun.name == ci_run_name)
        query = query.where(db_models.CIRun.id == db_models.Job.ci_run_id)
        query = query.where(db_models.Job.name == job_name)
        query = query.where(db_models.Job.outdated.is_(False))
        job = (await session.execute(query)).one()[0]
        job_info = JobInfo.model_validate(job)

        query = sqlalchemy.select(sqlalchemy.func.count())
        query = query.where(db_models.Job.ci_run_id == job.ci_run_id)
        query = query.where(db_models.Job.name == job.name)
        job_info.n_attempts = (await session.execute(query)).one()[0]
    return job_info


async def _artifact_path(
    pipeline_id: int, ci_run_name: str, job_name: str
) -> tuple[str, str]:
    query = sqlalchemy.select(
        db_models.ProjectWebhook.project_id,
        db_models.ProjectWebhook.eos_artifact_path,
        db_models.Pipeline.id.label("pipeline_id"),
        db_models.CIRun.name.label("ci_run_name"),
        db_models.Job.id.label("job_id"),
        db_models.Job.name.label("job_name"),
    )
    query = query.where(
        db_models.Pipeline.project_id == db_models.ProjectWebhook.project_id
    )
    query = query.where(db_models.Pipeline.id == db_models.CIRun.pipeline_id)
    query = query.where(db_models.CIRun.id == db_models.Job.ci_run_id)
    query = query.where(db_models.Pipeline.id == pipeline_id)
    query = query.where(db_models.CIRun.name == ci_run_name)
    query = query.where(db_models.Job.name == job_name)
    query = query.where(db_models.Job.outdated.is_(False))
    async with SessionLocal() as db:
        try:
            row = (await db.execute(query)).one()
        except sqlalchemy.exc.NoResultFound as exc:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Job not found"
            ) from exc
    kwargs = dict(row._mapping)
    project_id = kwargs.pop("project_id")
    upload_domain, upload_path = eos_artifact_path(**kwargs)
    return project_id, upload_domain, row.eos_artifact_path, upload_path, row.job_id


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/files",
    summary="Get the XRootD access URLs for the output files of a job",
)
async def job_files(
    pipeline_id: int, ci_run_name: str, job_name: str
) -> dict[str, str]:
    _, upload_domain, _, upload_path, _ = await _artifact_path(
        pipeline_id, ci_run_name, job_name
    )
    try:
        paths = await eos_ls(upload_path)
    except FileNotFoundError:
        return {}
    return {
        filename: f"root://{upload_domain}/{upload_path.rstrip('/')}/{filename}"
        for filename in paths
    }


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/signed-file-url",
    summary="Request a signed URL for a specific file",
)
async def job_signed_file_url(
    pipeline_id: int,
    ci_run_name: str,
    job_name: str,
    filename: Annotated[
        str | None, Query(max_length=128, pattern=r"^00012345_00006789_\d+\..+$")
    ] = None,
    user: User = Depends(get_current_user),
):
    if filename is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="No filename"
        )
    project_id, _, base_path, upload_path, job_id = await _artifact_path(
        pipeline_id, ci_run_name, job_name
    )
    path = f"{upload_path}/{filename}"

    expires = datetime.fromtimestamp(
        time.time() + 15 * 60,
        tz=timezone.utc,
    )
    eos_token, eos_token_info = await generate_eos_token(
        directory=path,
        allow_write=False,
        owner=user.username,
        expires=expires,
        tree=False,
    )
    logger.info("Issuing EOS token for %s: %s", path, eos_token_info)
    assert path.startswith(f"{base_path}/"), (path, base_path)
    url = f"/eos_proxy/browse_artifact/{project_id}/{pipeline_id}/{ci_run_name}/{job_id}/{job_name}/{filename}"
    url += f"?xrd.wantprot=unix&authz={urllib.parse.quote(eos_token)}"
    return {"success": True, "url": url}


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/logs/",
    response_model=list[str],
    summary="List the available log files for a given job",
)
async def job_list_logs(pipeline_id: int, ci_run_name: str, job_name: str):
    async with SessionLocal() as session:
        job_id = await find_job_id(session, pipeline_id, ci_run_name, job_name)

    async with opensearch.client() as client:
        results = await client.search(
            body={
                "query": {"bool": {"must": [{"term": {"job_id": job_id}}]}},
                "aggs": {
                    "filenames": {
                        "terms": {"field": "name", "size": 1000},
                    }
                },
            },
            params=dict(size=0),
            index=f"{settings.app_logs_index}-*",
        )

    if "aggregations" not in results:
        return []
    filenames = [x["key"] for x in results["aggregations"]["filenames"]["buckets"]]
    if len(filenames) >= 500:
        raise NotImplementedError(f"Too many filenames ({len(filenames)})")
    pattern = re.compile(r".+_(\d+)\..+")
    return sorted(
        filenames,
        key=lambda x: (
            (int(pattern.fullmatch(x).group(1)), x)
            if pattern.fullmatch(x)
            else (sys.maxsize, x)
        ),
    )


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/logs/{filename}",
    response_class=StreamingResponse,
    summary="Get a specific log file from a job",
)
async def job_log_file(
    pipeline_id: int, ci_run_name: str, job_name: str, filename: str
):
    async with SessionLocal() as session:
        job_id = await find_job_id(session, pipeline_id, ci_run_name, job_name)

    if filename == "prmon.txt":
        # Stream the merged prmon log files
        async def streaming_response():
            headings = []
            all_data = []
            filenames = await job_list_logs(pipeline_id, ci_run_name, job_name)
            for filename in filenames:
                if filename.startswith("prmon_"):
                    raw_data = "".join(
                        [
                            line
                            async for line in _read_file_from_opensearch(
                                job_id, filename
                            )
                        ]
                    )
                    headings, *data = [
                        line.split("\t") for line in raw_data.splitlines()
                    ]
                    all_data.append([[int(x) for x in row] for row in data])
            if not all_data or len(all_data[0]) <= 1:
                yield ""
                return

            # Merge the data, modifying the wtime to be relative to the first job
            result = "\t".join(headings) + "\n"
            idx_time = headings.index("Time")
            idx_wtime = headings.index("wtime")
            start_time = all_data[0][0][idx_time]
            for data in all_data:
                for row in data:
                    row[idx_wtime] = row[idx_time] - start_time
                    result += "\t".join(map(str, row)) + "\n"
                yield result
                result = ""

        return StreamingResponse(streaming_response())
    else:
        return StreamingResponse(_read_file_from_opensearch(job_id, filename))


async def _read_file_from_opensearch(job_id, filename):
    query = {
        "bool": {"must": [{"term": {"job_id": job_id}}, {"term": {"name": filename}}]}
    }

    async with opensearch.client() as client:
        async for doc in opensearchpy.helpers.async_scan(
            client,
            query={"query": query, "sort": [{"timestamp": {"order": "asc"}}]},
            index=f"{settings.app_logs_index}-*",
            preserve_order=True,
        ):
            yield doc["_source"]["data"] + "\n"


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/files",
    response_model=list[str],
    summary="Get list of initial files for the specified pipeline run",
)
async def cirun_initial_yamls(pipeline_id: int, ci_run_name: str):
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            db_models.ProjectWebhook.type,
            db_models.Pipeline.project_id,
            db_models.Pipeline.mr_id,
            db_models.Pipeline.commit_sha,
        )
        query = query.where(
            db_models.ProjectWebhook.project_id == db_models.Pipeline.project_id
        )
        query = query.where(db_models.Pipeline.id == pipeline_id)
        project_type, project_id, mr_id, commit_sha = (
            await session.execute(query)
        ).one()

    project = gitlab.api.projects.get(project_id)
    mr = project.mergerequests.get(mr_id)

    new_files = {
        f["path"]: f
        for f in project.repository_tree(iterator=True, ref=commit_sha, recursive=True)
    }
    old_files = {
        f["path"]: f
        for f in project.repository_tree(
            iterator=True, ref=mr.diff_refs["start_sha"], recursive=True
        )
    }
    changed_files = {
        path: info
        for path, info in new_files.items()
        if old_files.get(path, {}).get("id") != info["id"]
    }
    if project_type == WebhookType.ANA_PROD:
        changed_files = {
            path: info
            for path, info in changed_files.items()
            if path.endswith("info.yaml")
        }

    return [
        "".join(key.split(f"{ci_run_name}/"))
        for key in changed_files.keys()
        if f"{ci_run_name}/" in key
    ]


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/files/{filename}",
    response_class=PlainTextResponse,
    summary="Get a specific log file from a job",
)
async def cirun_initial_yaml_files(pipeline_id: int, ci_run_name: str, filename: str):
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            db_models.Pipeline.project_id,
            db_models.Pipeline.mr_id,
            db_models.Pipeline.commit_sha,
        )
        query = query.where(db_models.Pipeline.id == pipeline_id)
        project_id, mr_id, commit_sha = (await session.execute(query)).one()

    project = gitlab.api.projects.get(project_id)
    try:
        f = project.files.get(file_path=f"{ci_run_name}/{filename}", ref=commit_sha)
    except GitlabGetError as e:
        if e.response_code == 404:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND) from e
        raise

    return f.decode()


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/yaml",
    response_class=PlainTextResponse,
    summary="Get the DIRAC production request yaml for a specific job",
)
async def job_yaml(pipeline_id: int, ci_run_name: str, job_name: str):
    async with SessionLocal() as session:
        job_id = await find_job_id(session, pipeline_id, ci_run_name, job_name)
        query = sqlalchemy.select(db_models.Job.spec)
        query = query.where(db_models.Job.id == job_id)
        (spec,) = (await session.execute(query)).one()
    return yaml.safe_dump(spec["request"], sort_keys=False)


@router.get(
    "/{pipeline_id}/runs/{ci_run_name}/yaml",
    response_class=PlainTextResponse,
    summary="Get the DIRAC production request yaml for an entire CI run",
)
async def cirun_yaml(pipeline_id: int, ci_run_name: str):
    from LbAPI.ci.actions.submission import load_requests

    async with SessionLocal() as session:
        query = sqlalchemy.select(db_models.ProjectWebhook.type)
        query = query.where(
            db_models.ProjectWebhook.project_id == db_models.Pipeline.project_id
        )
        query = query.where(db_models.Pipeline.id == pipeline_id)
        (project_type,) = (await session.execute(query)).one()

    _, requests, _ = await load_requests(
        project_type, pipeline_id, ci_run_name=ci_run_name, validate=False
    )

    return yaml.safe_dump(requests, sort_keys=False)


@router.post(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/cancel",
    summary="Cancel an active job",
)
async def cancel_job(pipeline_id: int, ci_run_name: str, job_name: str):
    async with SessionLocal() as session:
        job_id = await find_job_id(session, pipeline_id, ci_run_name, job_name)

    try:
        await set_job_status(job_id, JobStatus.CANCELLED)
    except Exception as e:
        logger.exception("Failed to cancel job %s", job_id)
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=f"Failed to cancel job: {e}"
        ) from None
    return {"success": True}


@router.post(
    "/{pipeline_id}/runs/{ci_run_name}/jobs/{job_name}/retry",
    summary="Retry a job",
)
async def retry_job(pipeline_id: int, ci_run_name: str, job_name: str):
    async with SessionLocal() as session:
        job_id = await find_job_id(session, pipeline_id, ci_run_name, job_name)
    try:
        new_job_id = await resubmit_job(job_id)
    except Exception as e:
        logger.exception("Failed to resubmit job %s", job_id)
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail=f"Failed to resubmit job: {e}"
        ) from None
    start_job.delay(new_job_id)
    return {"success": True}
