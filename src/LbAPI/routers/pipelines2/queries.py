###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helper module of queries for the pipelines2 router that are non-trivial."""
from __future__ import annotations

from collections import defaultdict
from typing import Optional

import sqlalchemy

from LbAPI.database import SessionLocal
from LbAPI.database import webhooks as db_models
from LbAPI.enums import WebhookType

from .models import PipelineInfo


async def select_pipelines(
    session: SessionLocal,
    pipeline_id: Optional[int] = None,
    project_type: Optional[WebhookType] = None,
    project_id: Optional[int] = None,
) -> list[PipelineInfo]:
    query = sqlalchemy.select(
        db_models.ProjectWebhook.project_id,
        db_models.ProjectWebhook.name.label("project_name"),
        db_models.ProjectWebhook.type,
        db_models.Pipeline.id,
        db_models.Pipeline.created,
        db_models.Pipeline.status,
        db_models.Pipeline.author,
        db_models.Pipeline.commit_sha,
        db_models.Pipeline.commit_message,
        db_models.Pipeline.mr_id,
    ).where(db_models.ProjectWebhook.project_id == db_models.Pipeline.project_id)
    if project_type is not None:
        query = query.where(db_models.ProjectWebhook.type == project_type)
    if project_id is not None:
        query = query.where(db_models.ProjectWebhook.project_id == project_id)
    if pipeline_id is not None:
        query = query.where(db_models.Pipeline.id == pipeline_id)
    query = query.order_by(db_models.Pipeline.id.desc())
    query = query.limit(100)
    results = {}
    async for row in await session.stream(query):
        commit_url = (
            f"https://gitlab.cern.ch/{row.project_name}/-/commit/{row.commit_sha}"
        )
        mr_url = (
            f"https://gitlab.cern.ch/{row.project_name}/-/merge_requests/{row.mr_id}/"
        )
        mr_name = f"{row.project_name}!{row.mr_id}"
        results[row.id] = PipelineInfo(
            id=row.id,
            project_id=row.project_id,
            type=row.type,
            created=row.created,
            status=row.status,
            author=row.author,
            commit=PipelineInfo.CommitInfo(
                sha=row.commit_sha, message=row.commit_message, url=commit_url
            ),
            mr=PipelineInfo.MRInfo(id=str(row.mr_id), name=mr_name, url=mr_url),
            ci_runs=[],
        )

    query = sqlalchemy.select(
        db_models.CIRun.pipeline_id,
        db_models.CIRun.id,
        db_models.CIRun.name,
        db_models.CIRun.status,
    )
    query = query.where(db_models.CIRun.pipeline_id.in_(results))
    async for row in await session.stream(query):
        results[row.pipeline_id].ci_runs.append(
            PipelineInfo.CIRunSummary(
                id=row.id,
                name=row.name,
                status=row.status,
                job_statuses={},
            )
        )

    query = sqlalchemy.select(
        db_models.CIRun.id,
        db_models.Job.status,
        sqlalchemy.func.count(db_models.Job.id).label("status_count"),
    )
    query = query.where(db_models.CIRun.id == db_models.Job.ci_run_id)
    query = query.where(db_models.CIRun.pipeline_id.in_(results))
    query = query.where(db_models.Job.outdated.is_(False))
    query = query.group_by(
        db_models.CIRun.pipeline_id, db_models.CIRun.name, db_models.Job.status
    )
    job_statuses_by_ids = defaultdict(dict)
    async for row in await session.stream(query):
        job_statuses_by_ids[row.id][row.status] = row.status_count
    for pipeline in results.values():
        for ci_run in pipeline.ci_runs:
            ci_run.job_statuses = job_statuses_by_ids[ci_run.id]

    return list(results.values())


async def find_job_id(
    session: SessionLocal, pipeline_id: int, ci_run_name: str, job_name: str
) -> int:
    query = sqlalchemy.select(db_models.Job.id)
    query = _add_condition(query, pipeline_id, ci_run_name, job_name)
    (job_id,) = (await session.execute(query)).one()
    return job_id


def _add_condition(
    query,
    pipeline_id: int,
    ci_run_name: Optional[str] = None,
    job_name: Optional[str] = None,
):
    query = query.where(db_models.CIRun.pipeline_id == pipeline_id)
    if ci_run_name:
        query = query.where(db_models.CIRun.name == ci_run_name)
    if job_name:
        query = query.where(db_models.Job.ci_run_id == db_models.CIRun.id)
        query = query.where(db_models.Job.name == job_name)
        query = query.where(db_models.Job.outdated.is_(False))
    return query
