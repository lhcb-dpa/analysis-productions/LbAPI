###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from datetime import datetime
from typing import Optional

from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from fastapi import APIRouter, Depends, Query
from pydantic import BaseModel

from ..auth import get_apc, get_untrusted_user


class Sample(BaseModel):
    wg: str
    analysis: str
    version: str
    name: str

    request_id: int
    sample_id: int
    state: str
    owners: list[str]

    lfns: dict[str, list[str]]
    available_bytes: int
    total_bytes: int

    last_state_update: datetime
    validity_start: datetime
    validity_end: Optional[datetime]


router = APIRouter(
    prefix="/stable/v1",
    tags=["stable"],
    dependencies=[Depends(get_untrusted_user)],
)


@router.get(
    "/",
    response_model=dict[str, list[str]],
    summary="Get the list of known WGs and analyses",
)
async def list_analyses(
    at_time: Optional[datetime] = Query(None), apc=Depends(get_apc)
):
    return returnValueOrRaise(apc.listAnalyses(at_time=at_time))


@router.get(
    "/{wg}/{analysis}",
    response_model=list[Sample],
    summary="Get the list of samples for a specific analysis",
)
async def samples(
    wg: str,
    analysis: str,
    at_time: Optional[datetime] = Query(None),
    apc=Depends(get_apc),
):
    ret = apc.getProductions(
        wg=wg,
        analysis=analysis,
        with_lfns=True,
        with_pfns=True,
        with_transformations=False,
        at_time=at_time,
    )
    return returnValueOrRaise(ret)


@router.get(
    "/{wg}/{analysis}/tags",
    response_model=dict[str, dict[str, str]],
    summary="Get mapping of sample_id to tag for a specific analysis",
)
async def tags(
    wg: str,
    analysis: str,
    at_time: Optional[datetime] = Query(None),
    apc=Depends(get_apc),
):
    ret = apc.getTags(
        wg=wg,
        analysis=analysis,
        at_time=at_time,
    )
    return returnValueOrRaise(ret)
