###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

from datetime import datetime
from typing import Annotated, Any, Optional, Union

from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from fastapi import APIRouter, Body, Depends, HTTPException, Query, Response, status
from fastapi.responses import StreamingResponse
from LbAPCommon.config import known_working_groups
from pydantic import BaseModel

from ..auth import User, get_apc, get_current_user
from ..celery.dirac_prod_output import (
    get_ancestors,
    get_summary,
    list_possible_summary_filenames,
)


class StepInfo(BaseModel):
    stepID: int
    application: str
    extras: list[str]
    options: Union[list[str], dict[str, Any]]


class TransformationInfo(BaseModel):
    id: int
    status: str
    input_query: dict[str, Any]
    steps: list[StepInfo]
    used: bool


class SampleSummary(BaseModel):
    wg: str
    analysis: str
    version: str
    name: str

    request_id: int
    filetype: str
    sample_id: int
    state: str
    # TODO: Remove
    owners: list[str]

    tags: dict[str, str]

    merge_request: str | None = None
    jira_task: str | None = None
    last_state_update: datetime
    validity_start: datetime
    validity_end: datetime | None = None

    housekeeping_interaction_due: datetime
    progress: float | None = None
    publications: list[str] | None = None

    transformations: list[TransformationInfo] | None = None
    lfns: None | list[str] | dict[str, list[str]] = None


class Sample(SampleSummary):
    lfns: dict[str, list[str]]
    available_bytes: int
    total_bytes: int

    transformations: list[TransformationInfo]


class DatasetSpec(BaseModel):
    version: str
    name: str
    sample_id: int


class DatasetAssignPubSpec(BaseModel):
    datasets: list[DatasetSpec]
    publication_number: str


class DatasetTagModSpec(BaseModel):
    dataset: DatasetSpec
    old_tags: dict[str, str]
    new_tags: dict[str, str]


class GenericDatasetActionStatus(BaseModel):
    status: str


router = APIRouter(
    prefix="/productions",
    tags=["productions"],
    dependencies=[Depends(get_current_user)],
)


@router.get(
    "/",
    # TODO: When we switch from listAnalyses2 to listAnalyses we can have the model again
    # response_model=dict[str, list[str]],
    summary="Get the list of known WGs and analyses",
)
async def list_analyses(
    new_style: bool = False, at_time: datetime | None = None, apc=Depends(get_apc)
):
    if new_style:
        return returnValueOrRaise(
            apc.listAnalyses2() if not at_time else apc.listAnalyses2(at_time=at_time)
        )
    else:
        return returnValueOrRaise(apc.listAnalyses())


@router.get(
    "/-/requests",
    # response_model=dict[str, list[str]],
    summary="Get the list of requests which can be added to analyses",
)
async def list_requests(apc=Depends(get_apc)):
    return returnValueOrRaise(apc.listRequests())


@router.get(
    "/{wg}/{analysis}",
    response_model=list[SampleSummary],
    summary="Get summary of all productions for a specific analysis",
)
async def get_samples(
    wg: str,
    analysis: str,
    at_time: Optional[datetime] = Query(None),
    with_lfns: bool = Query(False),
    with_pfns: bool = Query(False),
    with_transformations: bool = Query(False),
    apc=Depends(get_apc),
):
    ret = apc.getProductions(
        wg=wg,
        analysis=analysis,
        with_lfns=with_lfns,
        with_pfns=with_pfns,
        with_transformations=with_transformations,
        at_time=at_time,
    )
    samples = returnValueOrRaise(ret)

    tags = returnValueOrRaise(apc.getTags(wg=wg, analysis=analysis, at_time=at_time))
    for sample in samples:
        sample["tags"] = tags[str(sample["sample_id"])]

    # # TODO: Remove
    # for sample in samples:
    #     if "owners" in sample:
    #         sample.pop("owners")

    return samples


@router.get(
    "/{wg}/{analysis}/owners",
    response_model=list[str],
    summary="Get the owners of all productions for a specific analysis",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Analysis not found"},
    },
)
def get_owners(wg: str, analysis: str, apc=Depends(get_apc)):
    return returnValueOrRaise(apc.getOwners(wg=wg, analysis=analysis))


@router.post(
    "/{wg}/{analysis}/owners",
    summary="Set the owners of all productions for a specific analysis",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Analysis not found"},
    },
)
def set_owners(
    wg: str,
    analysis: str,
    new_owners: list[str],
    user=Depends(get_current_user),
):
    apc_elevated = get_elevated_apc(wg, analysis, user)
    returnValueOrRaise(
        apc_elevated.setOwners(wg=wg, analysis=analysis, owners=new_owners)
    )
    return {"status": "success"}


@router.get(
    "/{wg}/{analysis}/{version}/{name}",
    response_model=Sample,
    summary="Get complete information for a specific sample",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Sample not found"},
    },
)
async def get_sample(
    wg: str,
    analysis: str,
    version: str,
    name: str,
    at_time: Optional[datetime] = Query(None),
    apc=Depends(get_apc),
):
    # TODO: Remove
    # if "owners" in sample:
    #     sample.pop("owners")

    return get_sample_(
        wg=wg,
        analysis=analysis,
        version=version,
        name=name,
        apc=apc,
        with_lfns=True,
        with_pfns=True,
        with_transformations=True,
        at_time=at_time,
        tags=True,
    )


@router.post(
    "/{wg}/{analysis}/tags",
    response_model=GenericDatasetActionStatus,
    summary="Request that dataset(s) tags be updated",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Sample not found"},
        status.HTTP_403_FORBIDDEN: {
            "description": "You do not have permissions to do that."
        },
    },
)
async def tags_update(
    wg: str,
    analysis: str,
    dataset_tagmod_specs: Annotated[list[DatasetTagModSpec], Body(min_items=1)],
    user=Depends(get_current_user),
):
    apc_elevated = get_elevated_apc(wg, analysis, user)
    verify_sample_ids_in_analysis(
        wg, analysis, [x.dataset for x in dataset_tagmod_specs], user
    )
    dataset_old_tags = {
        spec.dataset.sample_id: spec.old_tags for spec in dataset_tagmod_specs
    }
    dataset_new_tags = {
        spec.dataset.sample_id: spec.new_tags for spec in dataset_tagmod_specs
    }
    returnValueOrRaise(apc_elevated.setTags(dataset_old_tags, dataset_new_tags))
    return {"status": "success"}


def get_elevated_apc(
    wg: str, analysis: str, user: User, *, allow_create_analysis=False
):
    """Get an AnalysisProductionsClient with elevated permissions

    Raises a HTTP 403 if the user does not have permission to act upon the given analysis.
    """
    from LHCbDIRAC.ProductionManagementSystem.Client.AnalysisProductionsClient import (
        AnalysisProductionsClient,
    )

    if wg.lower() not in {k.lower() for k in known_working_groups}:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Working group not found, known values are: {known_working_groups}",
        )

    apc_elevated = AnalysisProductionsClient()

    owners = set(returnValueOrRaise(apc_elevated.getOwners(wg=wg, analysis=analysis)))
    if allow_create_analysis and not owners:
        returnValueOrRaise(
            apc_elevated.setOwners(wg=wg, analysis=analysis, owners=[user.email])
        )
        owners = set(
            returnValueOrRaise(apc_elevated.getOwners(wg=wg, analysis=analysis))
        )

    permitted = False

    # (a) they have the admin role
    if "admin" in user.roles:
        permitted = True

    # (b) they "own" the dataset
    if user.email in owners:
        permitted = True

    # (c) they have the liaison role (TODO)

    if not permitted:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"User not permitted to modify sample {wg}/{analysis}",
        )

    # At this point we can be sure the user actually referred to this sample.
    return apc_elevated


def verify_sample_ids_in_analysis(
    wg: str,
    analysis: str,
    datasets: list[DatasetSpec],
    user: User,
) -> list[int]:
    """Select samples under an analysis that the user has permissions to modify.

    TODO: This should be done in LHCbDIRAC instead of here...

    Args:
        wg (str): Analysis WG.
        analysis (str): Analysis Name
        datasets (list): Datasets requested to be selected
        user: user

    Raises:
        HTTPException: Raised if sample(s) cannot be found, or
                       user does not have appropriate permissions.

    Returns:
        list: List of selected datasets the user is allowed to touch.
    """

    apc_elevated = get_elevated_apc(wg, analysis, user)

    versions = list(set(d.version for d in datasets))
    ret = apc_elevated.getProductions(
        wg=wg,
        analysis=analysis,
        version=versions[0] if len(versions) == 1 else None,
        # We should bear in mind that some datasets have empty names and the query may be ambiguous
        name=(
            (datasets[0].name if datasets[0].name != "" else None)
            if len(datasets) == 1
            else None
        ),
        with_lfns=False,
        with_pfns=False,
        with_transformations=False,
    )

    samples = {sample["sample_id"]: sample for sample in returnValueOrRaise(ret)}
    if len(samples) == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Sample not found for analysis",
        )

    selected_sample_ids = []
    for specified_dset in datasets:
        if sample := samples.get(specified_dset.sample_id):
            if (
                sample["version"] == specified_dset.version
                and sample["name"] == specified_dset.name
                and sample["sample_id"] == specified_dset.sample_id
            ):
                selected_sample_ids.append(sample["sample_id"])
                continue
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=(
                f"Sample {specified_dset.version}/{specified_dset.name} "
                f"sample_id {specified_dset.sample_id} not found"
            ),
        )

    if len(selected_sample_ids) == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No samples were selected"
        )

    # Assuming now the user has the right permissions at this point
    return selected_sample_ids


@router.post(
    "/{wg}/{analysis}/add-samples",
    response_model=GenericDatasetActionStatus,
    summary="Request that dataset(s) be archived",
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": "You do not have permissions to do that."
        },
    },
)
def add_samples(
    wg: str,
    analysis: str,
    requests: Annotated[list[tuple[int, str]], Body(min_items=1)],
    user=Depends(get_current_user),
):
    apc_elevated = get_elevated_apc(wg, analysis, user, allow_create_analysis=True)
    returnValueOrRaise(apc_elevated.addRequestsToAnalysis(wg, analysis, requests))
    return {"status": "success"}


@router.post(
    "/{wg}/{analysis}/archive",
    response_model=GenericDatasetActionStatus,
    summary="Request that dataset(s) be archived",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Sample not found"},
        status.HTTP_403_FORBIDDEN: {
            "description": "You do not have permissions to do that."
        },
    },
)
async def archive(
    wg: str,
    analysis: str,
    datasets: Annotated[list[DatasetSpec], Body(min_items=1)],
    user=Depends(get_current_user),
    at_time: Union[datetime, None] = None,
):
    apc_elevated = get_elevated_apc(wg, analysis, user)
    selected_samples = verify_sample_ids_in_analysis(wg, analysis, datasets, user)
    if at_time:
        returnValueOrRaise(
            apc_elevated.archiveSamplesAtSpecificTime(selected_samples, at_time)
        )
    else:
        returnValueOrRaise(apc_elevated.archiveSamples(selected_samples))

    return {"status": "success"}


# /assign_publication
@router.post(
    "/{wg}/{analysis}/assign_publication",
    response_model=GenericDatasetActionStatus,
    summary="Request that dataset(s) be archived",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Sample not found"},
        status.HTTP_403_FORBIDDEN: {
            "description": "You do not have permissions to do that."
        },
    },
)
async def assign_publication(
    wg: str,
    analysis: str,
    body: Annotated[DatasetAssignPubSpec, Body()],
    user=Depends(get_current_user),
):
    if len(body.publication_number) > 64:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Publication number too long (>64chars)",
        )

    apc_elevated = get_elevated_apc(wg, analysis, user)
    selected_samples = verify_sample_ids_in_analysis(wg, analysis, body.datasets, user)
    returnValueOrRaise(
        apc_elevated.addPublication(selected_samples, body.publication_number)
    )

    return {"status": "success"}


@router.get(
    "/{wg}/{analysis}/{version}/{name}/summaryXML/{summaryFilename}",
    summary=(
        "EXPERIMENTAL: Get a merged summary XML for a given sample."
        "summaryFilename format: summary{app_name}_{transformID}_{step_index}.xml"
    ),
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Sample not found"},
        status.HTTP_202_ACCEPTED: {
            "description": "The task to create the merged summaries has been submitted"
        },
    },
)
async def get_summary_XML(
    wg: str,
    analysis: str,
    version: str,
    name: str,
    summaryFilename: str,
    apc=Depends(get_apc),
):
    sample = get_sample_(wg, analysis, version, name, apc=apc)
    if summaryFilename not in list_possible_summary_filenames(
        sample["transformations"]
    ):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="SummaryXML not found"
        )

    # check if a merged summary exists on S3
    response = get_summary(wg, analysis, version, name, summaryFilename)
    if response is None:
        # merged summary doesn't exist: create a task to make new ones
        # fetch_all_summary_XML_task.delay(wg, analysis, version, name)
        return Response("Task created", status_code=status.HTTP_202_ACCEPTED)
    elif response == "0":
        # if empty, a task is already running to create the summary.
        return Response("In progress", status_code=status.HTTP_202_ACCEPTED)
    else:
        # return S3 object
        return StreamingResponse(
            response,
            media_type="text/xml",
        )


@router.get(
    "/{wg}/{analysis}/{version}/{name}/ancestors",
    summary="EXPERIMENTAL: Get ancestor LFNs for a specific sample",
    responses={
        status.HTTP_404_NOT_FOUND: {"description": "Sample not found"},
    },
)
async def get_sample_ancestors(
    wg: str,
    analysis: str,
    version: str,
    name: str,
    user=Depends(get_current_user),
    apc=Depends(get_apc),
):
    sample = get_sample_(
        wg, analysis, version, name, apc, with_lfns=True, with_transformations=False
    )
    return await get_ancestors(sample["lfns"], user.username)


def get_sample_(
    wg,
    analysis,
    version,
    name,
    apc=None,
    with_lfns=False,
    with_pfns=False,
    with_transformations=True,
    tags=False,
    at_time=None,
    show_archived=False,
    require_has_publication=False,
):
    if apc is None:
        from LHCbDIRAC.ProductionManagementSystem.Client.AnalysisProductionsClient import (
            AnalysisProductionsClient,
        )

        apc = AnalysisProductionsClient()

    ret = apc.getProductions(
        wg=wg,
        analysis=analysis,
        version=version,
        name=name,
        with_lfns=with_lfns,
        with_pfns=with_pfns,
        with_transformations=with_transformations,
        at_time=at_time,
        # show_archived=show_archived,
        # require_has_publication=require_has_publication,
    )
    samples = returnValueOrRaise(ret)
    if len(samples) == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Sample not found"
        )
    elif len(samples) > 1:
        raise NotImplementedError(samples)
    sample = samples[0]

    if tags:
        tags = returnValueOrRaise(
            apc.getTags(wg=wg, analysis=analysis, at_time=at_time)
        )
        sample["tags"] = tags[str(sample["sample_id"])]

    return sample
