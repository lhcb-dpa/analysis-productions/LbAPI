###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from datetime import datetime, timedelta, timezone

from fastapi import APIRouter, Depends, HTTPException, status
from pydantic import BaseModel, Field
from sqlalchemy import func, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from ..auth import (
    USER_TOKEN_INACTIVITY_TIMEOUT,
    AuthTypes,
    User,
    get_current_user,
    get_untrusted_user,
)
from ..database import extra_func, get_db
from ..database.models import UserToken
from ..utils import Token

router = APIRouter(
    prefix="/user",
    tags=["productions"],
)


@router.get(
    "/",
    response_model=User,
    tags=["user"],
    summary="Get information about the current user",
)
async def user(user: User = Depends(get_untrusted_user)):
    """Get information about the current user"""
    return user


class TokenInfo(BaseModel, from_attributes=True):
    id: int
    created: datetime
    last_used: datetime
    owner: str
    description: str
    expires: datetime
    revoked: bool


class TokenSpec(BaseModel):
    description: str = Field(default="", max_length=256)
    expires: datetime = Field(
        default_factory=lambda: datetime.now(tz=timezone.utc) + timedelta(days=7),
        title="The token's expiration",
    )


@router.get(
    "/tokens/",
    dependencies=[Depends(get_current_user)],
    response_model=list[TokenInfo],
    tags=["user"],
    summary="List the tokens owned by the current user",
)
async def tokens(
    user: User = Depends(get_current_user), db: AsyncSession = Depends(get_db)
):
    """List the tokens owned by the current user"""
    query = select(UserToken)
    query = query.filter(UserToken.owner == user.username)
    query = query.filter(UserToken.expires > extra_func.utcnow())
    query = query.filter(
        func.date_add(UserToken.last_used, USER_TOKEN_INACTIVITY_TIMEOUT)
        > extra_func.utcnow()
    )
    query = query.filter(~UserToken.revoked)
    async with db.begin():
        result = await db.execute(query)
        return [TokenInfo.model_validate(v) for v, in result]


@router.post(
    "/tokens/create",
    dependencies=[Depends(get_current_user)],
    response_model=str,
    status_code=status.HTTP_201_CREATED,
    tags=["user"],
    summary="Create a new token for the current user",
)
async def make_token(
    spec: TokenSpec,
    user: User = Depends(get_current_user),
    db: AsyncSession = Depends(get_db),
):
    raw_token, token = Token.generate("user")
    db_item = UserToken(
        token=token.hashed_token,
        owner=user.username,
        description=spec.description,
        expires=spec.expires,
    )
    async with db.begin():
        db.add(db_item)
    return raw_token


@router.post(
    "/tokens/{token_id}/revoke",
    responses={status.HTTP_404_NOT_FOUND: {"description": "Token not found"}},
    tags=["user"],
    summary="Revoke a token owned by the current user",
)
async def revoke_specific_token(
    token_id: int,
    user: User = Depends(get_current_user),
    db: AsyncSession = Depends(get_db),
):
    return await _revoked_token(token_id, user.username, db)


@router.post(
    "/tokens/revoke",
    responses={
        status.HTTP_400_BAD_REQUEST: {
            "description": "Only user tokens can be used with this endpoint"
        },
        status.HTTP_404_NOT_FOUND: {"description": "Token not found"},
    },
    tags=["user"],
    summary="Revoke the token that is currently being used to authenticate",
)
async def revoke_current_token(
    user: User = Depends(get_untrusted_user),
    db: AsyncSession = Depends(get_db),
):
    if user.auth_type == AuthTypes.USER_TOKEN:
        return await _revoked_token(user.token_id, user.username, db)
    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail="Only user tokens can be used with this endpoint",
    )


async def _revoked_token(token_id: int, username: str, db: AsyncSession):
    query = update(UserToken)
    query = query.where(UserToken.owner == username)
    query = query.where(UserToken.id == token_id)
    query = query.values(revoked=True)
    async with db.begin():
        result = await db.execute(query)
        if result.rowcount == 0:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail=f"ID {token_id} not found"
            )
    return {"detail": f"ID {id} has been revoked"}
