###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import json
import logging
import re
import time
from datetime import datetime, timezone
from typing import Any, Optional

import pydantic
import sqlalchemy
from fastapi import APIRouter, Body, Depends, Header, HTTPException, Request, status
from opensearchpy.helpers import async_bulk

from LbAPI.celery import start_job
from LbAPI.ci.queries import resubmit_job, set_job_status
from LbAPI.ci.webhook import merge_request_hook, note_hook
from LbAPI.database import SessionLocal, webhooks
from LbAPI.enums import ALLOWED_JOB_TRANSITIONS, JobStatus, WebhookType
from LbAPI.responses import credentials_exception
from LbAPI.settings import (
    opensearch,
    settings,
)
from LbAPI.utils import (
    Token,
    eos_artifact_path,
    generate_eos_token,
    is_cern_ip,
)

logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/ci",
    tags=["ci"],
)


class JobInfo(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    project_id: int
    eos_artifact_path: Optional[str]
    type: WebhookType
    pipeline_id: int
    ci_run_id: int
    ci_run_name: str
    id: int
    name: str
    status: JobStatus
    author: str
    outdated: bool


class LogMessage(pydantic.BaseModel, extra=pydantic.Extra.forbid):
    timestamp: str
    path: str
    name: str
    line_number: int
    data: str


async def get_job_info(
    request: Request,
    authorization: str = Header(...),
) -> JobInfo:
    """Use the authorization header to validate a job's credentials."""
    logger.debug("Authentication request for get_job_info from %s", request.client.host)
    if match := re.fullmatch(r"Bearer (.+)", authorization):
        token = match.group(1)
    else:
        raise credentials_exception
    if not token.startswith("lbapi:"):
        raise credentials_exception

    token = Token(token)
    if token.purpose != "job":
        logger.warning("Unknown token purpose %r", token.purpose)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Unrecognised token purpose: {token.purpose!r}",
        )

    query = sqlalchemy.select(
        webhooks.ProjectWebhook.project_id,
        webhooks.ProjectWebhook.type,
        webhooks.ProjectWebhook.eos_artifact_path,
        webhooks.Pipeline.id.label("pipeline_id"),
        webhooks.Pipeline.author,
        webhooks.CIRun.id.label("ci_run_id"),
        webhooks.CIRun.name.label("ci_run_name"),
        webhooks.Job.id,
        webhooks.Job.name,
        webhooks.Job.status,
        webhooks.Job.outdated,
    )
    query = query.where(
        webhooks.Pipeline.project_id == webhooks.ProjectWebhook.project_id
    )
    query = query.where(webhooks.Pipeline.id == webhooks.CIRun.pipeline_id)
    query = query.where(webhooks.CIRun.id == webhooks.Job.ci_run_id)
    query = query.where(webhooks.Job.secret == token.hashed_token)
    async with SessionLocal() as db:
        try:
            row = (await db.execute(query)).one()
        except sqlalchemy.exc.NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Job not found"
            ) from None

    if row.outdated or row.status == JobStatus.CANCELLED:
        raise HTTPException(
            status_code=status.HTTP_410_GONE, detail="Job has been deleted"
        )

    if row.status == JobStatus.FAILED or not ALLOWED_JOB_TRANSITIONS[row.status]:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Job is not in a valid state for modification",
        ) from None

    job = JobInfo(**row._mapping)
    logger.debug("Used %s token to authorize job %r", token.purpose, job.id)
    return job


@router.post(
    "/webhook",
    summary="Set the status of the current job",
)
async def receive_webhook(
    request: Request,
    x_gitlab_token: str = Header(None),
    x_gitlab_event: str = Header(None),
):
    """Use the authorization header to validate a job's credentials."""
    logger.debug(
        "Authentication request for event %r receive_webhook from %s",
        x_gitlab_event,
        request.client.host,
    )
    if not x_gitlab_token:
        logger.debug("receive_webhook: GitLab Token was empty/not supplied")
        raise credentials_exception
    if not x_gitlab_token.startswith("lbapi:"):
        logger.debug("receive_webhook: GitLab Token did not start with lbapi:")
        raise credentials_exception
    if not is_cern_ip(request.client.host):
        logger.warning("Webhook token used from non-CERN IP %s", request.client.host)
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Webhook tokens are only valid from CERN IPs",
        )
    token = Token(x_gitlab_token)
    if token.purpose != "webhook":
        logger.warning("receive_webhook: GitLab Token parsed but had incorrect purpose")
        raise credentials_exception
    body = json.loads((await request.body()).decode())

    query = sqlalchemy.select(
        webhooks.ProjectWebhook.project_id,
        webhooks.ProjectWebhook.name,
        webhooks.ProjectWebhook.type,
    )
    query = query.where(webhooks.ProjectWebhook.secret == token.hashed_token)
    async with SessionLocal() as db:
        try:
            project_id, project_name, webhook_type = (await db.execute(query)).one()
        except sqlalchemy.exc.NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Project not found"
            ) from None
    logger.info("Got webhook for project %s (%s)", project_id, project_name)
    logger.debug("Webhook data for event %s: %r", x_gitlab_event, json.dumps(body))

    detail = "No action taken"
    if x_gitlab_event == "Merge Request Hook":
        detail = str(await merge_request_hook(webhook_type, project_id, body))
    elif x_gitlab_event == "Note Hook":
        detail = str(await note_hook(body))
    return {"success": True, "detail": detail}


@router.post(
    "/job/retry",
    summary="Retry a job",
)
async def retry_job(job_info: JobInfo = Depends(get_job_info)):
    if job_info.status not in {JobStatus.FAILED}:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Job is not in a valid state for retrying",
        )
    new_job_id = await resubmit_job(job_info.id, automatic=True)
    if new_job_id:
        start_job.delay(new_job_id)
    return {"success": True, "new_job_id": new_job_id}


@router.post(
    "/job/status",
    summary="Set the status of the current job",
)
async def set_status(
    new_status: JobStatus,
    result: Optional[dict[str, Any]] = Body(None),
    job_info=Depends(get_job_info),
):
    if new_status not in ALLOWED_JOB_TRANSITIONS[job_info.status]:
        logger.debug(
            "Forbidden transition for %r: %r -> %r",
            job_info.id,
            job_info.status,
            new_status,
        )
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"{job_info.status} -> {new_status} is not a valid transition",
        )
    extra_values = {}
    if new_status in (JobStatus.SUCCESS, JobStatus.FAILED, JobStatus.RUNNING):
        if result is None and new_status == JobStatus.SUCCESS:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Result must be provided for SUCCESS status",
            )
        if result is not None:
            extra_values["result"] = result
    elif result is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Result must not be provided for non-final statuses",
        )
    logger.debug("Updating job %r to status %r", job_info.id, new_status)
    await set_job_status(job_info.id, new_status, extra_values)
    return {"success": True}


@router.post(
    "/job/logs",
    summary="Set the status of the current job",
)
async def add_logs(
    messages: list[LogMessage],
    job_info=Depends(get_job_info),
):
    index_name = _log_index(job_info.id)

    template = {
        "_index": index_name,
        "type": job_info.type,
        "pipeline_id": job_info.pipeline_id,
        "job_id": job_info.id,
    }

    async with opensearch.client() as client:
        try:
            n_actions = await async_bulk(
                client, actions=[template | message.dict() for message in messages]
            )
        except TimeoutError:
            logger.error(
                "Timeout while adding %s logs to %s", len(messages), index_name
            )
            raise HTTPException(
                status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
                detail="Timeout while adding logs to opensearch",
            ) from None
        logger.info("Added %s actions to %s", n_actions, index_name)
    return {"success": True}


@router.get(
    "/job/upload-credentials",
    summary="Get information about where to upload output data from tests",
)
async def upload_info(
    job_info: JobInfo = Depends(get_job_info),
):
    if job_info.status not in {JobStatus.RUNNING, JobStatus.VALIDATING}:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail="Job is not in a valid state for uploading",
        )
    if job_info.eos_artifact_path is None:
        return {
            "success": False,
            "detail": "Uploading output data is not enabled for this project",
        }

    upload_domain, upload_path = eos_artifact_path(
        job_info.eos_artifact_path,
        job_info.pipeline_id,
        job_info.ci_run_name,
        job_info.id,
        job_info.name,
    )

    expires = datetime.fromtimestamp(
        time.time() + 15 * 60,
        tz=timezone.utc,
    )
    eos_token, eos_token_info = await generate_eos_token(
        directory=upload_path,
        allow_write=True,
        owner=job_info.author,
        expires=expires,
    )
    logger.info("Issuing EOS token for %s: %s", upload_path, eos_token_info)

    return {
        "success": True,
        "base_url": f"root://{upload_domain}/{upload_path}",
        "token": f"?xrd.wantprot=unix&authz={eos_token}",
        "expires": expires.isoformat(),
    }


def _log_index(job_id: int) -> str:
    suffix = job_id // settings.app_logs_index_size * settings.app_logs_index_size
    return f"{settings.app_logs_index}-{suffix}"
