###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import base64
import binascii
import json
import logging
import os
import ssl
import time
import urllib.parse
from datetime import datetime, timezone
from typing import Annotated, Optional

import httpx
import sqlalchemy
from cachetools import TTLCache, cached
from fastapi import APIRouter, Depends, Header, HTTPException, status
from fastapi.responses import JSONResponse, StreamingResponse
from starlette.background import BackgroundTask

from LbAPI.auth import User, get_current_user
from LbAPI.config import LOG_DB_BASE, LOG_SE_BASE
from LbAPI.database import SessionLocal
from LbAPI.database import webhooks as db_models
from LbAPI.utils import _make_eos_safe_name, eos_exists, generate_eos_token, safe_eos_ls

PROXIED_HEADERS = {
    "content-range",
    "content-length",
    "content-encoding",
    "date",
    "server",
    "etag",
    "last-modified",
}
CERTS_DIR = "/cvmfs/lhcb.cern.ch/etc/grid-security/certificates/"


logger = logging.getLogger(__name__)

router = APIRouter(
    prefix="/eos_proxy",
    tags=["misc"],
)


def check_lfn(log_lfn: str) -> tuple[str, JSONResponse | None]:
    """
    Check if the log_lfn is valid
    """

    log_lfn = os.path.normpath(log_lfn)
    # avoid api abuse (EOS scanning)
    if not log_lfn.startswith("/lhcb/"):
        return log_lfn, JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={"message": "Invalid file path"},
        )
    return log_lfn, None


@cached(cache=TTLCache(maxsize=1, ttl=60 * 60))
def get_httpx_client() -> httpx.AsyncClient:
    """
    Get an HTTPX client with the CERN CA certificates
    """
    ssl_context = ssl.create_default_context()
    ssl_context.load_verify_locations(capath=CERTS_DIR)
    return httpx.AsyncClient(verify=ssl_context)


@router.get(
    "/logs/list-prod",
    summary="List all productions",
)
async def list_prod(user: User = Depends(get_current_user)):
    """
    List all productions and subproductions
    """
    async with SessionLocal() as session:
        query = sqlalchemy.select(
            db_models.CacheEntries.value, db_models.CacheEntries.created
        )
        query = query.where(db_models.CacheEntries.key == "available_logs")
        result = await session.execute(query)
        tree, last_updated = result.one()
    return {"tree": json.loads(tree), "last_updated": last_updated}


@router.get(
    "/logs/list-subprod/{log_lfn:path}",
    summary="List one prod's subproductions",
)
async def list_subprod(
    log_lfn: str,  # format: XXX/XXXX/LOG/XXXXXXXX
    user: User = Depends(get_current_user),
):
    """
    List all subproductions of a production (format: list[str: 'XXXX'])
    """

    log_lfn, error = check_lfn(log_lfn)
    if error:
        return error

    db = set(
        i[:-3]
        for i in await safe_eos_ls(f"{LOG_DB_BASE}/{log_lfn}")
        if len(i) == 7 and i.endswith(".db")
    )
    zp = set(i for i in await safe_eos_ls(f"{LOG_SE_BASE}/{log_lfn}") if len(i) == 4)

    return [{"name": i, "legacy": i not in db} for i in sorted(db | zp)]


@router.get(
    "/logs/list-legacy/{log_lfn:path}",
    summary="List jobs of a subproductions (under the legacy zip format)",
)
async def list_legacy(
    log_lfn: str,  # XXX/XXXX/LOG/XXXXXXXX/XXXX
    user: User = Depends(get_current_user),
):
    """
    List all jobs of a subproduction (legacy zip format) from logSE (format: list[str: 'XXXXXXXX'])
    """

    log_lfn, error = check_lfn(log_lfn)
    if error:
        return error

    zp = sorted(
        i[:-4]
        for i in await safe_eos_ls(f"{LOG_SE_BASE}/{log_lfn}")
        if len(i) == 12 and i.endswith(".zip")
    )

    return zp


async def _sign_logs_eos_token(
    base: str,
    log_lfn: str,
    user: User,
    db: bool = False,
):
    path = f"{base}/{log_lfn}"

    expires = datetime.fromtimestamp(
        time.time() + 15 * 60,
        tz=timezone.utc,
    )
    eos_token, eos_token_info = await generate_eos_token(
        directory=path,
        allow_write=False,
        owner=user.username,
        expires=expires,
        tree=False,
    )
    logger.info("Issuing EOS token for %s: %s", path, eos_token_info)
    token = base64.urlsafe_b64encode(eos_token.encode()).rstrip(b"=").decode()
    url = f"{router.prefix}/logs/get{'' if db else '-legacy'}/{token}/{urllib.parse.quote(log_lfn)}"
    return {"legacy": not db, "url": url}


@router.get(
    "/logs/check-sign/{log_lfn:path}",
    summary="Check if the provided subproduction exists and sign it",
)
async def check_sign_logs_eos_token(
    log_lfn: str,  # XXX/XXXX/LOG/XXXXXXXX/XXXX
    user: User = Depends(get_current_user),
):
    """
    Check if the provided subproduction exists and sign it
    """

    log_lfn, error = check_lfn(log_lfn)
    if error:
        return error

    # if both exist, prefer the database
    if log_lfn.count("/") >= 4:
        if await eos_exists(f"{LOG_DB_BASE}/{log_lfn}.db"):
            return await _sign_logs_eos_token(
                LOG_DB_BASE, f"{log_lfn}.db", user, db=True
            )

        if await eos_exists(f"{LOG_SE_BASE}/{log_lfn}"):
            return await _sign_logs_eos_token(LOG_SE_BASE, log_lfn, user, db=False)

    return {"legacy": None, "url": None}  # not found


@router.get(
    "/logs/sign-legacy/{log_lfn:path}",
    summary="Sign a URL for log files from logSE",
)
async def sign_logs_legacy_eos_token(
    log_lfn: str,  # XXX/XXXX/LOG/XXXXXXXX/XXXX
    user: User = Depends(get_current_user),
):
    """
    Sign a URL for log files from logSE
    """

    log_lfn, error = check_lfn(log_lfn)
    if error:
        return error

    return await _sign_logs_eos_token(LOG_SE_BASE, log_lfn, user, db=False)


@router.get(
    "/logs/sign/{log_lfn:path}",
    summary="Sign a URL for log database from logArchive",
)
async def sign_logs_eos_token(
    log_lfn: str,  # XXX/XXXX/LOG/XXXXXXXX/XXXX.db
    user: User = Depends(get_current_user),
):
    """
    Sign a URL for log database from
    """

    log_lfn, error = check_lfn(log_lfn)
    if error:
        return error

    return await _sign_logs_eos_token(LOG_DB_BASE, log_lfn, user, db=True)


def parse_token(log_lfn: str) -> tuple[str, str, JSONResponse | None]:
    """
    Parse the token and return the log_lfn and authz
    """

    try:
        eos_token, log_lfn = log_lfn.split("/", 1)
        authz = base64.urlsafe_b64decode(f"{eos_token}===").decode()
    except (TypeError, ValueError, UnicodeDecodeError, binascii.Error):
        authz = ""

    return (
        log_lfn,
        authz,
        (
            JSONResponse(
                status_code=status.HTTP_400_BAD_REQUEST,
                content={"message": "Invalid token"},
            )
            if not authz
            else None
        ),
    )


async def _logs_eos_proxy(
    base: str,
    log_lfn: str,
    range: Annotated[Optional[str], Header()] = None,
    head: bool = False,
):

    log_lfn, authz, error = parse_token(log_lfn)

    if error:
        return error

    return await inner_eos_proxy(
        base, log_lfn, authz, range, verb="HEAD" if head else "GET"
    )


@router.get(
    "/logs/get-legacy/{log_lfn:path}",
    summary="Get log files from logSE",
)
async def logs_legacy_eos_proxy(
    log_lfn: str,
    range: Annotated[Optional[str], Header()] = None,
):
    """
    Get log files from logSE
    """

    return await _logs_eos_proxy(LOG_SE_BASE, log_lfn, range)


@router.head(
    "/logs/get-legacy/{log_lfn:path}",
    summary="Get log files from logSE",
)
async def head_logs_legacy_eos_proxy(
    log_lfn: str,
    range: Annotated[Optional[str], Header()] = None,
):
    """
    Get log files from logSE
    """

    return await _logs_eos_proxy(LOG_SE_BASE, log_lfn, range, head=True)


@router.get(
    "/logs/get/{log_lfn:path}",
    summary="Get log database from logArchive",
)
async def log_eos_proxy(
    log_lfn: str,
    range: Annotated[Optional[str], Header()] = None,
):
    """
    Get log database from logArchive
    """

    return await _logs_eos_proxy(LOG_DB_BASE, log_lfn, range)


@router.head(
    "/logs/get/{log_lfn:path}",
    summary="Get log database from logArchive",
)
async def head_log_eos_proxy(
    log_lfn: str,
    range: Annotated[Optional[str], Header()] = None,
):
    """
    Get log database from logArchive
    """

    return await _logs_eos_proxy(LOG_DB_BASE, log_lfn, range, head=True)


@router.get(
    "/browse_artifact/{project_id}/{ci_run}/{analysis}/{job_id}/{job_name}/{filename:path}",
    summary="EOS proxy for JSROOT browser ",
)
async def eos_proxy_jsroot(
    project_id: int,
    ci_run: int,
    analysis: str,
    job_id: int,
    job_name: str,
    filename: str,
    authz: str,
    range: Annotated[Optional[str], Header()] = None,
):
    """
    EOS proxy for JSROOT browser
    """

    query = sqlalchemy.select(db_models.ProjectWebhook.eos_artifact_path)
    query = query.where(db_models.ProjectWebhook.project_id == project_id)
    async with SessionLocal() as db:
        try:
            _ = (await db.execute(query)).scalar_one()
        except sqlalchemy.exc.NoResultFound as exc:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Job not found"
            ) from exc

    return await inner_eos_proxy_jsroot(
        ci_run, analysis, job_id, job_name, filename, authz, range
    )


@router.get(
    "/{project_id}/{file_path:path}",
    summary="Get the options for retrying a job",
)
async def eos_proxy(
    project_id: int,
    file_path: str,
    authz: str,
    range: Annotated[Optional[str], Header()] = None,
):
    """
    Get the options for retrying a job
    """

    query = sqlalchemy.select(db_models.ProjectWebhook.eos_artifact_path)
    query = query.where(db_models.ProjectWebhook.project_id == project_id)
    async with SessionLocal() as db:
        try:
            eos_artifact_path = (await db.execute(query)).scalar_one()
        except sqlalchemy.exc.NoResultFound as exc:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Job not found"
            ) from exc

    return await inner_eos_proxy(eos_artifact_path, file_path, authz, range)


async def inner_eos_proxy(
    prefix: str, file_path: str, authz: str, range: str | None, verb: str = "GET"
):
    """
    Inner function
    """
    file_path, error = check_lfn(file_path)
    if error:
        return error
    full_file_path = f"{prefix}/{file_path}"

    client = get_httpx_client()
    headers = None
    if range is not None:
        headers = {"Range": range}
    url = f"https://eoslhcb.cern.ch/{urllib.parse.quote(full_file_path)}"

    req = client.build_request(
        verb, url, headers=headers, params={"xrd.wantprot": "unix", "authz": authz}
    )
    resp = await client.send(req, stream=True, follow_redirects=True)
    headers = {k: resp.headers[k] for k in PROXIED_HEADERS if k in resp.headers}
    headers["Access-Control-Expose-Headers"] = (
        "Accept-Ranges, Content-Type, Content-Length, Content-Range, Content-Encoding"
    )
    headers["Accept-Ranges"] = "bytes"

    logger.info(f"{file_path=} {authz=} {range=} {resp.status_code=} {resp.headers=}")

    return StreamingResponse(
        resp.aiter_bytes(),
        status_code=resp.status_code,
        headers=headers,
        media_type=resp.headers["content-type"],
        background=BackgroundTask(resp.aclose),
    )


async def inner_eos_proxy_jsroot(
    ci_run: int,
    analysis: str,
    job_id: int,
    job_name: str,
    filename: str,
    authz: str,
    range: str | None,
    verb: str = "GET",
):
    """
    Inner function
    """

    full_file_path = os.path.normpath(
        f"/eos/lhcb/wg/dpa/wp2/ci/{ci_run}/{analysis}/{job_id}/{_make_eos_safe_name(job_name)}/{filename}"
    )

    if not full_file_path.startswith("/eos/lhcb/wg/dpa/wp2/ci"):
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={"message": "Invalid file path"},
        )

    client = get_httpx_client()
    headers = None
    if range is not None:
        headers = {"Range": range}
    url = f"https://eoslhcb.cern.ch/{urllib.parse.quote(full_file_path)}"

    req = client.build_request(
        verb, url, headers=headers, params={"xrd.wantprot": "unix", "authz": authz}
    )
    resp = await client.send(req, stream=True, follow_redirects=True)
    headers = {
        k: resp.headers[k]
        for k in PROXIED_HEADERS
        if k in resp.headers
        and k not in ["server", "date"]  # avoid repeated headers...
    }
    headers["Access-Control-Expose-Headers"] = (
        "Accept-Ranges, Content-Type, Content-Length, Content-Range, Content-Encoding"
    )
    headers["Accept-Ranges"] = "bytes"

    logger.info(
        f"{full_file_path=} {authz=} {range=} {resp.status_code=} {resp.headers=}"
    )

    return StreamingResponse(
        resp.aiter_bytes(),
        status_code=resp.status_code,
        headers=headers,
        media_type=resp.headers["content-type"],
        background=BackgroundTask(resp.aclose),
    )
