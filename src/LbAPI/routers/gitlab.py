###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import fnmatch
import logging
import os
import re
import time
from datetime import datetime, timezone
from typing import Optional

from authlib.integrations.starlette_client import OAuth
from fastapi import APIRouter, Depends, HTTPException, Request, status
from pydantic import BaseModel, validator
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from LbAPI.auth import dirac_username_mapping
from LbAPI.settings import (
    settings,
)

from ..auth import User, get_current_user, oidc_scheme
from ..database import extra_func, get_db
from ..database.models import GitLabToken, RegisteredRepository
from ..responses import credentials_exception
from ..utils import Token, generate_eos_token, is_cern_ip, parse_jwt

logger = logging.getLogger(__name__)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

router = APIRouter(
    prefix="/gitlab",
    tags=["gitlab"],
    # dependencies=[Depends(get_current_user)],
)

oauth = OAuth()
oauth.register(
    name="cern_gitlab",
    server_metadata_url=str(settings.cern_gitlab_well_known),
)

RE_ALLOWED_EOS_WG = re.compile(r"^/eos/lhcb/wg/[^/]+/[^/]+/(?:.+/)?$")
RE_ALLOWED_EOS_USER = re.compile(r"^/eos/lhcb/user/[a-z]/([^/]+)/[^/]+/(?:.+/)?$")
ALLOWED_TOKEN_READ_LOCATIONS = (
    "/eos/lhcb/grid/prod/lhcb",
    "/eos/lhcb/cern-swtest",
)


async def _select_repo(
    db: AsyncSession, project_id: int, user_login: str
) -> RegisteredRepositoryResponse:
    query = select(RegisteredRepository)
    query = query.filter(RegisteredRepository.project_id == project_id)
    query = query.filter(RegisteredRepository.user_login == user_login)
    query = query.filter(
        RegisteredRepository.expires.is_(None)
        | (RegisteredRepository.expires > extra_func.utcnow())
    )
    query = query.filter(~RegisteredRepository.revoked)
    async with db.begin():
        if result := (await db.execute(query)).first():
            return RegisteredRepositoryResponse.model_validate(result[0])
    return None


async def _gitlab_jwt_v2(request: Request, authorization: str = Depends(oidc_scheme)):
    logger.debug(
        "Authentication request for _gitlab_jwt_v2 from %s", request.client.host
    )
    if not is_cern_ip(request.client.host):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Only CERN IPs are allowed to access this endpoint",
        )
    try:
        token = await parse_jwt(
            authorization, "https://lbap.app.cern.ch", oauth.cern_gitlab
        )
    except Exception as e:
        logger.exception(
            "Failed to validate credentials %s: %r", time.time(), authorization
        )
        raise credentials_exception from e
    return dict(token)


async def _token_repo_info(
    token=Depends(_gitlab_jwt_v2), db: AsyncSession = Depends(get_db)
):
    repo_info = await _select_repo(db, int(token["project_id"]), token["user_login"])
    if repo_info is None:
        logger.warning("Found no registered repository for %r", token)
        url = "https://lhcb-analysis-productions.web.cern.ch/settings/"
        error_message = (
            f"Successfully validated token for {token['user_login']} however it is not registered to LbAPI.\n"
            "\n"
            "Please register your repository with this details:\n"
            f"    * Project ID: {token['project_id']}\n"
            f"    * Project Path: {token['project_path']}\n"
            "\n"
            "This can be done at:\n"
            f"{url}/?project_id={token['project_id']}&project_path={token['project_path']}\n"
        )
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=error_message,
            headers={"WWW-Authenticate": "Bearer"},
        )

    logger.info("Got GitLab credentials request from %r", token)
    if not fnmatch.fnmatch(token["sub"], repo_info.subject_filter):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"Subject ({token['sub']!r}) does not match allowed pattern ({repo_info.subject_filter!r})",
        )
    if repo_info.only_protected and token["ref_protected"] != "true":
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Reference is not protected!",
        )
    return repo_info


class EOSToken(BaseModel):
    path: str
    allow_write: bool
    token: str


class CICredentials(BaseModel):
    lbapi_token: str
    eos_tokens: list[EOSToken] = []


@router.get(
    "/credentials/",
    response_model=CICredentials,
    summary="Used by apd to get credentials for gitlab",
)
async def get_credentials(
    token=Depends(_gitlab_jwt_v2),
    repo_info=Depends(_token_repo_info),
    db: AsyncSession = Depends(get_db),
):
    if token["user_login"] not in dirac_username_mapping():
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail=f"User {token['user_login']} is not registered in LHCbDIRAC",
        )

    raw_token, lbapi_token = Token.generate("gitlab")
    expires = datetime.fromtimestamp(
        min(token["exp"], time.time() + repo_info.max_duration_seconds),
        tz=timezone.utc,
    )
    db_item = GitLabToken(
        registered_repo_id=repo_info.id,
        successful=False,
        gitlab_token=token,
        eos_token=[],
        apd_token=lbapi_token.hashed_token,
        expires=expires,
    )
    result = CICredentials(lbapi_token=raw_token)
    try:
        for eos_spec in repo_info.eos_specs:
            eos_token, eos_token_info = await generate_eos_token(
                directory=eos_spec.path,
                allow_write=eos_spec.allow_write,
                owner=repo_info.user_login,
                expires=expires,
            )
            db_item.eos_token.append(eos_token_info)
            result.eos_tokens.append(
                EOSToken(
                    path=eos_spec.path,
                    allow_write=eos_spec.allow_write,
                    token=eos_token,
                )
            )
    finally:
        async with db.begin():
            db.add(db_item)
    return result


class EOSTokenSpec(BaseModel):
    """Defines what EOS tokens should be given to GitLab jobs"""

    path: str
    allow_write: bool

    @validator("path")
    def path_must_be_normalized(cls, v):  # pylint: disable=no-self-argument
        if v != f"{os.path.normpath(v)}/":
            raise ValueError("Path must be normalized")
        if v[0] != "/":
            raise ValueError("Path must start with a '/'")
        if v[-1] != "/":
            raise ValueError("Path must end with a '/'")
        return v


class RegisterRepositoryRequest(BaseModel):
    project_id: int
    project_path: str
    eos_specs: list[EOSTokenSpec]
    subject_filter: str = "*"
    only_protected: bool = False
    max_duration_seconds: int = 60 * 60 * 24
    expires: Optional[datetime] = None

    class Config:
        schema_extra = {
            "example": {
                "project_id": 145826,
                "project_path": "cburr/test-simulation-requests",
                "eos_specs": [
                    {
                        "path": "/eos/lhcb/grid/prod/lhcb/",
                        "allow_write": False,
                    },
                    {
                        "path": "/eos/lhcb/wg/MYWG/MYANALYSIS/",
                        "allow_write": True,
                    },
                ],
            }
        }


class RegisteredRepositoryResponse(BaseModel, from_attributes=True):
    id: int
    project_id: int
    project_path: str
    user_login: str
    subject_filter: str
    only_protected: bool
    max_duration_seconds: int
    eos_specs: list[EOSTokenSpec]
    expires: Optional[datetime]
    revoked: bool


@router.post(
    "/register/",
    response_model=int,
    status_code=201,
    summary="Register a repository for use with GitLab CI",
)
async def register_repo(
    info: RegisterRepositoryRequest,
    user: User = Depends(get_current_user),
    db: AsyncSession = Depends(get_db),
):
    repo_info = await _select_repo(db, info.project_id, user.username)
    if repo_info is not None:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=f"Already registered with id={repo_info.id}",
        )

    for eos_spec in info.eos_specs:
        logger.debug("Considering granting token for %r", eos_spec)
        if not eos_spec.path.endswith("/"):
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"EOS paths must end with a trailing slash: {eos_spec.path}",
            )
        if eos_spec.path.startswith(ALLOWED_TOKEN_READ_LOCATIONS):
            if eos_spec.allow_write:
                raise HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=f"Cannot register {eos_spec.path} for writing",
                )
        elif RE_ALLOWED_EOS_WG.match(eos_spec.path):
            pass
        elif info.project_path.startswith(user.username):
            match = RE_ALLOWED_EOS_USER.fullmatch(eos_spec.path)
            if not match or match.groups(0)[0] != user.username:
                raise HTTPException(
                    status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                    detail=(
                        f"Can't register {eos_spec.path}. Only "
                        f"/eos/lhcb/user/{user.username}/ is supported. "
                        "Please contact lhcb-dpa-wp2-managers@cern.ch for support."
                    ),
                )
        else:
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=(
                    f"Can't register {eos_spec.path}. Only "
                    "/eos/lhcb/grid/prod/lhcb and /eos/lhcb/wg/ are supported. "
                    "Please contact lhcb-dpa-wp2-managers@cern.ch for support."
                ),
            )

    repo = RegisteredRepository(
        user_login=user.username,
        project_id=info.project_id,
        project_path=info.project_path,
        subject_filter=info.subject_filter,
        only_protected=info.only_protected,
        max_duration_seconds=info.max_duration_seconds,
        eos_specs=[x.dict() for x in info.eos_specs],
        expires=info.expires,
    )
    async with db.begin():
        db.add(repo)
        await db.flush()
        await db.refresh(repo)
        return repo.id


@router.get(
    "/list/",
    response_model=list[RegisteredRepositoryResponse],
    summary="List registered repositories for the current user",
)
async def list_repos(
    user: User = Depends(get_current_user), db: AsyncSession = Depends(get_db)
):
    query = select(RegisteredRepository)
    query = query.filter(RegisteredRepository.user_login == user.username)
    query = query.filter(
        RegisteredRepository.expires.is_(None)
        | (RegisteredRepository.expires > extra_func.utcnow())
    )
    query = query.filter(~RegisteredRepository.revoked)
    async with db.begin():
        return [
            RegisteredRepositoryResponse.model_validate(result)
            async for result, in await db.stream(query)
        ]


@router.post(
    "/revoke/{id}",
    dependencies=[Depends(get_current_user)],
    summary="Revoke the access of a registered repository",
)
async def revoke_repo(
    id: int, user: User = Depends(get_current_user), db: AsyncSession = Depends(get_db)
):
    query = update(RegisteredRepository)
    query = query.filter(RegisteredRepository.id == id)
    query = query.filter(RegisteredRepository.user_login == user.username)
    query = query.values(revoked=True)

    async with db.begin():
        result = await db.execute(query)
        if result.rowcount == 0:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail=f"ID {id} not found"
            )
        # Revoke any existing GitLab tokens associated with this RegisteredRepository
        query = update(GitLabToken)
        query = query.filter(GitLabToken.registered_repo_id == id)
        query = query.values(revoked=True)
        result = await db.execute(query)
        logger.info("Revoked %s tokens for %s", result.rowcount, id)
    return {"detail": f"ID {id} has been revoked"}
