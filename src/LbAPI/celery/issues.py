###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import logging
import re
from subprocess import check_output
from tempfile import TemporaryDirectory

import git
from cachetools import TTLCache, cached
from sqlalchemy import select

from LbAPI.database import SessionLocal
from LbAPI.database.webhooks import ProjectWebhook
from LbAPI.dirac import get_req_state
from LbAPI.enums import WebhookType
from LbAPI.settings import (
    gitlab,
)

logger = logging.getLogger(__name__)


async def check_issue():
    async with SessionLocal() as session:
        query = select(ProjectWebhook.project_id, ProjectWebhook.type)
        for project_id, project_type in (await session.execute(query)).all():
            check_issue_for_project(project_id, project_type)


@cached(cache=TTLCache(maxsize=1, ttl=30 * 60))
def get_simdq_available_request_ids() -> set[str]:
    # find . -name '*.yml' -exec yq .RequestID {} \; | sort | uniq
    # sh-5.1$ git clone https://gitlab.cern.ch/lhcb-simulation/simdqdata.git
    with TemporaryDirectory() as tmpdir:
        logger.debug("Cloning simdqdata")
        git.Repo.clone_from(
            "https://gitlab.cern.ch/lhcb-simulation/simdqdata.git", tmpdir
        )
        logger.debug("Searching for RequestIDs in simdqdata")
        text = check_output(
            ["find", tmpdir, "-name", "*.yml", "-exec", "yq", ".RequestID", "{}", ";"],
            text=True,
        )

    request_ids = set(text.splitlines())
    logger.debug("Found %s RequestIDs", len(request_ids))
    return request_ids


def check_issue_for_project(project_id, project_type):
    logger.debug("Closing issues for %s", project_id)
    project = gitlab.api.projects.get(project_id)
    for issue in project.issues.list(state="opened", iterator=True):
        logger.debug("Considering %s", issue.iid)
        # Reload the issue to make sure we have the latest view of the issue
        issue = project.issues.get(issue.iid)

        match = re.search(r"```json\n(.+)\n```", issue.description)
        if match is None:
            logger.debug("No match, skipping issue %s", issue.iid)
            continue
        issue_dict = json.loads(match.groups()[0])

        req_states = get_req_state(issue_dict["request_ids"], return_states=True)
        logger.debug("Request states for %s: %s", issue.iid, req_states)

        def requests_list(request_states, state_set: set):
            return [x in state_set for x in request_states.values()]

        resolved_set = {"Done", "Rejected", "Cancelled"}
        resolved_requests = requests_list(req_states, resolved_set)

        active_set = {"Active"}
        active_requests = requests_list(req_states, active_set)

        ppgok_set = {"PPG OK"}
        ppgok_requests = requests_list(req_states, ppgok_set)

        accepted_set = {"Accepted"}
        accepted_requests = requests_list(req_states, accepted_set)

        def set_production_progress_label(labels, substate):
            return [lab for lab in labels if not lab.startswith("Production")] + [
                f"Production::{substate}"
            ]

        if all(resolved_requests):
            issue.labels = set_production_progress_label(issue.labels, "done")
            logger.debug("Closing issue %s", issue.iid)
            issue.state_event = "close"
            issue.save()
        elif any(active_requests):
            # append "Production::active" label
            issue.labels = set_production_progress_label(issue.labels, "active")
            issue.save()

        elif any(accepted_requests):
            # append "Production::accepted" label
            issue.labels = set_production_progress_label(issue.labels, "accepted")
            issue.save()

        elif all(ppgok_requests):
            # append "Production::PPG OK" label
            issue.labels = set_production_progress_label(issue.labels, "PPG OK")
            issue.save()

        # Sim DQ tagging
        #! Temporary until #12 gets properly implemented
        if project_type != WebhookType.MC_REQUEST:
            continue

        available_ids = get_simdq_available_request_ids()
        simdq_list = [str(rid) in available_ids for rid in issue_dict["request_ids"]]

        def set_simdq_label(labels, substate):
            return [lab for lab in labels if not lab.startswith("SimDQ")] + [
                f"SimDQ::{substate}"
            ]

        if all(simdq_list):
            issue.labels = set_simdq_label(issue.labels, "Available")
            issue.save()
        elif any(simdq_list):
            issue.labels = set_simdq_label(issue.labels, "Partially Available")
            issue.save()
