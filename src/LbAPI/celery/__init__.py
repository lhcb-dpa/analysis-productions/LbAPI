###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import annotations

import asyncio
import json
import logging.config
import os
from asyncio import Semaphore
from datetime import timedelta

import sqlalchemy
from celery import Celery
from celery.signals import setup_logging, worker_process_init

from LbAPI.ci import actions
from LbAPI.ci.queries import mark_all_ci_runs_failed, mark_ci_run_ready_to_submit
from LbAPI.ci.webhook import set_commit_status
from LbAPI.config import LOG_DB_BASE, LOG_SE_BASE
from LbAPI.database import SessionLocal, create_engine
from LbAPI.database.webhooks import CacheEntries, Pipeline, ProjectWebhook
from LbAPI.enums import WebhookType
from LbAPI.settings import (
    settings,
)
from LbAPI.utils import (
    ExceptionForGitlab,
    eos_get_files_to_remove,
    eos_rm,
    retryable_on_error,
    safe_eos_ls,
)

from . import consistency, dirac_prod_output, issues


@setup_logging.connect
def on_setup_logging(**_kwargs):
    if "LBAP_LOGGING_CONFIG_INI" in os.environ:
        logging.config.fileConfig(os.environ["LBAP_LOGGING_CONFIG_INI"])


logger = logging.getLogger(__name__)

app = Celery(
    "LbAPI.celery",
    broker=str(settings.lbapi_broker),
    backend=f"db+{settings.lbapi_backend}" if settings.lbapi_backend else "rpc://",
)
app.conf.update(
    task_serializer="json",
    accept_content=["json"],  # Ignore other content
    result_serializer="json",
    timezone="Europe/Zurich",
    enable_utc=True,
    task_ignore_result=not bool(settings.lbapi_backend),
    worker_send_task_events=True,
    broker_connection_retry_on_startup=False,
    result_extended=True,
    result_expires=timedelta(days=90),
    result_backend_always_retry=True,
    task_track_started=True,
)
# app.conf.worker_max_memory_per_child = 200000  # 200 MB


async def get_project_type(
    project_id: int, commit_sha: str, mr_id: int
) -> tuple[WebhookType, int]:
    print(project_id, commit_sha, mr_id)
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(ProjectWebhook.type, Pipeline.id)
        query = query.where(Pipeline.project_id == ProjectWebhook.project_id)
        query = query.where(ProjectWebhook.project_id == project_id)
        query = query.where(Pipeline.commit_sha == commit_sha)
        query = query.where(Pipeline.mr_id == mr_id)
        return (await session.execute(query)).one()


async def get_pipeline_author(pipeline_id: int) -> str:
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(Pipeline.author)
        query = query.where(Pipeline.id == pipeline_id)
        return (await session.execute(query)).scalar_one()


async def get_eos_artifact_paths():
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(ProjectWebhook.eos_artifact_path)
        paths = [x for x, in (await session.execute(query)).all()]
    return paths


@worker_process_init.connect
def setup_globals(*args, **kwargs):
    global loop
    logger.debug("Global setup started")

    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_engine())

    logger.info("Global setup finished")


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Retry jobs that fail or are killed
    sender.add_periodic_task(5 * 60, check_dirac_wms_status.s(), expires=2 * 60)
    # Retry jobs which stop producing log output
    sender.add_periodic_task(60 * 60, check_last_log_updates.s(), expires=10 * 60)
    # Checks and closes issues when productions are done
    sender.add_periodic_task(60 * 60, check_issue.s(), expires=10 * 60)
    # Cleans EOS artifacts once a day
    sender.add_periodic_task(60 * 60 * 24, clean_eos_artifacts.s(), expires=60 * 60)
    # Cache the logs overview once an hour
    sender.add_periodic_task(60 * 60, cache_available_logs.s(), expires=15 * 60)
    # Check and resubmit failed webhooks every thirty minutes
    sender.add_periodic_task(30 * 60, check_webhooks.s(), expires=15 * 60)


@app.task
def cache_available_logs():
    """
    List all productions and subproductions
    """
    loop.run_until_complete(_cache_available_logs())


async def _cache_available_logs():
    tree: dict[str, dict[str, set | list]] = {}

    for dir in ("/lhcb/LHCb", "/lhcb/MC", "/lhcb/validation", "/lhcb/Dev"):
        subtree: dict[str, set | list]
        path = f"{LOG_DB_BASE}{dir}"
        subtree = tree[dir] = {
            subdir: set(await safe_eos_ls(f"{path}/{subdir}/LOG"))
            for subdir in await safe_eos_ls(path)
        }

        path = f"{LOG_SE_BASE}/{dir}"
        for subdir in await safe_eos_ls(path):
            subtree.setdefault(subdir, set()).update(
                set(await safe_eos_ls(f"{path}/{subdir}/LOG"))
            )

        for subdir in subtree:
            subtree[subdir] = sorted(subtree[subdir])  # cast to sorted list

    async with SessionLocal.begin() as session:
        query = sqlalchemy.delete(CacheEntries)
        query = query.where(CacheEntries.key == "available_logs")
        await session.execute(query)

        query = sqlalchemy.insert(CacheEntries)
        await session.execute(
            query, [{"key": "available_logs", "value": json.dumps(tree)}]
        )


@app.task
@retryable_on_error
def start_pipeline(project_id: int, commit_sha: str, mr_id: int):
    loop.run_until_complete(_start_pipeline(project_id, commit_sha, mr_id))


async def _start_pipeline(project_id: int, commit_sha: str, mr_id: int):
    async with SessionLocal.begin() as session:
        query = sqlalchemy.select(ProjectWebhook.type)
        query = query.where(ProjectWebhook.project_id == project_id)
        project_type = (await session.execute(query)).scalar_one()

    status = "failed"
    pipeline_id = None
    try:
        logger.debug("Starting pipeline for %s %s %s", project_id, commit_sha, mr_id)
        project_type, pipeline_id, ci_run_names = await actions.start_pipeline(
            project_id, commit_sha, mr_id
        )
        logger.debug(
            "Pipeline %s %s %s created as %s",
            *[project_id, commit_sha, mr_id, pipeline_id],
        )
        for name in ci_run_names:
            start_cirun.delay(pipeline_id, name)

        logger.debug("Finished starting pipeline %s", pipeline_id)

        if len(ci_run_names) != 0:
            status = "running"

    finally:
        set_commit_status(
            project_type,
            project_id,
            commit_sha,
            pipeline_id,
            status,
        )


@app.task
def start_cirun(pipeline_id: int, name: str):
    logger.debug("Starting CI run %s:%s", pipeline_id, name)
    loop.run_until_complete(_start_cirun(pipeline_id, name))


async def _start_cirun(pipeline_id: int, name: str):
    jobs = await actions.start_cirun(pipeline_id, name)
    for job_id in jobs or []:
        logger.debug("Queuing job %s for %s:%s", job_id, pipeline_id, name)
        start_job.delay(job_id)


@app.task
def start_job(job_id: int):
    logger.debug("Starting job_id=%s", job_id)
    loop.run_until_complete(_start_job(job_id))


async def _start_job(job_id: int):
    submitted_id = await actions.start_job(job_id)
    logger.debug("Submitted job_id=%s as %s", job_id, submitted_id)


@app.task(bind=True)
def pre_submission_tasks(
    self,
    project_id: int,
    commit_sha: str,
    mr_id: int,
    submitting_user: str | None,
    dry_run: bool,
):
    project_type, pipeline_id = loop.run_until_complete(
        get_project_type(project_id, commit_sha, mr_id)
    )
    if submitting_user is None:
        if not dry_run:
            raise ValueError("submitting_user must be provided if not in dry-run mode")
        submitting_user = loop.run_until_complete(get_pipeline_author(pipeline_id))

    logger.debug(
        "Running pre-submission tasks for %s %s !%s as %s (dry-run: %s)",
        project_id,
        commit_sha,
        mr_id,
        submitting_user,
        dry_run,
    )
    try:
        ready = loop.run_until_complete(
            actions.pre_submission_tasks(
                project_id, commit_sha, mr_id, submitting_user, dry_run=dry_run
            )
        )
    except Exception as e:
        if dry_run:
            message = f"Dry-run pre-submission failed: {e!r}"
            loop.run_until_complete(mark_all_ci_runs_failed(pipeline_id, message))
            set_commit_status(
                project_type, project_id, commit_sha, pipeline_id, "failed"
            )
        raise
    if not ready:
        if dry_run:
            raise NotImplementedError("This should not happen in dry-run mode!")
        logger.debug(
            "Pre-submission is not finished for %s %s !%s as %s",
            project_id,
            commit_sha,
            mr_id,
            submitting_user,
        )
        raise self.retry(countdown=15 * 60)

    # Now do the actual submission
    task = submit_productions.delay(
        project_id, commit_sha, mr_id, submitting_user, dry_run=dry_run
    )
    logger.debug(
        "Pre-submission finished for  %s %s !%s as %s (dry-run: %s). Actual submission task is %s",
        project_id,
        commit_sha,
        mr_id,
        submitting_user,
        dry_run,
        task,
    )


@app.task
def submit_productions(
    project_id: int, commit_sha: str, mr_id: int, submitting_user: str, dry_run: bool
):
    project_type, pipeline_id = loop.run_until_complete(
        get_project_type(project_id, commit_sha, mr_id)
    )

    logger.debug(
        "Submitting productions for %s %s !%s as %s (dry-run: %s)",
        project_id,
        commit_sha,
        mr_id,
        submitting_user,
        dry_run,
    )
    try:
        loop.run_until_complete(
            actions.submit_productions(
                project_id, commit_sha, mr_id, submitting_user, dry_run=dry_run
            )
        )
    except Exception as e:
        if dry_run:
            message = f"Dry-run submission failed: {e}"
            loop.run_until_complete(mark_all_ci_runs_failed(pipeline_id, message))
            set_commit_status(
                project_type, project_id, commit_sha, pipeline_id, "failed"
            )
            raise e
        raise ExceptionForGitlab(
            "Failed to submit production to DIRAC."
            "This will probably need to be looked at by @lhcb-dpa/analysis-productions/experts\n"
        ) from e
    if dry_run:
        logger.debug(
            "Dry-run submission finished for %s %s !%s as %s",
            project_id,
            commit_sha,
            mr_id,
            submitting_user,
        )
        loop.run_until_complete(mark_ci_run_ready_to_submit(pipeline_id))
        set_commit_status(project_type, project_id, commit_sha, pipeline_id, "success")
    else:
        task = post_submission_tasks.delay(project_id, commit_sha, mr_id)
        logger.debug(
            "Submission finished for %s %s !%s as %s. Post-submission task is %s",
            project_id,
            commit_sha,
            mr_id,
            submitting_user,
            task,
        )


@app.task
@retryable_on_error
def post_submission_tasks(project_id: int, commit_sha: str, mr_id: int):
    try:
        logger.debug(
            "Opening GitLab issue for %s %s !%s", project_id, commit_sha, mr_id
        )
        loop.run_until_complete(
            actions.create_gitlab_issue(project_id, commit_sha, mr_id)
        )
    except Exception as e:
        raise ExceptionForGitlab(
            "Post-submission task got stuck while creating task. "
            "This will probably need to be looked at by @lhcb-dpa/analysis-productions/experts\n"
        ) from e

    try:
        logger.debug(
            "Running post-submission tasks for %s %s !%s",
            project_id,
            commit_sha,
            mr_id,
        )
        loop.run_until_complete(
            actions.post_submission_tasks(project_id, commit_sha, mr_id)
        )
    except Exception as e:
        raise ExceptionForGitlab(
            "Post-submission task got stuck while launching this production in DIRAC. "
            "This will probably need to be looked at by @lhcb-dpa/analysis-productions/experts\n"
        ) from e

    try:
        logger.debug("Cleaning repository for %s %s !%s", project_id, commit_sha, mr_id)
        loop.run_until_complete(actions.clean_repo(project_id, commit_sha, mr_id))
    except Exception as e:
        raise ExceptionForGitlab(
            "Post-submission task got stuck while cleaning repository. "
            "This will probably need to be retried by @lhcb-dpa/analysis-productions/experts\n"
        ) from e

    logger.debug("Submission successful for %s %s !%s", project_id, commit_sha, mr_id)


@app.task(bind=True)
def launch_productions(
    self, project_type: WebhookType, request_ids: list[int], required_paths: list[str]
):
    """Launch productions for a given request.

    This task is retried if the required paths do not exist, e.g. to be able to
    poll for data packages to be deployed. The production launching is handled
    by a separate, per-production, celery task which is spawned by this task.
    """
    for path in required_paths:
        if not os.path.exists(path):
            logger.debug(
                "Required path %s does not exist for %s, retrying in 1 hour",
                path,
                request_ids,
            )
            raise self.retry(countdown=60 * 60)
    for request_id in request_ids:
        task = launch_production.delay(project_type, request_id)
        logger.debug("Launched production task %s for request %s", task, request_id)


@app.task
def launch_production(
    project_type: WebhookType, request_id: int, *, append_name: str | None = None
):
    loop.run_until_complete(
        actions.launch_production(project_type, request_id, append_name=append_name)
    )


@app.task
def check_dirac_wms_status():
    """Ensure that the status in the DIRAC WMS is consistent.

    Jobs which aren't running in DIRAC but are in the database are marked as
    cancelled and potentially retried.
    """
    loop.run_until_complete(consistency.check_dirac_wms_status())


@app.task
def check_webhooks():
    """Ensure that recently handled webhooks were properly handled."""

    loop.run_until_complete(consistency.check_webhooks())


@app.task
def check_issue():
    """Closes issues when they're finished"""
    loop.run_until_complete(issues.check_issue())


sem = Semaphore(10)


async def limited_eos_rm(path):
    async with sem:
        await eos_rm(path)


@app.task
def clean_eos_artifacts():
    """Cleans EOS artifacts"""
    artifact_paths = loop.run_until_complete(get_eos_artifact_paths())

    for artifact_path in artifact_paths:
        # TODO Due to /test-repo not having a ci path atm.
        if artifact_path is None:
            continue

        # Files which are more than 1GB are kept for 1 week
        paths_week = loop.run_until_complete(
            eos_get_files_to_remove(artifact_path, 7, 1024)
        )

        # Files which are more than 100MB are kept for 1 month
        paths_month = loop.run_until_complete(
            eos_get_files_to_remove(artifact_path, 30, 100)
        )

        # Other files are kept for 6 months
        paths_rest = loop.run_until_complete(
            eos_get_files_to_remove(artifact_path, 180, 0)
        )

        paths = set(paths_week + paths_month + paths_rest)
        loop.run_until_complete(
            asyncio.gather(*(limited_eos_rm(path) for path in paths))
        )


@app.task
def check_last_log_updates():
    """Ensure that running jobs are still active.

    Jobs which haven't updated their log in a while are marked as failed.
    """
    loop.run_until_complete(consistency.check_last_log_updates())


@app.task
def fetch_all_summary_XML_task(wg, analysis, version, name):
    # Disable for now
    return
    loop.run_until_complete(
        dirac_prod_output.fetch_all_summary_XML_task(wg, analysis, version, name)
    )


@app.task()
def update_discussion(pipeline_id: int):
    logger.debug("Updating discussion")
    loop.run_until_complete(actions.update_discussion(pipeline_id))
