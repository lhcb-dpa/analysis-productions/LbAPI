###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
import datetime
import logging
import os
import tempfile
import xml.etree.ElementTree as ET
from collections import defaultdict, deque
from pathlib import Path
from tempfile import TemporaryDirectory

import botocore
import requests
from dateutil.tz import tzutc
from DIRAC.Core.Utilities.ReturnValues import returnValueOrRaise
from LHCbDIRAC.ProductionManagementSystem.Client.AnalysisProductionsClient import (
    AnalysisProductionsClient,
)

from LbAPI.settings import (
    settings,
)

from ..dirac import DownloadedProxyFile
from ..external.XMLSummaryBase import summary
from ..utils import get_krb5_creds, run

logger = logging.getLogger(__name__)


def summaryXML_filename_from_LFN(
    output_LFN,
    appName,
    withDir=True,
    step_index=None,
):
    _, filename = os.path.split(output_LFN)

    if filename.startswith("PFN:"):
        filename = filename.removeprefix("PFN:")

    sequencer, _ = filename.split(".", 1)
    trfID, batch_jobID, step_idx = sequencer.split("_")
    filename = f"summary{appName}_{trfID}_{batch_jobID}_{step_index or step_idx}.xml"

    if withDir:
        filename = f"{batch_jobID}/{filename}"

    return filename


def eos_logSE_path_from_LFN(output_LFN, configPath=None):
    maybe_full_path, filename = os.path.split(output_LFN)

    # sometimes only a basename (PFN: prefix is a giveaway)
    # is provided... as long as config path (e.g. /lhcb/LHCb/Collision22)
    # is known we can still figure out where the log files are
    if filename.startswith("PFN:"):
        filename = filename.removeprefix("PFN:")
    else:
        if not configPath:
            # descern from output_LFN.. hoping it is still an LFN
            configPath = maybe_full_path.rsplit("/", 3)[0]

    sequencer, _ = filename.split(".", 1)
    trfID, batch_jobID, step_idx = sequencer.split("_")

    batch_id = batch_jobID[:4]

    logSE_path = "/".join(
        [
            "/eos/lhcb/grid/prod/lhcb/logSE" + configPath,
            "LOG",
            trfID,
            batch_id,
            f"{batch_jobID}.zip",
        ]
    )

    return logSE_path


async def download_summary_xml(
    output_LFN: str,
    appName: str,
    output_dir: Path,
    eoslhcb: str = "root://eoslhcb.cern.ch",
    extra_env: dict = None,
    step_index: int = None,
    configPath=None,
):
    output_filename = summaryXML_filename_from_LFN(output_LFN, appName, withDir=False)
    cmd = [
        "xrdcp",
        f"--zip={summaryXML_filename_from_LFN(output_LFN, appName, step_index=step_index)}",
        f"{eoslhcb}/{eos_logSE_path_from_LFN(output_LFN, configPath=configPath)}",
        f"{output_dir / output_filename}",
    ]
    returncode, stdout, stderr = await run(*cmd, extra_env=extra_env)
    assert returncode == 0, (returncode, stdout, stderr)
    assert stderr == ""

    return output_dir / output_filename


async def get_ancestors(output_LFNs, dirac_nick, depth=2):
    with DownloadedProxyFile(
        dirac_nick, "lhcb_user"
    ) as proxy_file, TemporaryDirectory() as _tmpdir:
        input_files = Path(_tmpdir) / "input_lfns.txt"
        input_files.write_text("\n".join(output_LFNs) + "\n")

        cmd = settings.lb_dirac + [
            "dirac-bookkeeping-get-file-ancestors",
            f"--File={input_files}",
            f"--Depth={depth}",
        ]
        returncode, stdout, stderr = await run(
            *cmd,
            extra_env={"X509_USER_PROXY": proxy_file.name},
        )
        assert returncode == 0, (returncode, stdout, stderr)
        assert "completed in " in stderr
        assert "Successful" in stdout

    ancestors = defaultdict(list)

    current_key = None
    for line in stdout.splitlines():
        line = line.strip()
        if line.endswith(" :") and not line.startswith("Successful :"):
            current_key = line.removesuffix(" :").strip()
            if current_key not in output_LFNs:
                current_key = None
            continue

        if current_key and " : Replica-" in line:
            ancestor_lfn, _ = line.split(" : ", 1)
            ancestor_lfn = ancestor_lfn.strip()
            ancestors[current_key] += [ancestor_lfn]

    assert set(ancestors.keys()) == set(
        output_LFNs
    ), f"not all provided LFNs had ancestors for depth {depth}"
    return ancestors


def merge_summary_xmls(xml_files):
    merged_summary = summary.Merge(xml_files)
    return str(merged_summary.xml())


async def fetch_merged_summary_xml(
    trf_output_lfns, appName, step_index=1, configPath=None
):
    extra_env = dict()
    krb5_cache = await get_krb5_creds(
        settings.eos_token_user, settings.eos_token_password, "CERN.CH"
    )
    extra_env["KRB5CCNAME"] = f"FILE:{krb5_cache.name}"

    task_chunk_sz = 10
    xml_files = []

    with tempfile.TemporaryDirectory() as _tmpdir:
        tmpdir = Path(_tmpdir)

        deque_o = deque(
            [
                download_summary_xml(
                    output_lfn,
                    appName,
                    tmpdir,
                    step_index=step_index,
                    extra_env=extra_env,
                    configPath=configPath,
                )
                for output_lfn in trf_output_lfns
            ]
        )

        while deque_o:  # while not empty
            # execute N=task_chunk_sz tasks
            xml_files += await asyncio.gather(
                *[deque_o.popleft() for _ in range(task_chunk_sz) if deque_o]
            )

        # merge all of these together
        return merge_summary_xmls([str(p) for p in xml_files])


def upload_summary(wg, analysis, version, name, mergedSummaryFn, signed_post, body):
    http_response = requests.post(
        signed_post["url"],
        data=signed_post["fields"],
        files={"file": (mergedSummaryFn, body)},
    )
    assert http_response.status_code == 204


def get_summary(wg, analysis, version, name, mergedSummaryFn):
    try:
        summary = settings.s3.client.get_object(
            Bucket=settings.s3.bucket,
            Key=os.path.join(
                "productions",
                wg,
                analysis,
                version,
                name,
                mergedSummaryFn,
            ),
        )
        content_length = summary["ContentLength"]
        age: datetime.timedelta = (
            datetime.datetime.now(tz=tzutc()) - summary["LastModified"]
        )
        if content_length < 5:
            if age.total_seconds() > (15.0 * 60.0):
                # the summary was in process of being created but that never finished
                return None
            # the summary is still being created
            return "0"
        return summary["Body"]
    except settings.s3.client.exceptions.NoSuchKey:
        return None
    except botocore.exceptions.ClientError as e:
        if e.response["Error"]["Code"] == "NoSuchKey":
            # The key does not exist
            return None
        else:
            raise e


def list_possible_summary_filenames(transforms: list):
    mergedSummaryFns = []
    for trf in transforms:
        for step_index, step in enumerate(trf["steps"]):
            appName, _ = step["application"].split("/")
            mergedSummaryFn = f"summary{appName}_{trf['id']}_{step_index+1}.xml"
            mergedSummaryFns += [mergedSummaryFn]

    return mergedSummaryFns


async def fetch_all_summary_XML_task(wg, analysis, version, name):
    apc = AnalysisProductionsClient()
    ret = apc.getProductions(
        wg=wg,
        analysis=analysis,
        version=version,
        name=name,
        with_lfns=True,
        with_pfns=False,
        with_transformations=True,
    )
    samples = returnValueOrRaise(ret)
    if len(samples) == 0 or len(samples) > 1:
        raise NotImplementedError(samples)
    sample = samples[0]

    if sample["state"] != "ready":
        # We should not do anything unless all output
        # is ready.
        return

    transforms = sample["transformations"]
    output_lfns = sample["lfns"]

    lfn_parts = output_lfns[0].split("/")
    if len(lfn_parts) == 0:
        raise NotImplementedError(f"Bad LFN: {lfn_parts!r}")
    configPath = "/".join(lfn_parts[:4])

    # Generate presigned POST (seems put_object permissions are not granted)
    signed_post = settings.s3.client.generate_presigned_post(
        settings.s3.bucket,
        os.path.join(
            "productions",
            wg,
            analysis,
            version,
            name,
            "${filename}",
        ),
        Fields={"acl": "private"},
        Conditions=[{"acl": "private"}, ["content-length-range", 0, 2 * 1024**3]],
        ExpiresIn=30 * 60,
    )

    # create empty files for each merged summary to be created
    # on s3 to avoid duplicate tasks running while we are doing work
    for trf in transforms:
        for step_index, step in enumerate(trf["steps"]):
            appName, _ = step["application"].split("/")
            mergedSummaryFn = f"summary{appName}_{trf['id']}_{step_index+1}.xml"
            resp = get_summary(wg, analysis, version, name, mergedSummaryFn)
            if resp is None:
                # create an empty file while we do work
                upload_summary(
                    wg, analysis, version, name, mergedSummaryFn, signed_post, "0"
                )
            elif resp == "0":
                logger.info(
                    f"Task stopped as {mergedSummaryFn} is already in process of creation"
                )
                return  # either an operation is in progress or merged summary was already created

    # start the work
    while transforms:
        mergedSummariesInner = []
        trf = transforms.pop()

        # (1) get merged summary XML for each step
        for step_index, step in enumerate(trf["steps"]):
            appName, _ = step["application"].split("/")
            mergedSummaryFn = f"summary{appName}_{trf['id']}_{step_index+1}.xml"

            mergedSummary = await fetch_merged_summary_xml(
                output_lfns, appName, step_index + 1, configPath=configPath
            )
            mergedSummariesInner.append(mergedSummary)
            upload_summary(
                wg, analysis, version, name, mergedSummaryFn, signed_post, mergedSummary
            )

        # (2) get input files from summary XML for previous transform's output files
        root = ET.fromstring(mergedSummariesInner[0])
        output_lfns = [
            input_file.get("name").removeprefix("LFN:")
            for input_file in root.findall("./input/file")
        ]

        # (3) go to previous transformation and start at (1), if no more transforms, stop
