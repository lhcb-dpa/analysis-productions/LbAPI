###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""This module contains celery tasks for consistency checks.

These are mostly used as periodic jobs that run with celery beat for
recovering from random grid failures and automatically retrying jobs.
"""
import asyncio
import json
import logging
import re
from collections import Counter
from datetime import UTC, datetime, timedelta

import requests
from sqlalchemy import select

from LbAPI.ci.queries import resubmit_job, set_job_status
from LbAPI.database import SessionLocal
from LbAPI.database.webhooks import CIRun, Job
from LbAPI.enums import JobStatus
from LbAPI.settings import (
    dirac_host_cert,
    dirac_host_key,
    gitlab,
    opensearch,
    settings,
)
from LbAPI.utils import run

logger = logging.getLogger(__name__)


async def check_dirac_wms_status():
    from . import start_job

    async with SessionLocal() as session:
        query = select(Job.id, Job.submitted_id)
        query = query.where(
            Job.status.in_(
                {JobStatus.SUBMITTED, JobStatus.RUNNING, JobStatus.VALIDATING}
            )
        )
        query = query.where(Job.outdated.is_(False))
        running_jobs = {
            job_id: ref.split(":", 1)[-1]
            for job_id, ref in (await session.execute(query)).all()
            if ref.startswith("dirac:")
        }
    if len(running_jobs) == 0:
        logger.debug("Skipping status check as there are no running jobs")
        return

    logger.debug(
        "Checking status of %s jobs: %s",
        len(running_jobs),
        ",".join(running_jobs.values()),
    )

    cmd = (
        settings.lb_dirac
        + [
            "dirac-wms-job-status",
            "--option=/DIRAC/Security/UseServerCertificate=yes",
            f"--option=/DIRAC/Security/CertFile={dirac_host_cert.path}",
            f"--option=/DIRAC/Security/KeyFile={dirac_host_key.path}",
        ]
        + list(running_jobs.values())
    )
    returncode, stdout, stderr = await run(*cmd)
    assert returncode == 0, (returncode, stdout, stderr)
    assert stderr == ""
    statuses = {}
    for line in stdout.splitlines():
        match = re.fullmatch(
            r"JobID=(\d+) ApplicationStatus.+; Status=([A-Za-z]+);.+", line
        )
        if match is not None:
            ref, status = match.groups()
            statuses[ref] = status
    logger.debug("Job status summary: %r", Counter(statuses.values()))

    for job_id, ref in running_jobs.items():
        if ref not in statuses:
            logger.debug("Status is missing for %s with DIRAC ID %s", job_id, ref)
            await set_job_status(job_id, JobStatus.CANCELLED)
        elif statuses[ref] in ["Killed", "Failed"]:
            logger.debug("Cancelling %s as DIRAC status is %s", job_id, statuses[ref])
            await set_job_status(job_id, JobStatus.CANCELLED)
            new_job_id = await resubmit_job(job_id, automatic=True)
            if new_job_id:
                start_job.delay(new_job_id)


async def check_last_log_updates():
    from . import start_job

    async with SessionLocal() as session:
        running_jobs = (
            await session.execute(
                select(CIRun.pipeline_id, CIRun.name.label("cirun_name"), Job.id)
                .where(CIRun.id == Job.ci_run_id)
                .where(Job.status == "RUNNING")
                .where(Job.outdated.is_(False))
            )
        ).all()

    logger.debug("Checking log updates for %s running jobs", len(running_jobs))
    for job in running_jobs:
        async with opensearch.client() as client:
            results = await client.search(
                body={
                    "query": {"bool": {"must": [{"term": {"job_id": job.id}}]}},
                    "sort": [{"timestamp": {"order": "desc"}}],
                },
                params=dict(size=1),
                index=f"{settings.app_logs_index}-*",
            )
        hits = results["hits"]["hits"]
        if not hits:
            logger.debug(
                "No matches for %s(%s) job_id=%s",
                job.pipeline_id,
                job.cirun_name,
                job.id,
            )
            continue
        timestamp = hits[0]["_source"]["timestamp"]
        if timestamp.endswith("Z"):
            timestamp = timestamp[:-1]
        else:
            raise NotImplementedError("Only UTC timestamps are supported")
        time_since_last_update = datetime.utcnow() - datetime.fromisoformat(timestamp)
        if time_since_last_update.total_seconds() > 60**2:
            logger.debug(
                "No log updates for %s(%s) job_id=%s for %s seconds, cancelling job",
                job.pipeline_id,
                job.cirun_name,
                job.id,
                time_since_last_update.total_seconds(),
            )
            await set_job_status(job.id, JobStatus.CANCELLED)
            new_job_id = await resubmit_job(job.id, automatic=True)
            if new_job_id:
                start_job.delay(new_job_id)


async def check_webhooks():

    await check_webhook(76059, 6549)  # FIXME: use database!

    # TODO: lbmcsubmit
    # await check_webhook(76059, 6549)  # FIXME: use database!


async def check_webhook(project_id, hook_id):
    # https://docs.gitlab.com/ee/api/project_webhooks.html#get-a-list-of-project-webhook-events
    response = requests.get(
        f"{gitlab.instance}/api/v4/projects/{project_id}/hooks/{hook_id}/events",
        headers={
            "Authorization": f"Bearer {gitlab.token}",
        },
        params={"status": "server_failure"},
    )
    successful_event_ids = []
    page = 1
    last_successful_webhook_datetime = datetime.now(UTC)
    last_failing_webhook_datetime = None
    # Gather all failing webhooks that occurred within the last day
    for failed_hook in response.json():
        fdate = datetime.fromisoformat(failed_hook["created_at"])
        if not (datetime.now(UTC) - fdate) > timedelta(days=1):
            last_failing_webhook_datetime = fdate
    if last_failing_webhook_datetime is None:
        logging.info("No failed webhooks to resubmit!")
        return
    # Gather all successful webhooks up until the last failing webhook
    while last_successful_webhook_datetime > last_failing_webhook_datetime:
        response2 = requests.get(
            f"{gitlab.instance}/api/v4/projects/{project_id}/hooks/{hook_id}/events",
            headers={
                "Authorization": f"Bearer {gitlab.token}",
            },
            params={"status": "successful", "page": page},
        )
        if not response2.ok:
            logger.error(
                f"Abort webhook check due to failing API call: {response2.json()}"
            )
            return
        if len(response2.json()) == 0:
            break
        # Idempotency keys let us check if we resubmitted this webhook already:
        successful_event_ids.extend(
            [r["request_headers"]["Idempotency-Key"] for r in response2.json()]
        )
        last_successful_webhook_datetime = datetime.fromisoformat(
            response2.json()[-1]["created_at"]
        )
        page += 1
    logging.debug(f"fetched {page} pages of successful webhooks")
    # for each event
    if not response.ok:
        logging.error(
            f"Error {response.status_code} from GitLab while checking webhook events"
        )
        return
    resendable_hook_ids = set()
    for event in response.json():
        # confirm the response_status == "200"
        if event["response_status"] == "200":
            continue
        if (
            event["request_headers"].get("Idempotency-Key", None)
            in successful_event_ids
        ):
            logger.debug(
                f"webhook (hook id {hook_id}) event id {event['id']} was already resubmitted"
            )
            continue
        resendable_hook_ids.add(event["id"])
        logger.warning(
            f"Discovered unhandled webhook (hook id {hook_id}) event id {event['id']} which should be resubmitted:"
        )
        logger.warning(f"response code: {event['response_status']}")
        logger.warning(f"response: {event['response_body']!r}")
        logger.warning(f"payload: {json.dumps(event['request_data'], indent=2)}")
    logger.warning(
        f"Resubmitting {len(resendable_hook_ids)} webhooks: {resendable_hook_ids!r}"
    )
    rate_limit = 4
    for resendable_hook in resendable_hook_ids:
        # POST /projects/:id/hooks/:hook_id/events/:hook_event_id/resend
        # this endpoint has a configured ratelimit (5 per minute)
        if rate_limit == 0:
            logger.info("rate limit protection - pause for 2 minutes.")
            await asyncio.sleep(120)
            rate_limit = 4
        rate_limit -= 1
        response = requests.post(
            f"{gitlab.instance}/api/v4/projects/{project_id}/hooks/{hook_id}/events/{resendable_hook}/resend",
            headers={
                "Authorization": f"Bearer {gitlab.token}",
            },
        )
        if response.ok:
            logger.info(f"Resubmitted webhook id {hook_id} event id {resendable_hook}")
        else:
            logger.error(
                f"Could not resubmit webhook id {hook_id} event id {resendable_hook}: {response.json()!r}"
            )
