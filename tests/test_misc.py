###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from itertools import chain

import pytest

extra_test_safe_eos_name_unchanged_cases = [
    [
        f"{char}HLT1HLT2",
        f"HLT1{char}HLT2",
        f"HLT1HLT2{char}",
        f"{char}{char}HLT1{char}{char}HLT2{char}{char}",
    ]
    for char in [" ", ".", "-", "_", "~", "#", ":", "^"]
]


@pytest.mark.parametrize(
    "orig", ["simple"] + list(chain(*extra_test_safe_eos_name_unchanged_cases))
)
def test_safe_eos_name_unchanged(orig):
    from LbAPI.utils import _make_eos_safe_name

    assert _make_eos_safe_name(orig) == orig


@pytest.mark.parametrize(
    "orig, safe",
    [
        ("HLT1,HLT2", "HLT1_HLT2"),
        (",,HLT1,,HLT2,,", "__HLT1__HLT2__"),
        ("//HLT1/HLT2//", "__HLT1_HLT2__"),
    ],
)
def test_safe_eos_name(orig, safe):
    from LbAPI.utils import _make_eos_safe_name

    assert _make_eos_safe_name(orig) == safe
