###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
import tempfile
from collections import namedtuple
from contextlib import contextmanager
from pathlib import Path

from fastapi.testclient import TestClient

from LbAPI import app
from LbAPI.ci import actions, queries
from LbAPI.enums import WebhookType

from .utils import clean_db, client, job_runner

TEST_PROJECT_ID = 148168
TEST_PROJECT_NAME = "lhcb-dpa/analysis-productions/test-repo"
MC_REQUEST_MR_ID = 1
MC_REQUEST_SHA_SUMS = [
    "dd95ecdf4bc2fc84b0c823ab614e4ef4b05c7249",
    "23c6862e99d3f729293ac1deb56836edc123f9ab",
    "d16dbeb4ed0a84341d8ab434445c8e47ccd79d92",
    "b98019dba24fc3b06ad1e2cf4cf916dfb65e5d0e",
    "538a0245800f8a971bdb6911f285699bcaee6b3a",
    "b123db6252b63b23b9f8558b53c3df422520d480",
    "cfc3cd436dda054442952f11363a236f4d395d5a",
]
REF_JOB_DIR = Path(__file__).parent / "ci-reference" / "successful"


RepoWebhook = namedtuple("RepoWebhook", ["id", "repo_name", "secret"])
RepoPipeline = namedtuple("RepoPipeline", ["webhook", "commit_sha", "mr_id", "id"])
RepoCIRun = namedtuple("RepoCIRun", ["pipeline", "names"])


async def fill_dummy_db_data():
    with TestClient(app):
        secret = await queries.create_webhook(
            TEST_PROJECT_ID, TEST_PROJECT_NAME, WebhookType.MC_REQUEST
        )
        webhook = RepoWebhook(TEST_PROJECT_ID, TEST_PROJECT_NAME, secret)

        coroutines = [
            _start_dummy_pipeline(i, webhook.id, commit_sha)
            for i, commit_sha in enumerate(MC_REQUEST_SHA_SUMS)
        ]
        pipeline_ids = await asyncio.gather(*coroutines)
    return pipeline_ids


async def _start_dummy_pipeline(i, project_id, commit_sha):
    pipeline_id = await actions.create_pipeline(
        project_id, commit_sha, "commit_message", MC_REQUEST_MR_ID, "jdoe"
    )
    _, _, ci_runs = await actions.start_pipeline(
        project_id, commit_sha, MC_REQUEST_MR_ID
    )
    for ci_run in ci_runs:
        await _start_dummy_ci_run(
            pipeline_id, ci_run, skip_jobs=i % 2 == 0, fail_jobs=i % 3 != 2
        )
    return pipeline_id


async def _start_dummy_ci_run(
    pipeline_id: int, ci_run_name: str, skip_jobs: bool = False, fail_jobs: bool = False
):
    job_ids = await actions.start_cirun(pipeline_id, ci_run_name)

    tasks = []
    for job_id in list(job_ids):
        if skip_jobs and job_id % 7 == 0:
            continue
        success = not fail_jobs or job_id % 3 != 0
        submitted_id = await actions.start_job(job_id)
        job_dir = Path(tempfile.gettempdir()) / f"lbapi-{submitted_id.split(':', 1)[1]}"
        tasks.append(job_runner(job_dir, client, success=success))
    await asyncio.gather(*tasks)


@contextmanager
async def repo_webhook():
    secret = await queries.create_webhook(
        TEST_PROJECT_ID, TEST_PROJECT_NAME, WebhookType.MC_REQUEST
    )
    try:
        yield RepoWebhook(TEST_PROJECT_ID, TEST_PROJECT_NAME, secret)
    finally:
        await clean_db(TEST_PROJECT_ID)


if __name__ == "__main__":
    asyncio.run(fill_dummy_db_data())
