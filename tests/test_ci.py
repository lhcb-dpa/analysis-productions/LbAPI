###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
import tempfile
from collections import namedtuple
from pathlib import Path

import pytest
import pytest_asyncio
import yaml

from LbAPI.ci import actions, queries
from LbAPI.enums import WebhookType

from .utils import clean_db, client, get_sso_token, job_runner

# Objects used by fixtures for convenience
RepoWebhook = namedtuple("RepoWebhook", ["id", "repo_name", "secret"])
RepoPipeline = namedtuple("RepoPipeline", ["webhook", "commit_sha", "mr_id", "id"])
RepoCIRun = namedtuple("RepoCIRun", ["pipeline", "names"])

# Examples
TEST_PROJECT_ID = 148168
TEST_PROJECT_NAME = "lhcb-dpa/analysis-productions/test-repo"
MC_REQUEST_MR_ID = 1
MC_REQUEST_SHA_1 = "dd95ecdf4bc2fc84b0c823ab614e4ef4b05c7249"
MC_REQUEST_MR_ID_YML = 16
MC_REQUEST_SHA_YML = "fe8842e45ec965ffb7590835fdf5771ae21e7869"
MC_REQUEST_MR_ID_OUTSIDE = 16
MC_REQUEST_SHA_1_OUTSIDE = "f885abb217f86596cdbd39a27486281299cb2162"


@pytest_asyncio.fixture
async def repo_webhook():
    secret = await queries.create_webhook(
        TEST_PROJECT_ID, TEST_PROJECT_NAME, WebhookType.MC_REQUEST
    )
    yield RepoWebhook(TEST_PROJECT_ID, TEST_PROJECT_NAME, secret)

    await clean_db(TEST_PROJECT_ID)


@pytest_asyncio.fixture
async def repo_pipeline_yml(repo_webhook):
    pipeline_id = await actions.create_pipeline(
        repo_webhook.id,
        MC_REQUEST_SHA_YML,
        "commit message",
        MC_REQUEST_MR_ID_YML,
        "jdoe",
    )
    yield RepoPipeline(
        repo_webhook, MC_REQUEST_SHA_YML, MC_REQUEST_MR_ID_YML, pipeline_id
    )


@pytest.mark.asyncio
async def test_repo_ci_run_yml(repo_pipeline_yml, apply_monkeypatches):
    _, _, ci_runs = await actions.start_pipeline(
        repo_pipeline_yml.webhook.id,
        repo_pipeline_yml.commit_sha,
        repo_pipeline_yml.mr_id,
    )

    assert len(ci_runs) == 0


@pytest_asyncio.fixture
async def repo_pipeline_outside(repo_webhook):
    pipeline_id = await actions.create_pipeline(
        repo_webhook.id,
        MC_REQUEST_SHA_1_OUTSIDE,
        "commit message",
        MC_REQUEST_MR_ID_OUTSIDE,
        "jdoe",
    )
    yield RepoPipeline(
        repo_webhook, MC_REQUEST_SHA_1_OUTSIDE, MC_REQUEST_MR_ID_OUTSIDE, pipeline_id
    )


@pytest_asyncio.fixture
async def apply_monkeypatches(monkeypatch):
    from LbAPI.ci import actions

    monkeypatch.setattr(
        actions,
        "get_proxy_data",
        lambda *args, **kwargs: None,
    )
    monkeypatch.setattr(
        actions.submission,
        "get_user_mapping",
        lambda: {"jdoe": "jdoe"},
    )
    monkeypatch.setattr(
        "LbAPI.celery.update_discussion.delay",
        lambda *args, **kwargs: None,
    )
    yield


@pytest_asyncio.fixture
async def repo_ci_run_outside(repo_pipeline_outside, apply_monkeypatches):
    _, _, ci_runs = await actions.start_pipeline(
        repo_pipeline_outside.webhook.id,
        repo_pipeline_outside.commit_sha,
        repo_pipeline_outside.mr_id,
    )
    yield RepoCIRun(repo_pipeline_outside, ci_runs)


@pytest.mark.asyncio
async def test_start_cirun_v2(repo_ci_run_outside):
    assert "my-analysis" in repo_ci_run_outside.names
    job_ids = await actions.start_cirun(repo_ci_run_outside.pipeline.id, "my-analysis")
    assert job_ids is None


@pytest_asyncio.fixture
async def repo_pipeline(repo_webhook):
    pipeline_id = await actions.create_pipeline(
        repo_webhook.id, MC_REQUEST_SHA_1, "commit message", MC_REQUEST_MR_ID, "jdoe"
    )
    yield RepoPipeline(repo_webhook, MC_REQUEST_SHA_1, MC_REQUEST_MR_ID, pipeline_id)


@pytest_asyncio.fixture
async def repo_ci_run(repo_pipeline, apply_monkeypatches):
    _, _, ci_runs = await actions.start_pipeline(
        repo_pipeline.webhook.id, repo_pipeline.commit_sha, repo_pipeline.mr_id
    )
    yield RepoCIRun(repo_pipeline, ci_runs)


@pytest.mark.asyncio
async def test_create_pipeline(repo_pipeline):
    # Creating the same pipeline should return None
    result = await actions.create_pipeline(
        repo_pipeline.webhook.id,
        repo_pipeline.commit_sha,
        "commit_message",
        repo_pipeline.mr_id,
        "jdoe",
    )
    assert result is None

    # Creating the same pipeline should return None
    other_pipeline_id = await actions.create_pipeline(
        repo_pipeline.webhook.id,
        repo_pipeline.commit_sha,
        "commit_message",
        repo_pipeline.mr_id + 1,
        "jdoe",
    )
    assert isinstance(other_pipeline_id, int)
    assert other_pipeline_id == repo_pipeline.id + 1


@pytest.mark.asyncio
async def test_start_cirun(repo_ci_run, monkeypatch):
    from LbAPI.ci.actions import mc_request as module

    async def mock_get_previous_bkk_paths_from_request(
        request, author, all_sim_versions
    ):
        return []

    monkeypatch.setattr(
        module,
        "get_previous_bkk_paths_from_request",
        mock_get_previous_bkk_paths_from_request,
    )

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/log", **get_sso_token()
    )
    assert r.status_code == 200, r.text
    assert r.text == ""

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/missing-key/log", **get_sso_token()
    )
    assert r.status_code == 404, r.text

    assert "my-analysis" in repo_ci_run.names
    # this is the bit that needs to be tested
    job_ids = await actions.start_cirun(repo_ci_run.pipeline.id, "my-analysis")
    assert len(job_ids) == 18

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/log", **get_sso_token()
    )
    assert r.status_code == 200, r.text
    assert r.text.splitlines()[1].startswith("Exit code from")
    assert "45,000,000 events" in r.text

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/yaml",
        **get_sso_token(),
    )
    assert r.status_code == 200, r.text
    assert len(yaml.safe_load(r.text)) == 6

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/files",
        **get_sso_token(),
    )
    assert r.status_code == 200, r.text
    yaml_filenames = r.json()
    assert "samples.yaml" in yaml_filenames

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/files/samples.yaml",
        **get_sso_token(),
    )
    assert r.status_code == 200, r.text
    assert len(r.text.splitlines()) >= 15, "samples.yaml"

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/files/missing-file.yaml",
        **get_sso_token(),
    )
    assert r.status_code == 404, r.text

    r = client.get(
        f"/pipelines2/{repo_ci_run.pipeline.id}/runs/my-analysis/jobs/"
        "samples.yaml:My Analysis 2018 pp MagUp:30000000/yaml",
        **get_sso_token(),
    )
    assert r.status_code == 200, r.text
    data = yaml.safe_load(r.text)
    assert len(data) == 1
    assert data[0]["name"] == "My Analysis 2018 pp MagUp"


# @pytest.mark.asyncio
# async def test_log_uploader(repo_ci_run):


@pytest.mark.asyncio
async def test_run_job(repo_ci_run, monkeypatch):
    from LbAPI.ci.actions import mc_request as module

    async def mock_get_previous_bkk_paths_from_request(
        request, author, all_sim_versions
    ):
        return []

    monkeypatch.setattr(
        module,
        "get_previous_bkk_paths_from_request",
        mock_get_previous_bkk_paths_from_request,
    )

    assert "my-analysis" in repo_ci_run.names
    jobs = await actions.start_cirun(repo_ci_run.pipeline.id, "my-analysis")
    assert len(jobs) == 18

    job_id = list(jobs)[0]
    await _run_job_check_api(repo_ci_run.pipeline.id, job_id, jobs[job_id], False)
    submitted_id = await actions.start_job(job_id)
    job_dir = Path(tempfile.gettempdir()) / f"lbapi-{submitted_id.split(':', 1)[1]}"
    await job_runner(job_dir, client, success=True)

    await _run_job_check_api(repo_ci_run.pipeline.id, job_id, jobs[job_id], True)


async def _run_job_check_api(pipeline_id, job_id, job_name, is_done):
    if is_done:
        expected_job_status = "SUCCESS"
        expected_status_counts = {"PENDING": 17, "SUCCESS": 1}
    else:
        expected_job_status = "PENDING"
        expected_status_counts = {"PENDING": 18}

    all_pipeline_info = client.get("/pipelines2/", **get_sso_token()).json()
    for pipeline_info in all_pipeline_info:
        if pipeline_info["id"] == pipeline_id:
            assert pipeline_info["status"] == "PROCESSED"
            assert len(pipeline_info["ci_runs"]) == 1
            ci_run_info = pipeline_info["ci_runs"][0]
            assert ci_run_info["status"] == "PROCESSED"
            assert ci_run_info["name"] == "my-analysis"
            assert ci_run_info["job_statuses"] == expected_status_counts
            break
    else:
        raise AssertionError(f"Pipeline {pipeline_id} not found")

    pipeline_info = client.get(f"/pipelines2/{pipeline_id}/", **get_sso_token()).json()
    assert pipeline_info["status"] == "PROCESSED"
    assert len(pipeline_info["ci_runs"]) == 1
    ci_run_info = pipeline_info["ci_runs"][0]
    assert ci_run_info["status"] == "PROCESSED"
    assert ci_run_info["name"] == "my-analysis"
    assert ci_run_info["job_statuses"] == expected_status_counts

    ci_run_info = client.get(
        f"/pipelines2/{pipeline_id}/runs/my-analysis/", **get_sso_token()
    ).json()
    assert ci_run_info["status"] == "PROCESSED"
    found = False
    for job_info in ci_run_info["jobs"]:
        if job_info["id"] == job_id:
            assert not found
            found = True
            assert job_info["name"] == job_name
            assert job_info["status"] == expected_job_status
        else:
            assert job_info["status"] == "PENDING"
    assert found

    job_info = client.get(
        f"/pipelines2/{pipeline_id}/runs/my-analysis/jobs/{job_name}", **get_sso_token()
    ).json()
    assert job_info["name"] == job_name
    assert job_info["status"] == expected_job_status
    if is_done:
        assert job_info["result"] is not None
    else:
        assert job_info["result"] is None

    r = client.get(
        f"/pipelines2/{pipeline_id}/runs/my-analysis/jobs/{job_name}/logs",
        **get_sso_token(),
    )
    log_filenames = r.json()
    assert r.status_code == 200
    if is_done:
        assert "DIRAC.log" in log_filenames
        assert "DaVinci_00012345_00006789_8.log" in log_filenames

        for _ in range(10):
            r = client.get(
                f"/pipelines2/{pipeline_id}/runs/my-analysis/jobs/{job_name}/logs/DIRAC.log",
                **get_sso_token(),
            )
            assert r.status_code == 200, r.text
            log = r.text
            assert len(log.splitlines()) >= 100
            if "Workflow execution successful, exiting" in log.splitlines()[-1]:
                break
            # Repeat for a while as opensearch is eventually consistent
            await asyncio.sleep(0.25)
        else:
            raise AssertionError("DIRAC.log did not finish")

        for log_filename in log_filenames:
            r = client.get(
                f"/pipelines2/{pipeline_id}/runs/my-analysis/jobs/{job_name}/logs/{log_filename}",
                **get_sso_token(),
            )
            assert r.status_code == 200, r.text
            assert len(r.text.splitlines()) >= 100, log_filename
    else:
        assert log_filenames == []
