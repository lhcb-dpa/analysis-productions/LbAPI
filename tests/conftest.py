###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
from contextlib import contextmanager
from unittest.mock import PropertyMock

import pytest
from DIRAC.Core.Utilities.ReturnValues import convertToReturnValue
from fastapi import Request
from starlette.datastructures import Address

from LbAPI.database import create_engine


async def prep_env():
    await create_engine()


asyncio.run(prep_env())


@pytest.fixture(autouse=True)
def override_request_client(monkeypatch):
    """
    Automatically used pytest fixture to monkey patch `fastapi.Request.client`
    so it returns `starlette.datastructures.Address("0.0.0.0", 1)`.
    """
    mock_client = PropertyMock(return_value=Address("0.0.0.0", 1))
    monkeypatch.setattr(Request, "client", mock_client)


# @pytest.fixture
# def fake_user(monkeypatch):
#     user = User(
#         username="roneil",
#         roles=[Roles.DEFAULT],
#         email="fake.user@cern.invalid",
#         name="Fake User",
#     )
#     monkeypatch.setattr(
#         "LbAPI.app.dependency_overrides", {get_current_user: lambda: user}
#     )
#     yield user


# @pytest.fixture
# def fake_liason(monkeypatch):
#     user = User(
#         username="to-be-set",
#         roles=[Roles.DEFAULT, Roles.ADMIN],
#         email="fake.liason@cern.invalid",
#         name="Fake Liaison",
#     )
#     monkeypatch.setattr(
#         "LbAPI.app.dependency_overrides", {get_current_user: lambda: user}
#     )
#     yield user


# @pytest.fixture
# def fake_admin(monkeypatch):
#     user = User(
#         username="cburr",
#         roles=[Roles.DEFAULT, Roles.ADMIN],
#         email="fake.admin@cern.invalid",
#         name="Fake Admin",
#     )
#     monkeypatch.setattr(
#         "LbAPI.app.dependency_overrides", {get_current_user: lambda: user}
#     )
#     yield user


@pytest.fixture(autouse=True)
def automatic_patches(monkeypatch, fake_apc_client):
    from LbAPI.auth import dirac_username_mapping

    # Default to assuming the request is coming from a CERN IP
    monkeypatch.setattr("starlette.datastructures.Address.host", "188.184.0.27")

    # Fake the mapping of CERN usernames to DIRAC nicknames
    dirac_username_mapping.cache[tuple()] = {"lbaptestuser": "lbaptestuser"}

    # Make it so that EOS tokens appear to have been generated
    async def fake_generate_eos_token(*args, **kwargs):
        return "fake-token", {"blah": "blah"}

    monkeypatch.setattr("LbAPI.gitlab.generate_eos_token", fake_generate_eos_token)

    yield


@pytest.fixture
def fake_apc_client(monkeypatch):
    @contextmanager
    def FakeRunAsDIRACUser(username):
        yield

    class FakeAnalysisProductionsClient:
        @convertToReturnValue
        def listAnalyses(self, *args, **kwargs):
            return {
                "WG1": ["Analysis1", "Analysis2"],
                "WG2": ["Analysis2", "Analysis3", "Analysis4"],
            }

        @convertToReturnValue
        def getProductions(self, wg, analysis, **kwargs):
            if wg.lower() != "wg1" or analysis.lower() != "analysis1":
                raise NotImplementedError()
            return [
                {
                    "wg": "charm",
                    "analysis": "d2hll",
                    "version": "v0r0p0",
                    "name": "sample1",
                    "request_id": 1234,
                    "sample_id": 67,
                    "state": "ready",
                    "owners": [],
                    "lfns": {
                        "/lhcb/dir/file1.root": [
                            "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/dir/file1.root"
                        ],
                        "/lhcb/dir/file2.root": [
                            "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/dir/file2.root"
                        ],
                        "/lhcb/dir/file4.root": [
                            "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/dir/file4.root"
                        ],
                    },
                    "available_bytes": 123456,
                    "total_bytes": 246912,
                    "last_state_update": "2022-02-18T10:29:10",
                    "validity_start": "2017-09-14T15:16:39",
                    "validity_end": None,
                }
            ]

    monkeypatch.setattr("LbAPI.auth.RunAsDIRACUser", FakeRunAsDIRACUser)
    monkeypatch.setattr(
        "LHCbDIRAC.ProductionManagementSystem.Client.AnalysisProductionsClient.AnalysisProductionsClient",
        FakeAnalysisProductionsClient,
    )
    yield
