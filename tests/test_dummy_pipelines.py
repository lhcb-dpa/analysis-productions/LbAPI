###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import pytest_asyncio

from tests.utils import get_sso_token

from .dummy_data import fill_dummy_db_data
from .utils import clean_db, client


@pytest_asyncio.fixture
async def dummy_db_data():
    pipeline_ids = await fill_dummy_db_data()
    try:
        yield pipeline_ids
    finally:
        for pipeline_id in pipeline_ids:
            await clean_db(pipeline_id)


@pytest.mark.skip
@pytest.mark.asyncio
async def test_with_dummy_data(dummy_db_data):
    with client:
        r = client.get("/pipelines2/", **get_sso_token())
        pipeline_info = r.json()
        assert len(pipeline_info) >= 5
