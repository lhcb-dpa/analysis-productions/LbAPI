###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import requests
from fastapi.testclient import TestClient

from LbAPI import app
from LbAPI.models import JobInfo, PipelineProductionInfo

client = TestClient(app)


def test_auth_required():
    response = client.get("/pipelines")
    assert response.status_code == 403
    assert response.json() == {"detail": "Not authenticated"}

    response = client.get(
        "/pipelines",
        headers={
            "Authorization": (
                "Bearer "
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9."
                "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ."
                "SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
            )
        },
    )
    assert response.status_code == 401, response.text
    assert response.json() == {"detail": "Could not validate credentials"}


def test_list_pipelines(fake_user):
    response = client.get("/pipelines")
    assert response.status_code == 200
    results = response.json()
    assert len(results) == 100
    for result in results:
        assert PipelineProductionInfo.parse_obj(result)


@pytest.mark.parametrize(
    "endpoint,condition",
    [
        ["/pipelines/stats", lambda x: 500 <= len(x) <= 1000],
        ["/pipelines/2771046/stats", lambda x: 1 == len(x)],
        ["/pipelines/2771046/LbchicpK/stats", lambda x: len(x)],
    ],
)
def test_pipelines_stats(fake_user, endpoint, condition):
    response = client.get(endpoint)
    assert response.status_code == 200
    results = response.json()
    assert condition(results)


@pytest.mark.parametrize(
    "endpoint,condition",
    [
        ["/pipelines/progress", lambda x: 500 <= len(x) <= 10_000],
        ["/pipelines/2771046/progress", lambda x: 1 == len(x)],
        ["/pipelines/2771046/LbchicpK/progress", lambda x: len(x)],
    ],
)
def test_pipelines_progress(fake_user, endpoint, condition):
    response = client.get(endpoint)
    assert response.status_code == 200
    results = response.json()
    assert condition(results)


def test_list_production(fake_user):
    response = client.get("/pipelines/100/LbchicpK")
    assert response.status_code == 404
    response = client.get("/pipelines/2771046/invalid")
    assert response.status_code == 404
    response = client.get("/pipelines/2771046/LbchicpK")
    assert response.status_code == 200
    result = response.json()
    assert PipelineProductionInfo.parse_obj(result)


@pytest.mark.parametrize(
    "params,n_results",
    [
        [None, 88],
        [{"status": "Failed"}, 29],
        [{"status": "Unknown"}, 0],
        [{"status": "Success"}, 59],
        [{"search": "2011"}, 14],
        [{"search": "2011*"}, 0],
        [{"search": "*2011"}, 0],
        [{"search": "201?"}, 0],
        [{"search": "*201?*"}, 88],
    ],
)
def test_list_jobs(fake_user, params, n_results):
    response = client.get("/pipelines/2771046/LbchicpK/jobs", params=params)
    assert response.status_code == 200
    results = response.json()
    assert len(results) == n_results
    for result in results:
        assert JobInfo.parse_obj(result)


def test_list_job(fake_user):
    response = client.get(
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling"
    )
    assert response.status_code == 200
    result = response.json()
    assert JobInfo.parse_obj(result)


def test_list_job_attempts(fake_user):
    response = client.get(
        "/pipelines/2874772/Radiative/jobs/RadTuple_2hG_MC11102202_stripping29r2p1_2017_down"
    )
    assert response.status_code == 200
    info_latest = JobInfo.parse_obj(response.json())
    assert info_latest.test.attempt == 1
    assert info_latest.test.status == "SUCCESS"

    response = client.get(
        "/pipelines/2874772/Radiative/jobs/RadTuple_2hG_MC11102202_stripping29r2p1_2017_down/tests/0"
    )
    assert response.status_code == 200
    info_0 = JobInfo.parse_obj(response.json())
    assert info_0.test.attempt == 0
    assert info_0.test.status == "FAILED"

    response = client.get(
        "/pipelines/2874772/Radiative/jobs/RadTuple_2hG_MC11102202_stripping29r2p1_2017_down/tests/1"
    )
    assert response.status_code == 200
    info_1 = JobInfo.parse_obj(response.json())
    assert info_1.test.attempt == 1
    assert info_1.test.status == "SUCCESS"

    assert info_0 != info_latest
    assert info_1 == info_latest


def test_list_job_404(fake_user):
    response = client.get("/pipelines/2771046/LbchicpK/jobs/invalid-job-name")
    assert response.status_code == 404


@pytest.mark.parametrize(
    "endpoint",
    [
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling/files/",
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling/tests/0/files/",
    ],
)
def test_list_job_files(fake_user, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 200
    assert response.json() == [
        "00012345_00006789_1.dvntuple.root",
        "DIRAC.log",
        "DaVinci_00012345_00006789_1.log",
        "bookkeeping_00012345_00006789_1.xml",
        "jobDescription.xml",
        "pool_xml_catalog.xml",
        "prodConf_DaVinci_00012345_00006789_1.py",
        "summaryDaVinci_00012345_00006789_1.xml",
    ]
    for fn in response.json():
        response1 = client.get(endpoint + fn, stream=True)
        assert response1.status_code == 200

        response = client.get(endpoint + fn + "/url")
        assert response.status_code == 200
        presigned_url = response.json()["url"]
        response2 = requests.get(presigned_url, stream=True)
        assert response2.status_code == 200

        assert response1.raw.read() == response2.raw.read()


def test_list_job_files_attempts(fake_user):
    base = "/pipelines/2827902/D02HHPi0/jobs/MC_D02pipipi0_MagUp"

    response = client.get(f"{base}/files")
    assert response.status_code == 200
    assert "DIRAC.log" in response.json()

    response = client.get(f"{base}/tests/0/files")
    assert response.status_code == 200
    assert "DIRAC.log" not in response.json()

    response = client.get(f"{base}/tests/1/files")
    assert response.status_code == 200
    assert "DIRAC.log" not in response.json()

    response = client.get(f"{base}/tests/2/files")
    assert response.status_code == 200
    assert "DIRAC.log" in response.json()

    response = client.get(f"{base}/tests/3/files")
    assert response.status_code == 404


@pytest.mark.parametrize(
    "endpoint",
    [
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling/files/DIRAC.log",
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling/tests/0/files/DIRAC.log",
    ],
)
def test_get_job_file(fake_user, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 200
    assert "lb-run" in response.text


def test_get_job_file_attempts(fake_user):
    base = "/pipelines/2874772/Radiative/jobs/RadTuple_2hG_MC11102202_stripping29r2p1_2017_down"

    response = client.get(f"{base}/files/Castelao_00012345_00006789_1.log")
    assert response.status_code == 200
    assert "segmentation violation" not in response.text

    response = client.get(f"{base}/tests/0/files/Castelao_00012345_00006789_1.log")
    assert response.status_code == 200
    assert "segmentation violation" in response.text

    response = client.get(f"{base}/tests/1/files/Castelao_00012345_00006789_1.log")
    assert response.status_code == 200
    assert "segmentation violation" not in response.text

    response = client.get(f"{base}/tests/2/files/Castelao_00012345_00006789_1.log")
    assert response.status_code == 404


@pytest.mark.parametrize(
    "endpoint",
    [
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling/files/DIRACS.log",
        "/pipelines/2771046/LbchicpK/jobs/LbJpsipK_2011_MagUp_ntupling/tests/0/files/DIRACS.log",
    ],
)
def test_get_job_file_missing(fake_user, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 404


def test_yaml_raw(fake_user):
    response = client.get("/pipelines/2771046/LbchicpK/yaml/raw")
    assert response.status_code == 200
    assert len(response.text) > 10000


def test_yaml_rendered(fake_user):
    response = client.get("/pipelines/2771046/LbchicpK/yaml/rendered")
    assert response.status_code == 200
    assert len(response.text) > 10000


def test_yaml_404(fake_user):
    response = client.get("/pipelines/2771046/invalid/yaml/raw")
    assert response.status_code == 404
    response = client.get("/pipelines/100/LbchicpK/yaml/raw")
    assert response.status_code == 404
    response = client.get("/pipelines/2771046/invalid/yaml/rendered")
    assert response.status_code == 404
    response = client.get("/pipelines/100/LbchicpK/yaml/rendered")
    assert response.status_code == 404
