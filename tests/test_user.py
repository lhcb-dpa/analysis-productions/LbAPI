###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import time

from fastapi.testclient import TestClient

from LbAPI import app

from .utils import get_sso_token

client = TestClient(app)


def test_missing():
    r = client.get("/user")
    assert r.status_code == 403, r.text


def test_bad_jwt():
    r = client.get("/user", headers={"Authorization": "Bearer invalid"})
    assert r.status_code == 401, r.text


def test_user():
    auth_kwargs = get_sso_token()

    r = client.get("/user", **auth_kwargs)
    assert r.status_code == 200, r.text
    assert r.json() == {
        "username": "lbaptestuser",
        "roles": ["default-role"],
        "email": "test.user@cern.invalid",
        "name": "Test User",
        "auth_type": "CERN_SSO",
        "token_id": None,
    }

    # Wait for the credentials to expire
    time.sleep(5)

    # Ensure that the expired credentials don't work
    r = client.get("/user", **auth_kwargs)
    assert r.status_code == 401, r.text
    assert r.json()["detail"] == "Could not validate credentials"
