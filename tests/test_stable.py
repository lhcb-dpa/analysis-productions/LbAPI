###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from fastapi.testclient import TestClient

from LbAPI import app

from .utils import get_sso_token

client = TestClient(app)


def test_list_analyses():
    response = client.get("/stable/v1/", **get_sso_token())
    assert response.status_code == 200
    result = response.json()
    assert result == {
        "WG1": ["Analysis1", "Analysis2"],
        "WG2": ["Analysis2", "Analysis3", "Analysis4"],
    }


def test_samples():
    # response = client.get("/stable/v1/WG1/Analysis1", params={"with_data": True})
    # assert response.status_code == 200
    # samples = response.json()
    # assert "lfns" in samples[0]
    # assert any(samples[0].values()), "No LFNs with PFNs found"
    # assert "transformations" not in samples[0]

    response = client.get("/stable/v1/WG1/Analysis1/", **get_sso_token())
    assert response.status_code == 200
    samples = response.json()
    assert len(samples) == 1

    # response = client.get("/stable/v1/charm/d2hll/tags")
    # assert response.status_code == 200
    # tags = response.json()
    # assert set(map(int, tags)) == {x["sample_id"] for x in samples}
