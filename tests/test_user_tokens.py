###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import secrets
import time

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import text

from LbAPI import app

from .utils import get_sso_token

client = TestClient(app)


@pytest.mark.parametrize("url", ["/user/tokens/"])
def test_list_missing(url):
    r = client.get(url)
    assert r.status_code == 403, r.text


@pytest.mark.parametrize("url", ["/user/tokens/"])
def test_list_bad_jwt(url):
    r = client.get(url, headers={"Authorization": "Bearer invalid"})
    assert r.status_code == 401, r.text


def test_list_with_expire(monkeypatch):
    monkeypatch.setattr(
        "LbAPI.user.USER_TOKEN_INACTIVITY_TIMEOUT", text("INTERVAL 1 second")
    )
    auth_kwargs = get_sso_token()

    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text

    # Use a random description to avoid collisions
    description = f"example 🏴‍☠️ description {secrets.token_hex()}"

    r = client.post(
        "/user/tokens/create", **auth_kwargs, json={"description": description}
    )
    assert r.status_code == 201, r.text

    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text
    assert any(x["description"] == description for x in r.json())

    # Wait for the credentials to expire
    time.sleep(2)

    # Ensure that the credentials are no longer listed
    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text
    assert not any(x["description"] == description for x in r.json())


def test_list_with_revoke():
    auth_kwargs = get_sso_token()

    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text

    # Use a random description to avoid collisions
    description = f"example 🏴‍☠️ description {secrets.token_hex()}"

    r = client.post(
        "/user/tokens/create", **auth_kwargs, json={"description": description}
    )
    assert r.status_code == 201, r.text

    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text
    token_info = [x for x in r.json() if x["description"] == description][0]

    r = client.post(f"/user/tokens/{token_info['id']}/revoke", **auth_kwargs)
    assert r.status_code == 200, r.text

    # Ensure that the credentials are no longer listed
    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text
    assert not any(x["description"] == description for x in r.json())


def test_create_and_expire(monkeypatch):
    monkeypatch.setattr(
        "LbAPI.auth.USER_TOKEN_INACTIVITY_TIMEOUT", text("INTERVAL 5 second")
    )
    # Use a random description to avoid collisions
    description = f"example 🏴‍☠️ description {secrets.token_hex()}"

    r = client.post(
        "/user/tokens/create", **get_sso_token(), json={"description": description}
    )
    assert r.status_code == 201, r.text
    user_token = r.json()

    for _ in range(5):
        # Check that the credentials can be used to access stable endpoints
        r = client.get(
            "/stable/v1/",
            headers={"Authorization": f"Bearer {user_token}"},
        )
        assert r.status_code == 200, r.text
        assert r.json() == {
            "WG1": ["Analysis1", "Analysis2"],
            "WG2": ["Analysis2", "Analysis3", "Analysis4"],
        }
        time.sleep(3)

    # Wait for the credentials to expire
    time.sleep(3)

    # Check that the credentials can no longer be used to access stable endpoints
    r = client.get(
        "/stable/v1/",
        headers={"Authorization": f"Bearer {user_token}"},
    )
    assert r.status_code == 401, r.text


def test_create_and_revoke():
    auth_kwargs = get_sso_token()

    # Use a random description to avoid collisions
    description = f"example 🏴‍☠️ description {secrets.token_hex()}"

    r = client.post(
        "/user/tokens/create", **auth_kwargs, json={"description": description}
    )
    assert r.status_code == 201, r.text
    user_token = r.json()

    # Get the token ID
    r = client.get("/user/tokens/", **auth_kwargs)
    assert r.status_code == 200, r.text
    token_info = [x for x in r.json() if x["description"] == description][0]

    # Check that the credentials can be used to access stable endpoints
    r = client.get(
        "/stable/v1/",
        headers={"Authorization": f"Bearer {user_token}"},
    )
    assert r.status_code == 200, r.text
    assert r.json() == {
        "WG1": ["Analysis1", "Analysis2"],
        "WG2": ["Analysis2", "Analysis3", "Analysis4"],
    }

    # Revoke the token
    r = client.post(f"/user/tokens/{token_info['id']}/revoke", **auth_kwargs)
    assert r.status_code == 200, r.text

    # Check that the credentials can no longer be used to access stable endpoints
    r = client.get(
        "/stable/v1/",
        headers={"Authorization": f"Bearer {user_token}"},
    )
    assert r.status_code == 401, r.text


def test_revoke_missing():
    r = client.post("/user/tokens/99999/revoke", **get_sso_token())
    assert r.status_code == 404, r.text
    assert r.json()["detail"] == "ID 99999 not found"


def test_revoke_current_token():
    # Use a random description to avoid collisions
    description = f"example 🏴‍☠️ description {secrets.token_hex()}"

    r = client.post(
        "/user/tokens/create", **get_sso_token(), json={"description": description}
    )
    assert r.status_code == 201, r.text
    user_token = r.json()
    token_headers = dict(headers={"Authorization": f"Bearer {user_token}"})

    # Check that the token currently works
    r = client.get("/user/", **token_headers)
    assert r.status_code == 200, r.text
    assert isinstance(r.json()["token_id"], int)
    assert r.json()["auth_type"] == "USER_TOKEN"

    # Revoke the token
    r = client.post("/user/tokens/revoke", **token_headers)
    assert r.status_code == 200, r.text

    # Check that the credentials can no longer be used to access stable endpoints
    r = client.get("/stable/v1/", **token_headers)
    assert r.status_code == 401, r.text
