###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from fastapi.testclient import TestClient

from LbAPI import app

client = TestClient(app)


def test_list_analyses(fake_user):
    response = client.get("/productions")
    assert response.status_code == 200
    result = response.json()
    assert "charm" in result
    assert "d2hll" in result["charm"]


def test_samples(fake_user):
    response = client.get("/productions/charm/d2hll")
    assert response.status_code == 200
    result = response.json()
    assert "lfns" not in result[0]
    assert "transformations" not in result[0]
    assert "tags" in result[0]


def test_sample(fake_user):
    response = client.get(
        "/productions/charm/d2hll/v0r0p2510752/mc_2016_magup_dptopiphi_phitomumu_os"
    )
    assert response.status_code == 200
    result = response.json()
    assert "lfns" in result
    assert any(result), "No LFNs with PFNs found"
    assert "transformations" in result
    assert "tags" in result

    response = client.get(
        "/productions/Charm/D2hll/v0r0p2510752/MC_2016_MAGUP_DPTOPIPHI_PHITOMUMU_OS"
    )
    assert response.status_code == 200
    assert result == response.json()

    response = client.get(
        "/productions/Charm/XicpStStToLcKPi/v0r0p2898273/xicpststtolckpi_mc_26265973_2016_magdown"
    )
    assert response.status_code == 200
    assert result != response.json()
    assert "gitlab.cern.ch" in response.json()["merge_request"]
    assert ".cern.ch/jira" in response.json()["jira_task"]

    response = client.get(
        "/productions/Charm/XicpStStToLcKPi/v0r0p2898273/invalid-name"
    )
    assert response.status_code == 404


def test_samples_2(fake_user):
    response = client.get("/productions/charm/XicpStStToLcKPi")
    assert response.status_code == 200
    result = response.json()
    assert "lfns" not in result[0]
    assert "transformations" not in result[0]
    assert "tags" in result[0]
