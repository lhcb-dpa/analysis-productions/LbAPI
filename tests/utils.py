###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
import secrets
import sys
from functools import partial
from itertools import zip_longest
from pathlib import Path
from urllib import parse

import requests
from fastapi.testclient import TestClient

from LbAPI import app
from LbAPI.database import SessionLocal
from LbAPI.settings import (
    settings,
)

REF_JOB_DIR = Path(__file__).parent / "ci-reference" / "successful"


client = TestClient(app)


def get_sso_token():
    url = parse.urlparse(settings.cern_sso_well_known)
    r = requests.post(
        f"{url.scheme}://{url.netloc}/cern_sso/token",
        timeout=5,
        data={
            "grant_type": "authorization_code",
            "code": "xxxxxx",
            "client_id": settings.cern_sso_client_id,
        },
    )
    r.raise_for_status()

    return dict(headers={"Authorization": f"Bearer {r.json()['access_token']}"})


def get_gitlab_jwt():
    url = parse.urlparse(settings.cern_gitlab_well_known)
    r = requests.post(
        f"{url.scheme}://{url.netloc}/cern_gitlab/token",
        timeout=5,
        data={
            "grant_type": "authorization_code",
            "code": "xxxxxx",
            "client_id": "cern-gitlab",
        },
    )
    r.raise_for_status()
    return dict(headers={"Authorization": f"Bearer {r.json()['access_token']}"})


async def clean_db(project_id: int):
    from sqlalchemy import delete, select

    from LbAPI.database.webhooks import CIRun, Job, Pipeline, ProjectWebhook

    async with SessionLocal() as session:
        ci_runs = select(CIRun.id)
        ci_runs = ci_runs.where(CIRun.pipeline_id == Pipeline.id)
        ci_runs = ci_runs.where(Pipeline.project_id == project_id)

        result = await session.execute(
            delete(Job)
            .where(Job.ci_run_id.in_(ci_runs))
            .execution_options(synchronize_session=False)
        )
        result = await session.execute(
            delete(CIRun)
            .where(CIRun.id.in_(ci_runs))
            .execution_options(synchronize_session=False)
        )
        result = await session.execute(
            delete(Pipeline)
            .where(Pipeline.project_id == project_id)
            .execution_options(synchronize_session=False)
        )
        session.expire_all()

        result = await session.execute(
            delete(ProjectWebhook).where(ProjectWebhook.project_id == project_id)
        )
        if result.rowcount != 1:
            raise NotImplementedError()
        await session.commit()


def job_runner(job_dir, client, *, success):
    assert (job_dir / "log_uploader.py").is_file()
    assert (job_dir / "job_script.py").is_file()
    assert (job_dir / "config.json").is_file()

    from LbAPI.ci.templates import log_uploader

    sys.modules["log_uploader"] = sys.modules[log_uploader.__name__]
    from LbAPI.ci.templates import job_script

    sys.modules.pop("log_uploader")

    return job_script.main(
        job_dir, session=client, func=partial(_fake_run_job, success=success)
    )


async def _fake_run_job(job_dir, uploader, extra_env, extra_args=None, *, success):
    ref_dirac_log = (REF_JOB_DIR / "DIRAC.log").read_text().splitlines(keepends=True)
    ref_job_logs_paths = sorted(
        REF_JOB_DIR.glob("Local_son7w77b_JobDir/*.log"),
        key=lambda x: int(x.name.split("_")[-1].split(".")[0]),
    )

    ref_job_logs = []
    for path in ref_job_logs_paths:
        ref_log = path.read_text().splitlines(keepends=True)
        n = len(ref_log) // 10
        ref_job_logs.extend(
            (path.name, ref_log[x : x + n]) for x in range(0, len(ref_log), n)
        )
    n = len(ref_dirac_log) // sum(map(len, ref_job_logs))
    ref_dirac_log = [ref_dirac_log[x : x + n] for x in range(0, len(ref_dirac_log), n)]

    local_job_dir = job_dir / f"Local_{secrets.token_hex(10)}_JobDir"
    local_job_dir.mkdir()
    for dlog, jlog in zip_longest(ref_dirac_log, ref_job_logs):
        if dlog:
            # print("Writing", len(dlog), "lines to DIRAC.log")
            with (job_dir / "DIRAC.log").open("at") as fp:
                fp.write("".join(dlog))

        if jlog:
            jlog_name, jlog = jlog
            # print("Writing", len(jlog), "lines to", jlog_name)
            with (local_job_dir / jlog_name).open("at") as fp:
                fp.write("".join(jlog))
        await asyncio.sleep(0.1)

    return success
