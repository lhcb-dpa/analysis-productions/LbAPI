###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import time
from copy import deepcopy

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import select

from LbAPI import app
from LbAPI.database import SessionLocal
from LbAPI.database.models import GitLabToken
from LbAPI.utils import Token

from .utils import get_gitlab_jwt, get_sso_token

EXAMPLE_REPO_INFO = {
    "project_id": 145826,
    "project_path": "lbaptestuser/test-simulation-requests",
    "eos_specs": [
        {"path": "/eos/lhcb/grid/prod/lhcb/", "allow_write": False},
        {"path": "/eos/lhcb/wg/MYWG/MYANALYSIS/", "allow_write": True},
        {"path": "/eos/lhcb/user/l/lbaptestuser/my-thing/", "allow_write": True},
    ],
}

EXAMPLE_REPO_INFO_2 = {
    "project_id": 145826,
    "project_path": "lhcb-simulation/test-simulation-requests",
    "eos_specs": [
        {"path": "/eos/lhcb/grid/prod/lhcb/", "allow_write": False},
        {"path": "/eos/lhcb/wg/MYWG/MYANALYSIS/", "allow_write": True},
    ],
}

client = TestClient(app)


def test_missing():
    r = client.get("/gitlab/credentials/")
    assert r.status_code == 403, r.text


def test_bad_jwt():
    r = client.get("/gitlab/credentials/", headers={"Authorization": "Bearer invalid"})
    assert r.status_code == 401, r.text


def test_valid_but_unregistered():
    auth_kwargs = get_gitlab_jwt()

    r = client.get("/gitlab/credentials/", **auth_kwargs)
    assert r.status_code == 401, r.text
    assert "register your repository" in r.json()["detail"]

    # Wait for the credentials to expire
    time.sleep(5)

    # Ensure that the expired credentials don't work
    r = client.get("/gitlab/credentials/", **auth_kwargs)
    assert r.status_code == 401, r.text
    assert r.json()["detail"] == "Could not validate credentials"


def test_valid_but_external_ip(monkeypatch):
    auth_kwargs = get_gitlab_jwt()

    r = client.post("/gitlab/register/", **get_sso_token(), json=EXAMPLE_REPO_INFO)
    assert r.status_code == 201, r.text
    registered_id = r.json()

    try:
        # Ensure that the credentials work
        r = client.get("/gitlab/credentials/", **auth_kwargs)
        assert r.status_code == 200, r.text

        # Ensure that the credentials don't work from a different IP
        monkeypatch.setattr("starlette.datastructures.Address.host", "8.8.8.8")
        r = client.get("/gitlab/credentials/", **auth_kwargs)
        assert r.status_code == 401, r.text
        assert r.json()["detail"] == "Only CERN IPs are allowed to access this endpoint"
    finally:
        # And revoke to clean up
        r = client.post(
            f"/gitlab/revoke/{registered_id}", **get_sso_token(), json=EXAMPLE_REPO_INFO
        )
        assert r.status_code == 200, r.text


def test_revoke_missing():
    r = client.post("/gitlab/revoke/99999", **get_sso_token())
    assert r.status_code == 404, r.text
    assert r.json()["detail"] == "ID 99999 not found"


@pytest.mark.asyncio
async def test_valid_workflow():
    auth_kwargs = get_sso_token()

    r = client.request("GET", "/gitlab/list/", **auth_kwargs, json=EXAMPLE_REPO_INFO)
    assert r.status_code == 200, r.text
    assert r.json() == []

    # Register a repository
    r = client.post("/gitlab/register/", **auth_kwargs, json=EXAMPLE_REPO_INFO)
    assert r.status_code == 201, r.text
    registered_id = r.json()
    assert isinstance(registered_id, int)

    # Make sure we now have a registered record
    r = client.request("GET", "/gitlab/list/", **auth_kwargs, json=EXAMPLE_REPO_INFO)
    assert r.status_code == 200, r.text
    assert len(r.json()) == 1

    # Make sure we can get credentials for the newly registered repository
    r = client.get("/gitlab/credentials/", **get_gitlab_jwt())
    assert r.status_code == 200, r.text
    apd_credentials = r.json()

    # Make sure the credentials were logged to the database
    assert apd_credentials["lbapi_token"].startswith("lbapi:")
    assert len(apd_credentials["eos_tokens"]) == 3
    async with SessionLocal() as session:
        query = select(GitLabToken.eos_token)
        hashed_token = Token(apd_credentials["lbapi_token"]).hashed_token
        query = query.where(GitLabToken.apd_token == hashed_token)
        result = (await session.execute(query)).all()
        assert len(result) == 1
        assert result[0].eos_token == [
            {"blah": "blah"},
            {"blah": "blah"},
            {"blah": "blah"},
        ]

    # Check that the credentials can be used to access stable endpoints
    r = client.get(
        "/stable/v1/",
        headers={"Authorization": f"Bearer {apd_credentials['lbapi_token']}"},
    )
    assert r.status_code == 200
    assert r.json() == {
        "WG1": ["Analysis1", "Analysis2"],
        "WG2": ["Analysis2", "Analysis3", "Analysis4"],
    }

    # Re-registering should fail
    r = client.post("/gitlab/register/", **auth_kwargs, json=EXAMPLE_REPO_INFO)
    assert r.status_code == 409, r.text
    assert r.json()["detail"].startswith("Already registered with id=")

    # Revoke the repository access
    r = client.post(
        f"/gitlab/revoke/{registered_id}", **auth_kwargs, json=EXAMPLE_REPO_INFO
    )
    assert r.status_code == 200, r.text

    # Revoked record should no longer be listed
    r = client.request("GET", "/gitlab/list/", **auth_kwargs, json=EXAMPLE_REPO_INFO)
    assert r.status_code == 200, r.text
    assert r.json() == []

    # Getting credentials should now fail
    r = client.get("/gitlab/credentials/", **get_gitlab_jwt())
    assert r.status_code == 401, r.text
    assert "register your repository" in r.json()["detail"]

    # Check that the credentials can no longer be used to access stable endpoints
    r = client.get(
        "/stable/v1/",
        headers={"Authorization": f"Bearer {apd_credentials['lbapi_token']}"},
    )
    assert r.status_code == 401

    # Re-revoking should be fine
    r = client.post(
        f"/gitlab/revoke/{registered_id}", **auth_kwargs, json=EXAMPLE_REPO_INFO
    )
    assert r.status_code == 200, r.text


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "eos_specs",
    [
        [{"path": "/eos/lhcb/grid/prod/lhcb", "allow_write": False}],
        [{"path": "eos/lhcb/grid/prod/lhcb/", "allow_write": False}],
        [{"path": "/eos/lhcb/grid/prod/lhcb/../", "allow_write": False}],
        [{"path": "/eos/lhcb/grid/prod/lhcb/", "allow_write": True}],
        [{"path": "/eos/lhcb/wg/Semilep/MyAnalysis/output", "allow_write": False}],
        [{"path": "/eos/lhcb/wg/Semilep/MyAnalysis/output", "allow_write": True}],
        [{"path": "/eos/lhcb/wg/Semilep/", "allow_write": False}],
        [{"path": "/eos/lhcb/wg/Semilep/", "allow_write": True}],
        [{"path": "/eos/lhcb/wg/", "allow_write": False}],
        [{"path": "/eos/lhcb/wg/", "allow_write": True}],
        [{"path": "/eos/lhcb/user/a/auser/", "allow_write": False}],
        [{"path": "/eos/lhcb/user/a/auser/", "allow_write": True}],
    ],
)
async def test_invalid_eos_path(eos_specs):
    repo_info = deepcopy(EXAMPLE_REPO_INFO)
    repo_info["eos_specs"] = eos_specs
    r = client.post("/gitlab/register/", **get_sso_token(), json=repo_info)
    assert r.status_code == 422, r.text


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "eos_specs",
    [
        [{"path": "/eos/lhcb/user/l/lbaptestuser/my-thing/", "allow_write": False}],
        [{"path": "/eos/lhcb/user/l/lbaptestuser/my-thing/", "allow_write": True}],
    ],
)
async def test_invalid_eos_path_2(eos_specs):
    repo_info = deepcopy(EXAMPLE_REPO_INFO_2)
    repo_info["eos_specs"] = eos_specs
    r = client.post("/gitlab/register/", **get_sso_token(), json=repo_info)
    assert r.status_code == 422, r.text


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "eos_specs",
    [
        [{"path": "/eos/lhcb/grid/prod/lhcb/", "allow_write": False}],
        [{"path": "/eos/lhcb/wg/Semilep/MyAnalysis/", "allow_write": False}],
        [{"path": "/eos/lhcb/wg/Semilep/MyAnalysis/", "allow_write": True}],
        [{"path": "/eos/lhcb/wg/Semilep/MyAnalysis/output/", "allow_write": False}],
        [{"path": "/eos/lhcb/wg/Semilep/MyAnalysis/output/", "allow_write": True}],
        [{"path": "/eos/lhcb/user/l/lbaptestuser/my-thing/", "allow_write": False}],
        [{"path": "/eos/lhcb/user/l/lbaptestuser/my-thing/", "allow_write": True}],
    ],
)
async def test_valid_eos_path(eos_specs):
    repo_info = deepcopy(EXAMPLE_REPO_INFO)
    repo_info["eos_specs"] = eos_specs
    r = client.post("/gitlab/register/", **get_sso_token(), json=repo_info)
    assert r.status_code == 201, r.text
    registered_id = r.json()

    r = client.post(
        f"/gitlab/revoke/{registered_id}", **get_sso_token(), json=EXAMPLE_REPO_INFO
    )
    assert r.status_code == 200, r.text
